use std::{
    collections::HashMap,
    fmt::Display,
    sync::atomic::{self, AtomicBool},
    time::{Duration, Instant},
};

use chers::{Board, Move, PieceColor, PieceKind};

use crate::{Brain, Response};

type ZobristHash = u64;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct AlphaBetaSettings {
    pub max_depth_plies: usize,
}

impl Default for AlphaBetaSettings {
    fn default() -> Self {
        Self {
            max_depth_plies: 20,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Default)]
pub struct AlphaBeta {
    settings: AlphaBetaSettings,
    transposition_table: HashMap<(ZobristHash, u8), f64>,
}

impl AlphaBeta {
    pub fn new(settings: AlphaBetaSettings) -> Self {
        Self {
            settings,
            ..Self::default()
        }
    }
}

pub struct Stats {
    pub start_time: Instant,
    pub num_positions_searched: u64,
    pub num_positions_evaluated: u64,
}

impl Stats {
    pub fn new() -> Self {
        Self {
            start_time: Instant::now(),
            num_positions_searched: 0,
            num_positions_evaluated: 0,
        }
    }
}

pub fn search(
    board: &Board,
    evaluate: &mut impl FnMut(&Board) -> f32,
    sort_moves: &mut impl FnMut(&Board, &mut [Move]),
    stats: &mut Stats,
    depth_plies: usize,
    mut alpha: f32,
    beta: f32,
) -> f32 {
    stats.num_positions_searched += 1;

    if depth_plies == 0 {
        stats.num_positions_evaluated += 1;
        return evaluate(board);
    }

    for (_mov, new_board) in board.after_each_sorted_legal_move(sort_moves) {
        let score = -search(
            &new_board,
            evaluate,
            sort_moves,
            stats,
            depth_plies - 1,
            -beta,
            -alpha,
        );
        if score >= beta {
            return beta;
        }
        alpha = alpha.max(score);
    }

    alpha
}

pub fn find_next_move(
    board: &Board,
    evaluate: &mut impl FnMut(&Board) -> f32,
    sort_moves: &mut impl FnMut(&Board, &mut [Move]),
    depth_plies: usize,
    end_search: &AtomicBool,
) -> Option<Move> {
    let mut stats = Stats::new();
    let best_move = board
        .after_each_sorted_legal_move(&mut *sort_moves)
        .take_while(|_| end_search.load(atomic::Ordering::Relaxed) == false)
        .map(|(mov, new_board)| {
            (
                mov,
                -search(
                    &new_board,
                    &mut *evaluate,
                    &mut *sort_moves,
                    &mut stats,
                    depth_plies - 1,
                    f32::NEG_INFINITY,
                    f32::INFINITY,
                ),
            )
        })
        .max_by(|(_mov1, score1), (_mov2, score2)| score1.total_cmp(score2))
        .map(|(mov, _score)| mov);

    let Some(best_move) = best_move else {
        // No move found.
        return None;
    };

    tracing::debug!("Positions searched: {}", stats.num_positions_searched);
    tracing::debug!("Positions evaluated: {}", stats.num_positions_evaluated);
    tracing::debug!(
        "Time taken: {:.3} milliseconds",
        stats.start_time.elapsed().as_secs_f64() * 1000.0
    );

    Some(best_move)
}

pub fn iterative_deepening(
    board: &Board,
    evaluate: &mut impl FnMut(&Board) -> f32,
    sort_moves: &mut impl FnMut(&Board, &mut [Move]),
    max_depth_plies: usize,
    end_search: &AtomicBool,
) -> Move {
    let mut best_move = *board
        .legal_moves()
        .get(0)
        .expect("At least one move must be legal");
    for depth_plies in 1..max_depth_plies {
        if end_search.load(atomic::Ordering::Acquire) {
            break;
        }

        if let Some(new_best_move) = find_next_move(
            board,
            &mut *evaluate,
            &mut *sort_moves,
            depth_plies,
            end_search,
        ) {
            best_move = new_best_move;
        }
    }
    return best_move;
}

pub fn eval_diff(
    mut evaluate_side: impl FnMut(&Board, PieceColor) -> f32,
) -> impl FnMut(&Board) -> f32 {
    move |board| evaluate_side(board, board.turn_color) - evaluate_side(board, !board.turn_color)
}

pub fn piece_value_evaluator(board: &Board, color: PieceColor) -> f32 {
    let count_kind = |kind| {
        (board.raw.piece_locations()[kind] & board.raw.piece_colors()[color])
            .0
            .count_ones() as f32
    };

    count_kind(PieceKind::Pawn) * get_piece_value(PieceKind::Pawn) as f32
        + count_kind(PieceKind::Bishop) * get_piece_value(PieceKind::Bishop) as f32
        + count_kind(PieceKind::Knight) * get_piece_value(PieceKind::Knight) as f32
        + count_kind(PieceKind::Rook) * get_piece_value(PieceKind::Rook) as f32
        + count_kind(PieceKind::Queen) * get_piece_value(PieceKind::Queen) as f32
}

pub fn get_piece_value(kind: PieceKind) -> i32 {
    match kind {
        PieceKind::Pawn => 100,
        PieceKind::Bishop => 350,
        PieceKind::Knight => 300,
        PieceKind::Rook => 500,
        PieceKind::Queen => 900,
        PieceKind::King => 1000,
    }
}

pub fn move_value_heuristic(board: &Board, moves: &mut [Move]) {
    moves.sort_by_cached_key(|&mov| {
        let mut score_guess = 0;

        let mov_piece = board
            .raw
            .index(mov.movement().from)
            .expect("Can't move a piece that isn't there");

        if let Some(captured_piece) = board.raw.index(mov.movement().to) {
            score_guess +=
                10 * get_piece_value(captured_piece.kind()) - get_piece_value(mov_piece.kind());
        }

        if let Some(promotion) = mov.promotion() {
            score_guess += 10 * get_piece_value(promotion.to_piece_kind());
        }

        if board.raw.threatened_squares()[!board.turn_color].get_pos(mov.movement().to) {
            score_guess -= get_piece_value(mov_piece.kind());
        } else if board.raw.threatened_squares()[!board.turn_color].get_pos(mov.movement().from) {
            score_guess += get_piece_value(mov_piece.kind()) / 2;
        }

        -score_guess
    });
}

impl Display for AlphaBeta {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("AlphaBeta")
    }
}

impl Brain for AlphaBeta {
    fn next_move(&mut self, board: &Board, end_search: &AtomicBool) -> Response {
        let mov = iterative_deepening(
            board,
            &mut eval_diff(piece_value_evaluator),
            &mut move_value_heuristic,
            self.settings.max_depth_plies,
            end_search,
        );
        Response::Move(mov)
    }

    fn target_time(&mut self, remaining_time: Duration) -> Duration {
        (remaining_time / 5).min(Duration::from_secs(10))
    }
}
