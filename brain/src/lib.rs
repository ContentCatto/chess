pub mod alphabeta;
pub mod bogo;

use std::{
    fmt::{Debug, Display},
    sync::atomic::AtomicBool,
    time::Duration,
};

use chers::{Board, Move};

#[derive(Debug, Clone, Copy)]
pub enum Response {
    Move(Move),
    Forfeit,
}

impl Display for Response {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Response::Move(mov) => write!(f, "{mov}"),
            Response::Forfeit => write!(f, "forfeit"),
        }
    }
}

pub trait Brain: Send + Sync + Debug + Display {
    /// Do any changes necessary when a new game is started.
    fn new_game(&mut self) {}

    /// Prepare everything.
    fn get_ready(&mut self) {}

    /// Calculate how much time we want to think based on the amount of remaining time we have.
    fn target_time(&mut self, remaining_time: Duration) -> Duration {
        return remaining_time / 10;
    }

    /// Figure out the next move, exiting the search when `end_search` is set to true.
    fn next_move(&mut self, board: &Board, end_search: &AtomicBool) -> Response;
}
