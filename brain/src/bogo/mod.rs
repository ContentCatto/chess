use std::{fmt::Display, sync::atomic::AtomicBool};

use chers::Board;

use crate::{Brain, Response};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Bogo;

impl Display for Bogo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Bogo")
    }
}

impl Brain for Bogo {
    fn next_move(&mut self, board: &Board, _end_search: &AtomicBool) -> Response {
        let available_moves = board.legal_moves();
        let mov = fastrand::choice(available_moves).unwrap();
        Response::Move(mov)
    }
}
