use std::sync::atomic::AtomicBool;

use brain::{Brain, alphabeta::AlphaBeta};
use chers::Board;

const KIWIPETE_FEN: &str = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1";

fn main() {
    let board = Board::from_fen(KIWIPETE_FEN).expect("Kiwipete board FEN should be valid");
    let response = AlphaBeta { max_depth_plies: 4 }.next_move(&board, &AtomicBool::new(false));
    println!("Response: {response}");
}
