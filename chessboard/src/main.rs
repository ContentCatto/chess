use bevy::prelude::*;
use board::{board::add_board, BoardInteraction, BoardPlugin, LoadingStage};

pub mod board;

fn main() {
    App::new().add_plugins(MyPlugin).run();
}

#[derive(Component)]
struct MyCameraMaker;

fn setup_camera(mut commands: Commands) {
    commands.spawn((
        Transform::from_xyz(0., 0., 0.),
        Camera2d::default(),
        MyCameraMaker,
        Msaa::Sample8,
    ));
}

pub struct MyPlugin;

impl Plugin for MyPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(DefaultPlugins.set(ImagePlugin::default_linear()))
            .add_plugins(BoardPlugin)
            .add_plugins(BoardInteraction)
            .add_systems(Startup, setup_camera)
            .add_systems(OnEnter(LoadingStage::Loaded), add_board(60., true))
        // let it return
        ;
    }
}
