use std::sync::Arc;

use bevy::prelude::*;

use super::{dragging::DragFinished, draw_rect};

#[derive(Debug, Clone, Copy, Component)]
pub struct Slot(pub Rect);

impl Slot {
    pub fn hitbox(&self, global_transform: &GlobalTransform) -> Rect {
        let translation = global_transform.translation();
        let hitbox_min = global_transform
            .transform_point(self.0.min.extend(translation.z))
            .xy();
        let hitbox_max = global_transform
            .transform_point(self.0.max.extend(translation.z))
            .xy();
        Rect::from_corners(hitbox_min, hitbox_max)
    }
}

#[derive(Debug, Clone, Component)]
pub struct SnapToSlot {
    slots: Arc<[Entity]>,
}

impl SnapToSlot {
    pub fn new(slots: Arc<[Entity]>) -> Self {
        Self { slots }
    }
}

#[derive(Debug, Clone, Copy, Event)]
pub struct SnappedToSlot {
    pub entity: Entity,
    pub slot_rect: Rect,
    pub slot_entity: Entity,
    pub drag_event: DragFinished,
}

#[derive(Debug, Clone, Copy, Event)]
pub struct FailedToSnapToSlot {
    pub entity: Entity,
    pub drag_event: DragFinished,
}

pub(crate) fn handle_snap_to_slot(
    mut commands: Commands,
    mut snapped_to_slot: EventWriter<SnappedToSlot>,
    mut failed_to_snap_to_slot: EventWriter<FailedToSnapToSlot>,

    mut drag_finished: EventReader<DragFinished>,

    mut slottable_entities: Query<(&SnapToSlot, &mut Transform, &GlobalTransform)>,
    slot_entities: Query<(&Transform, &GlobalTransform, &Slot), Without<SnapToSlot>>,
) {
    'events: for event in drag_finished.read() {
        let entity = event.entity;
        let Ok((snap, mut transform, global_transform)) = slottable_entities.get_mut(entity) else {
            continue;
        };

        for &slot_entity in &*snap.slots {
            let (slot_transform, slot_global_transform, slot) = slot_entities
                .get(slot_entity)
                .expect("Slot entities must have a transform and global transform");

            let slot_rect = slot.hitbox(slot_global_transform);

            if slot_rect.contains(global_transform.translation().xy()) {
                commands.entity(entity).set_parent(slot_entity);
                tracing::trace!(
                    "Prev parent: {:?}, new parent: {slot_entity:?}.",
                    event.prev_parent,
                );
                transform.translation = slot.0.center().extend(slot_transform.translation.z);
                if event.prev_parent != slot_entity {
                    tracing::debug!(
                        "Snapping {:?} to position {}, which is the center of the slot {slot:?}",
                        event.entity,
                        slot_rect.center(),
                    );
                    snapped_to_slot.send(SnappedToSlot {
                        entity,
                        slot_rect,
                        slot_entity,
                        drag_event: *event,
                    });
                } else {
                    tracing::debug!(
                        "Snapping back {:?} to previous position {}, which is the center of the slot {slot:?}",
                        event.entity,
                        slot_rect.center(),
                    );
                }
                continue 'events;
            }
        }
        transform.translation = event.start_position;
        tracing::debug!(
            "Failed to snap {:?} to any of the slots, returning it to position {}.",
            event.entity,
            transform.translation,
        );
        failed_to_snap_to_slot.send(FailedToSnapToSlot {
            entity,
            drag_event: *event,
        });
    }
}

pub(crate) fn draw_drag_slots(mut gizmos: Gizmos, draggables: Query<(&GlobalTransform, &Slot)>) {
    for (global_transform, slot) in draggables.iter() {
        draw_rect(
            &mut gizmos,
            global_transform,
            slot.0,
            LinearRgba {
                red: 0.0,
                green: 0.5,
                blue: 1.0,
                alpha: 1.0,
            },
        );
    }
}
