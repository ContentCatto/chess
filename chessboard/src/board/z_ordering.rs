pub const LIGHT_CHECKER: f32 = 0.1;
pub const DARK_CHECKER: f32 = 0.2;

pub const THREATENED_SQUARE: f32 = 1.1;
pub const PINNED_SQUARE: f32 = 1.2;
pub const CHECK_PATH_1_SQUARE: f32 = 1.3;
pub const CHECK_PATH_2_SQUARE: f32 = 1.4;
pub const CHECK_PATH_3_SQUARE: f32 = 1.5;
pub const AVAILABLE_MOVE_SQUARE: f32 = 1.9;

pub const GRID_SQUARE: f32 = 10.0;
pub const GRID_SQUARE_SLOT: f32 = GRID_SQUARE;

pub const PIECE_SPRITE: f32 = GRID_SQUARE_SLOT;
