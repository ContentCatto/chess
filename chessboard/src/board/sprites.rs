use bevy::prelude::*;
use bevy::{asset::Handle, sprite::TextureAtlas, utils::HashMap};
use chers::{ColPiece, PieceColor, PieceKind};

use super::LoadBoardProgress;

#[derive(Resource)]
pub struct LoadingChessSprites(Handle<Image>);

#[derive(Resource)]
pub struct ChessSprites {
    atlas_image: Handle<Image>,
    atlas_layout: Handle<TextureAtlasLayout>,
    piece_atlas_indices: HashMap<ColPiece, usize>,
    piece_sprite_size: UVec2,
}

impl ChessSprites {
    pub fn get_piece(&self, piece: ColPiece) -> Sprite {
        let index = *self.piece_atlas_indices.get(&piece).unwrap();
        let atlas = TextureAtlas {
            layout: self.atlas_layout.clone(),
            index,
        };
        let image = self.atlas_image.clone();
        Sprite::from_atlas_image(image, atlas)
    }

    pub fn get_sprite(&self, piece: ColPiece, transform: Transform) -> (Sprite, Transform) {
        (self.get_piece(piece), transform)
    }

    pub fn get_sprite_scaled(
        &self,
        col_piece: ColPiece,
        transform: Transform,
        tile_width: f32,
    ) -> (Sprite, Transform) {
        let scale = Vec3::new(
            tile_width / self.piece_sprite_size.x as f32,
            tile_width / self.piece_sprite_size.y as f32,
            1.0,
        );
        self.get_sprite(col_piece, transform * Transform::from_scale(scale))
    }

    pub fn update_sprite(&self, piece: ColPiece, sprite: &mut Sprite) {
        *sprite = self.get_piece(piece);
    }

    pub fn piece_size(&self) -> UVec2 {
        self.piece_sprite_size
    }
}

pub fn load_chess_sprites(
    mut commands: Commands,
    server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlasLayout>>,
) {
    let atlas_texture: Handle<Image> = server.load("standard_pieces_sprite_atlas_rendered.png");
    let loading = LoadingChessSprites(atlas_texture.clone());
    commands.insert_resource(loading);

    let atlas_layout =
        TextureAtlasLayout::from_grid(UVec2::new(6000 / 6, 2000 / 2), 6, 2, None, None);
    let piece_size = atlas_layout.textures[0].size();

    let atlas_layout = texture_atlases.add(atlas_layout);

    let pieces = [
        ColPiece::new(PieceKind::King, PieceColor::White),
        ColPiece::new(PieceKind::Queen, PieceColor::White),
        ColPiece::new(PieceKind::Bishop, PieceColor::White),
        ColPiece::new(PieceKind::Knight, PieceColor::White),
        ColPiece::new(PieceKind::Rook, PieceColor::White),
        ColPiece::new(PieceKind::Pawn, PieceColor::White),
        ColPiece::new(PieceKind::King, PieceColor::Black),
        ColPiece::new(PieceKind::Queen, PieceColor::Black),
        ColPiece::new(PieceKind::Bishop, PieceColor::Black),
        ColPiece::new(PieceKind::Knight, PieceColor::Black),
        ColPiece::new(PieceKind::Rook, PieceColor::Black),
        ColPiece::new(PieceKind::Pawn, PieceColor::Black),
    ]
    .into_iter()
    .enumerate()
    .map(|(index, colpiece)| (colpiece, index))
    .collect();

    let chess_sprites = ChessSprites {
        atlas_image: atlas_texture,
        atlas_layout,
        piece_atlas_indices: pieces,
        piece_sprite_size: piece_size,
    };
    commands.insert_resource(chess_sprites);
}

pub fn check_chess_sprites_loaded(
    mut loading_progress: ResMut<LoadBoardProgress>,
    handle: Res<LoadingChessSprites>,
    mut events: EventReader<AssetEvent<Image>>,
) {
    for event in events.read() {
        if event.is_loaded_with_dependencies(&handle.0) {
            loading_progress.sprites_loaded = true;
        }
    }
}
