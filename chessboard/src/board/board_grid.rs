use std::sync::Arc;

use bevy::prelude::*;
use chers::Pos;

use crate::board::snapping::Slot;

use super::{meshes::ChessMeshes, z_ordering};

#[derive(Debug, Clone, Copy, Component)]
pub struct GridSquare {
    pub(crate) pos: Pos,
    pub(crate) board_grid: Entity,
}

#[derive(Debug, Component)]
pub struct BoardGrid {
    pub(crate) slots: Arc<[Entity]>,
}

pub fn get_pos_from_row_and_col_index(col: u8, row: u8) -> Vec2 {
    Vec2::new(col as f32 - 3.5, row as f32 - 3.5)
}

pub fn add_board_grid(commands: &mut Commands, chess_meshes: &Res<ChessMeshes>) -> Entity {
    let mut grid = commands.spawn(Transform::IDENTITY);

    grid.with_children(|parent| {
        let light_square = chess_meshes.get_light_square(
            Transform::from_xyz(0.0, 0.0, z_ordering::LIGHT_CHECKER)
                * Transform::from_scale(Vec3::new(8.0, 8.0, 1.0)),
        );
        parent.spawn(light_square);
    });

    let mut slots: Vec<Entity> = Vec::with_capacity(8 * 8);

    for row in (0..8).rev() {
        for col in 0..8 {
            let pos = get_pos_from_row_and_col_index(col, row);
            let slot = Rect::from_center_size(Vec2::ZERO, Vec2::ONE);
            grid.with_children(|parent| {
                let chess_pos =
                    Pos::from_coords(col as u8, row as u8).expect("Coords should be valid");
                tracing::trace!(
                    "Chess pos {chess_pos}, flat index {}, board pos {pos}",
                    chess_pos.to_flat_index(),
                );
                let entity = parent
                    .spawn((
                        Transform::from_translation(pos.extend(z_ordering::GRID_SQUARE)),
                        GridSquare {
                            pos: chess_pos,
                            board_grid: parent.parent_entity(),
                        },
                        Slot(slot),
                    ))
                    .id();

                slots.push(entity);
            });

            if (col + row) % 2 == 0 {
                grid.with_children(|parent| {
                    parent.spawn(chess_meshes.get_dark_square(Transform::from_xyz(
                        pos.x,
                        pos.y,
                        z_ordering::DARK_CHECKER,
                    )));
                });
            }
        }
    }

    let slots: Arc<[Entity]> = slots.into();

    grid.insert(BoardGrid { slots });

    grid.id()
}

pub(crate) fn draw_chesspos_flat_index(
    mut gizmos: Gizmos,
    grid_squares: Query<(&GlobalTransform, &GridSquare)>,
) {
    for (global_transform, grid_square) in grid_squares.iter() {
        let (scale, _rot, translation) = global_transform.to_scale_rotation_translation();

        let size_factor = grid_square.pos.to_flat_index() as f32 / 64.0;
        let average_side_length = (scale.x + scale.y) / 2.0;
        let radius = size_factor * average_side_length / 2.0;

        gizmos.circle_2d(
            translation.xy(),
            radius,
            LinearRgba {
                red: 0.0,
                green: 0.8,
                blue: 0.2,
                alpha: 1.0,
            },
        );
    }
}
