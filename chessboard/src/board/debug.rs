use bevy::prelude::*;

// FIXME: Completely untested when using rotations.
pub fn draw_cross_gizmo(
    gizmos: &mut Gizmos,
    rect: Rect,
    rotation: f32,
    size_ratio: f32,
    color: LinearRgba,
) {
    let transform = Transform::from_rotation(Quat::from_axis_angle(Vec3::Z, rotation))
        .with_scale(Vec3::splat(size_ratio))
        .with_translation(rect.center().extend(0.0));

    let do_transform = |vec: Vec2| -> Vec2 { (transform * (vec - rect.center()).extend(0.0)).xy() };

    let tl = do_transform(Vec2::new(rect.min.x, rect.max.y));
    let tr = do_transform(Vec2::new(rect.max.x, rect.max.y));
    let bl = do_transform(Vec2::new(rect.min.x, rect.min.y));
    let br = do_transform(Vec2::new(rect.max.x, rect.min.y));

    gizmos.line_2d(tl.xy(), br.xy(), color);
    gizmos.line_2d(bl.xy(), tr.xy(), color);
}
