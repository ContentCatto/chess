use bevy::prelude::*;

use super::LoadBoardProgress;

#[derive(Resource)]
pub struct ChessMeshes {
    square_mesh: Handle<Mesh>,
    light_material: Handle<ColorMaterial>,
    dark_material: Handle<ColorMaterial>,
    background_material: Handle<ColorMaterial>,
    available_move_material: Handle<ColorMaterial>,
    threatened_square_material: Handle<ColorMaterial>,
    check_path_1_material: Handle<ColorMaterial>,
    check_path_2_material: Handle<ColorMaterial>,
    check_path_3_material: Handle<ColorMaterial>,
    pinned_square_material: Handle<ColorMaterial>,
}

impl ChessMeshes {
    fn get_square_mesh_from_material_and_transform(
        &self,
        material_handle: &Handle<ColorMaterial>,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        (
            Mesh2d(self.square_mesh.clone()),
            MeshMaterial2d(material_handle.clone()),
            transform,
        )
    }

    pub fn get_light_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(&self.light_material, transform)
    }

    pub fn get_dark_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(&self.dark_material, transform)
    }

    pub fn get_background_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(&self.background_material, transform)
    }

    pub fn get_available_move_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(&self.available_move_material, transform)
    }

    pub fn get_threatened_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(
            &self.threatened_square_material,
            transform,
        )
    }

    pub fn get_check_path_1_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(&self.check_path_1_material, transform)
    }

    pub fn get_check_path_2_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(&self.check_path_2_material, transform)
    }

    pub fn get_check_path_3_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(&self.check_path_3_material, transform)
    }

    pub fn get_pinned_square(
        &self,
        transform: Transform,
    ) -> (Mesh2d, MeshMaterial2d<ColorMaterial>, Transform) {
        self.get_square_mesh_from_material_and_transform(&self.pinned_square_material, transform)
    }
}

#[derive(Resource)]
pub struct ChessMeshResources(Vec<UntypedHandle>);

pub(crate) fn load_chess_meshes(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    fn color_f32(r: f32, g: f32, b: f32) -> Color {
        Srgba::rgb(r, g, b).into()
    }

    fn color_u8(r: u8, g: u8, b: u8) -> Color {
        Srgba::from_u8_array_no_alpha([r, g, b]).into()
    }

    fn add_untyped<A: Asset>(res: &mut ChessMeshResources, handle: &Handle<A>) {
        res.0.push(handle.clone().untyped())
    }

    fn add_mesh(
        res: &mut ChessMeshResources,
        mes: &mut Assets<Mesh>,
        mesh: impl Into<Mesh>,
    ) -> Handle<Mesh> {
        let mesh_handle = mes.add(mesh);
        add_untyped(res, &mesh_handle);
        mesh_handle
    }

    fn add_color(
        res: &mut ChessMeshResources,
        mat: &mut Assets<ColorMaterial>,
        color: Color,
    ) -> Handle<ColorMaterial> {
        let material = mat.add(color);
        add_untyped(res, &material);
        material
    }

    let mut resources = ChessMeshResources(Vec::new());
    let res = &mut resources;
    let mat = &mut *materials;
    let mes = &mut *meshes;

    commands.insert_resource(ChessMeshes {
        square_mesh: add_mesh(res, mes, Rectangle::new(1.0, 1.0)),
        light_material: add_color(res, mat, color_u8(0xEC, 0xD3, 0xB9)),
        dark_material: add_color(res, mat, color_u8(0xA1, 0x6F, 0x5A)),
        background_material: add_color(res, mat, color_u8(0x1F, 0x1F, 0x1F)),
        available_move_material: add_color(res, mat, color_f32(0.8, 0.05, 0.25).with_alpha(0.7)),
        threatened_square_material: add_color(res, mat, color_f32(0.0, 0.1, 0.5).with_alpha(0.3)),
        check_path_1_material: add_color(res, mat, color_u8(0xFF, 0x53, 0x04).with_alpha(0.7)),
        check_path_2_material: add_color(res, mat, color_u8(0xFF, 0x6D, 0x04).with_alpha(0.7)),
        check_path_3_material: add_color(res, mat, color_u8(0xFF, 0x39, 0x04).with_alpha(0.7)),
        pinned_square_material: add_color(res, mat, color_u8(0xFE, 0xDC, 0x00).with_alpha(0.7)),
    });
    commands.insert_resource(resources);
}

pub fn check_chess_meshes_loaded(
    mut loading_progress: ResMut<LoadBoardProgress>,
    resources: Res<ChessMeshResources>,
    meshes: Res<Assets<Mesh>>,
    materials: Res<Assets<ColorMaterial>>,
) {
    if loading_progress.meshes_loaded {
        return;
    }
    let mut loaded = 0;

    fn try_all_typed<'a, T: Asset>(
        items: impl IntoIterator<Item = &'a UntypedHandle>,
    ) -> Vec<Handle<T>> {
        items
            .into_iter()
            .map(Clone::clone)
            .map(UntypedHandle::try_typed)
            .filter(Result::is_ok)
            .collect::<Result<_, _>>()
            .unwrap()
    }

    let mesh_handles = try_all_typed(&resources.0);
    for handle in &mesh_handles {
        if meshes.get(handle).is_some() {
            loaded += 1;
        }
    }

    let material_handles = try_all_typed(&resources.0);
    for handle in &material_handles {
        if materials.get(handle).is_some() {
            loaded += 1;
        }
    }

    if loaded == resources.0.len() {
        loading_progress.meshes_loaded = true;
    }
}
