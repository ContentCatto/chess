use bevy::{
    input::{mouse::MouseButtonInput, ButtonState},
    prelude::*,
};

use crate::board::reparent_inplace;

use super::transform_rect;

#[derive(Clone, Copy, Debug, Component)]
pub struct Drag {
    pub relative_hitbox: Rect,
    dragging_enabled: bool,
    is_being_dragged: bool,
}

impl Drag {
    pub fn new(relative_hitbox: Rect) -> Self {
        Self {
            relative_hitbox,
            dragging_enabled: true,
            is_being_dragged: false,
        }
    }

    pub const fn is_being_dragged(&self) -> bool {
        self.is_being_dragged
    }

    pub const fn is_dragging_enabled(&self) -> bool {
        self.dragging_enabled
    }

    pub fn hitbox(&self, global_transform: &GlobalTransform) -> Rect {
        let translation = global_transform.translation();
        let hitbox_min = global_transform
            .transform_point(self.relative_hitbox.min.extend(translation.z))
            .xy();
        let hitbox_max = global_transform
            .transform_point(self.relative_hitbox.max.extend(translation.z))
            .xy();
        Rect::from_corners(hitbox_min, hitbox_max)
    }
}

#[derive(Clone, Copy, Debug, Event)]
pub struct DragStarted {
    pub entity: Entity,
    pub prev_parent: Entity,
    pub start_position: Vec3,
}

#[derive(Clone, Copy, Debug, Event)]
pub struct DragContinues {
    pub entity: Entity,
    pub prev_parent: Entity,
    pub delta: Vec2,
    pub start_position: Vec3,
    pub current_position: Vec3,
}

#[derive(Clone, Copy, Debug, Event)]
pub struct DragFinished {
    pub entity: Entity,
    pub prev_parent: Entity,
    pub start_position: Vec3,
    pub end_position: Vec3,
}

#[derive(Clone, Copy, Debug, Resource)]
pub struct CurrentDragInfo {
    held: Option<(Entity, Entity)>,
    drag_parent: Entity,
    last_relative_position: Option<Vec2>,
    start_position: Vec3,
}

impl CurrentDragInfo {
    pub fn new(drag_parent: Entity) -> Self {
        Self {
            held: None,
            drag_parent,
            last_relative_position: None,
            start_position: Vec3::ZERO,
        }
    }
}

#[derive(Debug, Clone, Copy, Component)]
pub struct UpdateDraggingEnabled(pub bool);

pub(crate) fn update_dragging_enabled(
    mut commands: Commands,
    mut draggables: Query<(Entity, &mut Drag, &UpdateDraggingEnabled)>,
) {
    for (entity, mut drag, update) in draggables.iter_mut() {
        drag.dragging_enabled = update.0;
        commands.entity(entity).remove::<UpdateDraggingEnabled>();
    }
}

pub(crate) fn handle_dragging(
    mut commands: Commands,

    mut current: ResMut<CurrentDragInfo>,
    mut started: EventWriter<DragStarted>,
    mut continues: EventWriter<DragContinues>,
    mut finished: EventWriter<DragFinished>,

    mut mouse_moved: EventReader<CursorMoved>,
    mut mouse_button: EventReader<MouseButtonInput>,
    mut mouse_entered: EventReader<CursorEntered>,
    mut mouse_left: EventReader<CursorLeft>,

    mut draggables: Query<(Entity, &GlobalTransform, &mut Transform, &mut Drag)>,
    global_transforms: Query<&GlobalTransform>,
    parents: Query<&Parent>,

    cameras: Query<(&Camera, &GlobalTransform)>,
    windows: Query<&Window>,
) {
    const FLIP_Y: Vec2 = Vec2::new(1.0, -1.0);
    fn cursor_world_pos(
        window: &Window,
        cameras: &Query<(&Camera, &GlobalTransform)>,
    ) -> Option<Vec2> {
        let (camera, camera_transform) = cameras.single();

        let pixel_pos = window.cursor_position()?;
        let pos = camera
            .viewport_to_world_2d(camera_transform, pixel_pos)
            .expect("Should not fail to compute");
        Some(pos)
    }

    fn start_dragging(
        commands: &mut Commands,
        current: &mut ResMut<CurrentDragInfo>,
        started: &mut EventWriter<DragStarted>,
        draggables: &mut Query<(Entity, &GlobalTransform, &mut Transform, &mut Drag)>,
        global_transforms: &Query<&GlobalTransform>,
        parents: &Query<&Parent>,

        dragged: Entity,
    ) {
        let (.., mut transform, mut drag) = draggables
            .get_mut(dragged)
            .expect("We know entity already exists");

        current.start_position = transform.translation;

        let prev_parent = parents.get(dragged).expect("Must have a parent").get();

        reparent_inplace(
            commands,
            &mut transform,
            &global_transforms,
            dragged,
            current.drag_parent,
        );

        current.held = Some((dragged, prev_parent));
        current.last_relative_position = None;
        drag.is_being_dragged = true;

        tracing::debug!("Started dragging {dragged:?} with previous parent {prev_parent:?}.");
        started.send(DragStarted {
            entity: dragged,
            prev_parent,
            start_position: current.start_position,
        });
    }

    fn finish_dragging(
        commands: &mut Commands,
        current: &mut ResMut<CurrentDragInfo>,
        finished: &mut EventWriter<DragFinished>,
        draggables: &mut Query<(Entity, &GlobalTransform, &mut Transform, &mut Drag)>,
        global_transforms: &Query<&GlobalTransform>,
    ) {
        let Some((entity, prev_parent)) = current.held.take() else {
            tracing::error!("Can't finish drag while not holding anything.");
            return;
        };
        current.last_relative_position = None;

        let (.., mut transform, mut drag) = match draggables.get_mut(entity) {
            Ok(components) => components,
            Err(error) => {
                tracing::error!("Failed to get dragged entity: {error}");
                return;
            }
        };

        drag.is_being_dragged = false;

        tracing::debug!("Finished dragging {entity:?}, reassigning parent {prev_parent:?}.",);
        reparent_inplace(
            commands,
            &mut transform,
            &global_transforms,
            entity,
            prev_parent,
        );
        finished.send(DragFinished {
            entity,
            prev_parent,
            start_position: current.start_position,
            end_position: transform.translation,
        });
    }

    fn abort_dragging(
        commands: &mut Commands,
        current: &mut ResMut<CurrentDragInfo>,
        draggables: &mut Query<(Entity, &GlobalTransform, &mut Transform, &mut Drag)>,
        global_transforms: &Query<&GlobalTransform>,
    ) {
        current.last_relative_position = None;

        let Some((entity, prev_parent)) = current.held.take() else {
            tracing::debug!("No entity is being dragged.");
            return;
        };

        if let Ok((.., mut transform, mut drag)) = draggables.get_mut(entity) {
            drag.is_being_dragged = false;

            reparent_inplace(
                commands,
                &mut transform,
                &global_transforms,
                entity,
                prev_parent,
            );
        } else {
            tracing::warn!("Held entity is not draggable.");
        }
    }

    fn find_draggable_under_cursor(
        draggables: &Query<(Entity, &GlobalTransform, &mut Transform, &mut Drag)>,
        cameras: &Query<(&Camera, &GlobalTransform)>,

        window: &Window,
    ) -> Option<Entity> {
        let Some(pos) = cursor_world_pos(window, &cameras) else {
            tracing::debug!("Cursor is not over this specific window");
            return None;
        };

        let mut frontmost_entity = None;
        for (entity, global_transform, _transform, drag) in draggables.iter() {
            if !drag.dragging_enabled {
                continue;
            }
            let hitbox = drag.hitbox(global_transform);
            if !hitbox.contains(pos) {
                continue;
            }
            let z = global_transform.translation().z;

            if let Some((_other_entity, other_z)) = frontmost_entity {
                if other_z <= z {
                    continue;
                }
            }

            frontmost_entity = Some((entity, z));
        }

        tracing::debug!("Found draggables: {frontmost_entity:?}");

        frontmost_entity.unzip().0
    }

    if let Some((dragged, ..)) = current.held {
        if let Ok((.., drag)) = draggables.get(dragged) {
            if !drag.is_dragging_enabled() {
                tracing::info!("Entity is no longer draggable, so dropping it as-is: {dragged:?}");
                abort_dragging(
                    &mut commands,
                    &mut current,
                    &mut draggables,
                    &global_transforms,
                );
            }
        } else {
            tracing::warn!("Can't find entity currently being dragged: {dragged:?}");
        }
    }

    for event in mouse_button.read() {
        match event {
            MouseButtonInput {
                button: MouseButton::Left,
                state: ButtonState::Pressed,
                // TODO: Handle multiple windows.
                window,
            } => {
                if let Some((dragged, _)) = current.held {
                    tracing::warn!("Starting new drag while still holding old object {dragged:?}");
                    abort_dragging(
                        &mut commands,
                        &mut current,
                        &mut draggables,
                        &global_transforms,
                    );
                }

                let window = windows.get(*window).expect("Window should exist");
                let Some(dragged) = find_draggable_under_cursor(&draggables, &cameras, window)
                else {
                    tracing::debug!("Clicked nothing.");
                    continue;
                };

                start_dragging(
                    &mut commands,
                    &mut current,
                    &mut started,
                    &mut draggables,
                    &global_transforms,
                    &parents,
                    dragged,
                );
            }
            MouseButtonInput {
                button: MouseButton::Left,
                state: ButtonState::Released,
                // TODO: Handle multiple windows.
                window: _window,
            } => {
                if current.held.is_none() {
                    tracing::debug!("Finishing drag while not holding anything.");
                    abort_dragging(
                        &mut commands,
                        &mut current,
                        &mut draggables,
                        &global_transforms,
                    );
                    continue;
                };

                finish_dragging(
                    &mut commands,
                    &mut current,
                    &mut finished,
                    &mut draggables,
                    &global_transforms,
                );
            }
            _ => {}
        }
    }

    for _event in mouse_left.read() {
        tracing::trace!("Cursor left window.");
    }

    for event in mouse_entered.read() {
        tracing::trace!("Cursor entered window.");

        let Some((entity, _prev_parent)) = current.held else {
            tracing::trace!("Entered window while not dragging anything.");
            continue;
        };

        let (.., mut transform, _drag) = match draggables.get_mut(entity) {
            Ok(components) => components,
            Err(error) => {
                tracing::error!("Drag entity does not have a transform: {error}");
                continue;
            }
        };
        let window = windows.get(event.window).expect("Window is a window");

        let Some(cursor_pos) = cursor_world_pos(window, &cameras) else {
            tracing::debug!("Cursor left window again before drag could be caught up.");
            continue;
        };

        let Some(last_relative_pos) = current.last_relative_position else {
            tracing::info!(
                "Can't snap dragged entity to cursor, no recorded last relative position.",
            );
            continue;
        };

        let snap_pos = (cursor_pos + last_relative_pos).extend(transform.translation.z);
        tracing::debug!(
            "Snapping held object from position {} to position {snap_pos} because cursor re-entered window.",
            transform.translation,
        );

        transform.translation = snap_pos;
    }

    for event in mouse_moved.read() {
        let Some((entity, prev_parent)) = current.held else {
            continue;
        };
        let window = windows.get(event.window).expect("Window must exist");

        let (.., mut transform, _drag) = match draggables.get_mut(entity) {
            Ok(components) => components,
            Err(error) => {
                tracing::error!("Drag entity does not have a transform: {error}");
                continue;
            }
        };

        let delta = event.delta.unwrap_or(Vec2::ZERO) * FLIP_Y;

        transform.translation += delta.extend(0.0);
        if let Some(pos) = cursor_world_pos(window, &cameras) {
            current.last_relative_position = Some(transform.translation.xy() - pos);
        }

        tracing::trace!("Continuing dragging {entity:?}, moved it by {delta}");
        continues.send(DragContinues {
            entity,
            prev_parent,
            delta,
            start_position: current.start_position,
            current_position: transform.translation,
        });
    }
}

pub(crate) fn draw_draggables(mut gizmos: Gizmos, draggables: Query<(&GlobalTransform, &Drag)>) {
    for (global_transform, drag) in draggables.iter() {
        let (hitbox, rotation) = transform_rect(drag.relative_hitbox, global_transform);
        gizmos.rect_2d(
            Isometry2d {
                rotation: Rot2::radians(rotation),
                translation: hitbox.center(),
            },
            hitbox.size(),
            LinearRgba {
                red: 1.0,
                green: 0.0,
                blue: 1.0,
                alpha: 1.0,
            },
        );
    }
}
