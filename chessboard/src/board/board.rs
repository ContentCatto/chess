use std::{
    collections::HashMap,
    fmt::{Debug, Display},
    sync::atomic::{self, AtomicBool},
    time::{Duration, Instant},
};

use bevy::prelude::*;
use bevy_egui::{
    egui::{self, Align2, Id, Response},
    EguiContexts,
};

use brain::Brain;
use chers::{
    bitboard::BitBoard, ByColor, ColPiece, Move, MovePart, Movement, PieceColor, PieceKind, Pos,
    Promotion, RawBoard, RawMoveOutcome,
};

use super::{
    board_grid::{add_board_grid, get_pos_from_row_and_col_index, BoardGrid, GridSquare},
    debug::draw_cross_gizmo,
    dragging::{Drag, DragFinished, DragStarted},
    meshes::ChessMeshes,
    snapping::{Slot, SnapToSlot, SnappedToSlot},
    sounds::ChessSounds,
    sprites::ChessSprites,
    transform_rect, z_ordering, DebugEnabled,
};

#[derive(Component)]
pub struct Piece(pub ColPiece);

pub fn create_raw_board_from_board_visuals(board: &Board, pieces: &Query<&Piece>) -> RawBoard {
    let pieces = board.piece_entities.iter().copied().map(|piece_entity| {
        piece_entity.map(|entity| pieces.get(entity).expect("Piece entites must exist"))
    });

    let pieces = pieces.map(|piece| piece.map(|piece| piece.0));
    let mut raw_board = RawBoard::new();
    for (index, piece) in pieces.enumerate() {
        let pos = Pos::from_flat_index_likely(index as u8).expect("Pos should be valid");
        raw_board[pos] = piece;
    }

    raw_board
}

fn validate_board_is_visually_in_sync(board: &Board, pieces: &Query<&Piece>) -> bool {
    let visual_board = create_raw_board_from_board_visuals(board, pieces);
    let actual_board = board.board.raw_board();

    if visual_board.pieces().collect::<Vec<_>>() != actual_board.pieces().collect::<Vec<_>>() {
        tracing::error!("Board visuals do not match actual board.\nVisuals:\n{visual_board:?}\nActual board:\n{actual_board:?}");
        false
    } else {
        true
    }
}

pub(crate) fn update_hovered_piece(
    mut drag_started: EventReader<DragStarted>,
    mut drag_finished: EventReader<DragFinished>,

    pieces: Query<&Piece, With<Drag>>,
    boards: Query<&Board>,
    mut highlights: Query<&mut BitBoardHighlightEntities>,
    grid_squares: Query<&GridSquare>,

    parents: Query<&Parent>,
) {
    for &DragFinished {
        entity,
        prev_parent,
        ..
    } in drag_finished.read()
    {
        if !pieces.contains(entity) {
            continue;
        }
        let grid_square = grid_squares
            .get(prev_parent)
            .expect("Pieces must only snap from grid squares");
        let board_entity = parents
            .get(grid_square.board_grid)
            .expect("BoardGrid must have a parent")
            .get();
        let board = boards
            .get(board_entity)
            .expect("Parent entity of a BoardGrid must be a Board");
        let mut highlights = highlights
            .get_mut(board_entity)
            .expect("Board entites must have a BitBoardHighlightEntities");

        board.clear_hovered_piece(&mut highlights);
        highlights.needs_visual_update = true;
    }

    for &DragStarted {
        entity,
        prev_parent,
        ..
    } in drag_started.read()
    {
        if !pieces.contains(entity) {
            continue;
        }
        let grid_square = grid_squares
            .get(prev_parent)
            .expect("Pieces must only snap from grid squares");
        let board_entity = parents
            .get(grid_square.board_grid)
            .expect("BoardGrid must have a parent")
            .get();
        let board = boards
            .get(board_entity)
            .expect("Parent entity of a BoardGrid must be a Board");
        let mut highlights = highlights
            .get_mut(board_entity)
            .expect("Board entites must have a BitBoardHighlightEntities");

        board.set_hovered_piece(&mut highlights, grid_square.pos);
        highlights.needs_visual_update = true;
    }
}

#[derive(Debug, Clone, Copy, Event)]
pub(crate) struct PieceMoved {
    pub mov: Move,

    pub drag_event: DragFinished,
    pub piece_entity: Entity,
    #[allow(unused)]
    pub prev_grid_square_entity: Entity,
    #[allow(unused)]
    pub new_grid_square_entity: Entity,
    pub board_entity: Entity,
}

pub(crate) fn detect_piece_move(
    mut commands: Commands,

    mut snap_events: EventReader<SnappedToSlot>,
    mut promotion_picked_events: EventReader<PickedPromotion>,
    mut piece_moved_events: EventWriter<PieceMoved>,

    pieces: Query<&Piece, With<Drag>>,
    mut boards: Query<&mut Board>,
    grid_squares: Query<&GridSquare>,

    parents: Query<&Parent>,
) {
    for &SnappedToSlot {
        entity: piece_entity,
        slot_rect: _,
        slot_entity: new_grid_square_entity,
        drag_event,
    } in snap_events.read()
    {
        let Ok(piece) = pieces.get(piece_entity) else {
            continue;
        };

        let prev_grid_square_entity = drag_event.prev_parent;
        let prev_grid_square = grid_squares
            .get(prev_grid_square_entity)
            .expect("Pieces must only snap from grid squares");

        let new_grid_square = grid_squares
            .get(new_grid_square_entity)
            .expect("Pieces must only snap to grid squares");

        let board_grid_parent = parents
            .get(new_grid_square.board_grid)
            .expect("BoardGrid must have a parent")
            .get();
        let board = boards
            .get_mut(board_grid_parent)
            .expect("Parent entity of a BoardGrid must be a Board");

        let movement = Movement {
            from: prev_grid_square.pos,
            to: new_grid_square.pos,
        };
        let mov = Move::new(movement);
        let move_data = PieceMoved {
            mov,
            drag_event,
            piece_entity,
            prev_grid_square_entity,
            new_grid_square_entity,
            board_entity: board_grid_parent,
        };

        if would_be_valid_promotion_move(&board.board, piece.0, mov) {
            // FIXME: Actually get cursor position.
            let cursor_position = Vec2::new(50.0, 50.0);
            commands.spawn(PromotionPicker {
                position: cursor_position,
                partial_move_data: move_data,
            });
            continue;
        }

        piece_moved_events.send(move_data);
    }

    for &PickedPromotion(move_data) in promotion_picked_events.read() {
        piece_moved_events.send(move_data);
    }
}

fn would_be_valid_promotion_move(board: &chers::Board, piece: ColPiece, mov: Move) -> bool {
    if piece.kind() != PieceKind::Pawn {
        return false;
    }

    let promotion_rank = match piece.color() {
        PieceColor::White => 8,
        PieceColor::Black => 1,
    };

    if mov.movement().to.rank() != promotion_rank {
        return false;
    }

    board.is_move_pseudolegal(mov)
}

pub(crate) fn handle_piece_move(
    mut commands: Commands,
    sounds: Res<ChessSounds>,
    chess_sprites: Res<ChessSprites>,

    mut move_events: EventReader<PieceMoved>,
    mut pieces: Query<&mut Piece, With<Drag>>,
    mut boards: Query<&mut Board>,
    board_grids: Query<&BoardGrid>,
    mut highlights: Query<&mut BitBoardHighlightEntities>,

    mut transforms: Query<&mut Transform>,
    mut sprites: Query<&mut Sprite>,
    children: Query<&Children>,
) {
    for &PieceMoved {
        mov,
        drag_event,
        piece_entity,
        prev_grid_square_entity: _,
        new_grid_square_entity: _,
        board_entity,
    } in move_events.read()
    {
        let mut board = boards
            .get_mut(board_entity)
            .expect("board_entity of a PieceMoved event must have a Board component");
        let board_grid = board_grids
            .get(board.grid)
            .expect("Board.grid must have a BoardGrid component");

        let mut highlights = highlights
            .get_mut(board_entity)
            .expect("Board entity must have a BitBoardHighlightEntities component");

        let moving_piece = board
            .board
            .raw
            .index(mov.movement().from)
            .expect("Can't move from a square that has no piece");
        let moving_piece_kind = moving_piece.kind();

        let outcome = board.try_make_move(
            &mut commands,
            &chess_sprites,
            &mut transforms,
            &mut pieces.transmute_lens::<&mut Piece>().query(),
            &children,
            &mut sprites,
            mov,
            board_grid,
            &mut highlights,
        );

        let Some(outcome) = outcome else {
            tracing::info!("Invalid move: {moving_piece_kind:#?} {mov}");

            commands
                .entity(piece_entity)
                .set_parent(drag_event.prev_parent);
            let mut transform = transforms
                .get_mut(piece_entity)
                .expect("Piece entity must have a transform");
            transform.translation = drag_event.start_position;

            continue;
        };
        tracing::info!("Performed move: {moving_piece_kind:#?} {mov}");

        if outcome.is_capture() {
            commands.spawn(sounds.capture_sound());
        } else if outcome.is_promotion() {
            commands.spawn(sounds.move_sound());
        } else {
            commands.spawn(sounds.move_sound());
        }

        if board.board.is_game_finished() {
            // NOTE: Might not actually be a win.
            let win_color = !board.board.turn_color;
            let win_player = &board.mtch.players[win_color];

            if board.board.is_draw() {
                tracing::info!("Game over: Draw");
                commands.spawn(sounds.draw_sound());
            } else {
                tracing::info!("Game over: Victory for {win_color:#?} ({win_player})");
                if board.is_human_win() {
                    commands.spawn(sounds.victory_sound());
                } else if board.is_human_loss() {
                    commands.spawn(sounds.defeat_sound());
                } else {
                    commands.spawn(sounds.victory_sound());
                }
            }
        }

        if !board.board.is_game_finished() {
            commands.entity(board_entity).insert(RequestNextMove);
        }
    }
}

#[derive(Debug, Clone, Copy, Component)]
pub struct RequestNextMove;

pub(crate) fn fetch_next_move(
    mut commands: Commands,
    mut boards: Query<&mut Board>,

    board_grids: Query<&BoardGrid>,
    parents: Query<&Parent>,
    transforms: Query<&Transform>,

    move_requests: Query<(Entity, &RequestNextMove)>,
    mut move_events: EventWriter<PieceMoved>,
) {
    for (board_entity, RequestNextMove) in move_requests.iter() {
        commands.entity(board_entity).remove::<RequestNextMove>();
        let mut board = boards
            .get_mut(board_entity)
            .expect("Tried to get next move for board that doesn't exist");
        let current_turn = board.board.turn_color;

        let mov = {
            let (mtch, board) = board.match_and_board();
            let current_ai_player = match &mut mtch.players[current_turn] {
                Player::Human => continue,
                Player::Ai(brain) => &mut **brain,
            };

            let end_search = AtomicBool::new(false);
            let search_start = Instant::now();
            const SEARCH_TIME: Duration = Duration::from_secs(5);

            let response = std::thread::scope(|scope| {
                let search_thread_handle =
                    scope.spawn(|| current_ai_player.next_move(&board, &end_search));

                while !search_thread_handle.is_finished() {
                    if search_start.elapsed() > SEARCH_TIME {
                        // FIXME: Trace level.
                        tracing::info!("Telling search thread to end the search");
                        end_search.store(true, atomic::Ordering::Release);
                        break;
                    }
                    // FIXME: Trace level.
                    tracing::info!("Waiting for search thread");
                    std::thread::sleep(Duration::from_millis(10));
                }
                // FIXME: Trace level.
                tracing::info!("Joining search thread");
                search_thread_handle
                    .join()
                    .expect("Search thread should not panic")
            });
            match response {
                brain::Response::Move(mov) => mov,
                brain::Response::Forfeit => {
                    tracing::info!("{current_turn:#?} forfeits the game.");
                    continue;
                }
            }
        };

        let board_grid = board_grids
            .get(board.grid)
            .expect("Board.grid must have a BoardGrid component");

        let Some(from_entity) = board.piece_entities[mov.movement().from.to_flat_index() as usize]
        else {
            tracing::error!("Returned move from position does not have a piece.");
            continue;
        };
        let from_square_entity = parents
            .get(from_entity)
            .expect("Existing piece entity should have a parent")
            .get();

        let to_square_entity = board_grid.slots[mov.movement().to.to_flat_index() as usize];

        move_events.send(PieceMoved {
            mov,
            drag_event: DragFinished {
                entity: from_entity,
                prev_parent: from_square_entity,
                start_position: transforms
                    .get(from_entity)
                    .expect("Existing piece entity should have a parent")
                    .translation,
                // Shouldn't matter.
                end_position: Vec3::ZERO,
            },
            piece_entity: from_entity,
            prev_grid_square_entity: from_square_entity,
            new_grid_square_entity: to_square_entity,
            board_entity,
        });
    }
}

#[derive(Component)]
pub(crate) struct AddPieces;

#[derive(Component)]
pub(crate) struct RemovePieces;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct BitBoardHighlights {
    available_moves: BitBoard,
    threatened_squares: BitBoard,
    check_path_1: BitBoard,
    check_path_2: BitBoard,
    check_path_3: BitBoard,
    pinned_squares: BitBoard,
}

#[derive(Debug, Clone, PartialEq, Eq, Component)]
pub struct BitBoardHighlightEntities {
    bitboards: BitBoardHighlights,
    needs_visual_update: bool,
    hidden: bool,

    available_move_entities: Box<[Entity; 8 * 8]>,
    threatened_square_entities: Box<[Entity; 8 * 8]>,
    check_path_1_entities: Box<[Entity; 8 * 8]>,
    check_path_2_entities: Box<[Entity; 8 * 8]>,
    check_path_3_entities: Box<[Entity; 8 * 8]>,
    pinned_square_entities: Box<[Entity; 8 * 8]>,
}

impl BitBoardHighlightEntities {
    pub fn new_empty(commands: &mut Commands, chess_meshes: &ChessMeshes) -> Self {
        let mut available_move_entities = Vec::with_capacity(8 * 8);
        let mut threatened_square_entities = Vec::with_capacity(8 * 8);
        let mut check_path_1_entities = Vec::with_capacity(8 * 8);
        let mut check_path_2_entities = Vec::with_capacity(8 * 8);
        let mut check_path_3_entities = Vec::with_capacity(8 * 8);
        let mut pinned_square_entities = Vec::with_capacity(8 * 8);

        let mut add_invisible = |list: &mut Vec<Entity>, (mesh, mat, transform)| {
            list.push(
                commands
                    .spawn((mesh, mat, transform, Visibility::Hidden))
                    .id(),
            );
        };

        for row in (0..8).rev() {
            for col in 0..8 {
                let pos = get_pos_from_row_and_col_index(col, row);
                let xy = Vec2::new(pos.x, pos.y);
                let transform_with_z = |pos_z: f32| Transform::from_translation(xy.extend(pos_z));

                add_invisible(
                    &mut available_move_entities,
                    chess_meshes.get_available_move_square(transform_with_z(
                        z_ordering::AVAILABLE_MOVE_SQUARE,
                    )),
                );
                add_invisible(
                    &mut threatened_square_entities,
                    chess_meshes
                        .get_threatened_square(transform_with_z(z_ordering::THREATENED_SQUARE)),
                );
                add_invisible(
                    &mut check_path_1_entities,
                    chess_meshes
                        .get_check_path_1_square(transform_with_z(z_ordering::CHECK_PATH_1_SQUARE)),
                );
                add_invisible(
                    &mut check_path_2_entities,
                    chess_meshes
                        .get_check_path_2_square(transform_with_z(z_ordering::CHECK_PATH_2_SQUARE)),
                );
                add_invisible(
                    &mut check_path_3_entities,
                    chess_meshes
                        .get_check_path_3_square(transform_with_z(z_ordering::CHECK_PATH_3_SQUARE)),
                );
                add_invisible(
                    &mut pinned_square_entities,
                    chess_meshes.get_pinned_square(transform_with_z(z_ordering::PINNED_SQUARE)),
                );
            }
        }

        let available_move_entities = available_move_entities
            .try_into()
            .expect("Should be exactly 64 squares");
        let threatened_square_entities = threatened_square_entities
            .try_into()
            .expect("Should be exactly 64 squares");
        let check_path_1_entities = check_path_1_entities
            .try_into()
            .expect("Should be exactly 64 squares");
        let check_path_2_entities = check_path_2_entities
            .try_into()
            .expect("Should be exactly 64 squares");
        let check_path_3_entities = check_path_3_entities
            .try_into()
            .expect("Should be exactly 64 squares");
        let pinned_square_entities = pinned_square_entities
            .try_into()
            .expect("Should be exactly 64 squares");

        Self {
            bitboards: BitBoardHighlights::none(),
            needs_visual_update: true,
            hidden: true,

            available_move_entities,
            threatened_square_entities,
            check_path_1_entities,
            check_path_2_entities,
            check_path_3_entities,
            pinned_square_entities,
        }
    }

    fn entities(&self) -> impl Iterator<Item = &[Entity; 8 * 8]> {
        [
            &*self.available_move_entities,
            &*self.threatened_square_entities,
            &*self.check_path_1_entities,
            &*self.check_path_2_entities,
            &*self.check_path_3_entities,
            &*self.pinned_square_entities,
        ]
        .into_iter()
    }

    fn update(&self, visibility: &mut Query<&mut Visibility>) {
        let mut set_visible = |bitboard: BitBoard, entites: &[Entity; 8 * 8]| {
            for (pos, should_be_visible) in bitboard.all_positions() {
                let entity = entites[pos.to_flat_index() as usize];
                let mut visibility = visibility
                    .get_mut(entity)
                    .expect("Should have Visibility component");

                *visibility = match should_be_visible {
                    true => Visibility::Inherited,
                    false => Visibility::Hidden,
                };
            }
        };

        for (&bitboard, entites) in [(
            &self.bitboards.available_moves,
            &*self.available_move_entities,
        )] {
            set_visible(bitboard, entites);
        }

        if !self.hidden {
            for (&bitboard, entites) in [
                (
                    &self.bitboards.threatened_squares,
                    &*self.threatened_square_entities,
                ),
                (&self.bitboards.check_path_1, &*self.check_path_1_entities),
                (&self.bitboards.check_path_2, &*self.check_path_2_entities),
                (&self.bitboards.check_path_3, &*self.check_path_3_entities),
                (
                    &self.bitboards.pinned_squares,
                    &*self.pinned_square_entities,
                ),
            ] {
                set_visible(bitboard, entites);
            }
        }
    }
}

impl Debug for BitBoardHighlights {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "available moves:\n{:?}", self.available_moves)
    }
}

impl BitBoardHighlights {
    pub fn none() -> BitBoardHighlights {
        Self {
            available_moves: BitBoard::none(),
            threatened_squares: BitBoard::none(),
            check_path_1: BitBoard::none(),
            check_path_2: BitBoard::none(),
            check_path_3: BitBoard::none(),
            pinned_squares: BitBoard::none(),
        }
    }
}

pub(crate) fn update_bitboard_highlight_visibility(
    debugging_enabled: Res<State<DebugEnabled>>,
    mut highlights: Query<&mut BitBoardHighlightEntities>,
    mut visibility: Query<&mut Visibility>,
) {
    for mut highlights in highlights.iter_mut() {
        highlights.hidden = !debugging_enabled.0;

        if !highlights.needs_visual_update {
            continue;
        }

        highlights.update(&mut visibility);
        highlights.needs_visual_update = false;
    }
}

#[derive(Debug)]
pub enum Player {
    Human,
    Ai(Box<dyn Brain>),
}

impl Player {
    pub const fn is_human(&self) -> bool {
        matches!(self, Player::Human)
    }

    pub const fn is_ai(&self) -> bool {
        matches!(self, Player::Ai(_))
    }
}

impl Display for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Player::Human => f.write_str("Human"),
            Player::Ai(brain) => <dyn Brain as Display>::fmt(&**brain, f),
        }
    }
}

#[derive(Debug)]
pub struct Match {
    /// AI playing as the specified player, if any. If [`None`], the human plays as that side.
    players: ByColor<Player>,
    /// TODO: Implement
    _god_mode: bool,
}

#[derive(Debug, Component)]
pub struct Board {
    mtch: Match,
    board: Box<chers::Board>,
    piece_entities: Box<[Option<Entity>; 8 * 8]>,
    grid: Entity,
}

impl Board {
    pub fn match_and_board(&mut self) -> (&mut Match, &mut chers::Board) {
        (&mut self.mtch, &mut self.board)
    }

    pub fn is_human_win(&self) -> bool {
        let win_color = !self.board.turn_color;
        self.board.is_checkmate() && self.mtch.players[win_color].is_human()
    }

    pub fn is_human_loss(&self) -> bool {
        let lose_color = self.board.turn_color;
        self.board.is_checkmate() && self.mtch.players[lose_color].is_human()
    }

    pub fn try_make_move(
        &mut self,
        commands: &mut Commands,
        chess_sprites: &Res<ChessSprites>,

        transforms: &mut Query<&mut Transform>,
        pieces: &mut Query<&mut Piece>,
        children: &Query<&Children>,
        sprites: &mut Query<&mut Sprite>,

        mov: Move,
        grid: &BoardGrid,
        highlights: &mut BitBoardHighlightEntities,
    ) -> Option<RawMoveOutcome> {
        if !validate_board_is_visually_in_sync(self, &pieces.transmute_lens::<&Piece>().query()) {
            return None;
        }

        let outcome = self.board.try_make_move(mov, true);
        let Some(outcome) = outcome else {
            return None;
        };

        for move_part in self.board.last_move.clone() {
            match move_part {
                MovePart::Movement(mov) => {
                    let from_entity = self.piece_entity(mov.from);
                    let to_entity = self.piece_entity(mov.to);
                    assert_ne!(from_entity, None);
                    assert_eq!(to_entity, None);

                    *self.piece_entity_mut(mov.to) = self.piece_entity_mut(mov.from).take();

                    let from_entity = from_entity.expect("Was just asserted to not be None");

                    let new_parent = grid.slots[mov.to.to_flat_index() as usize];
                    commands.entity(from_entity).set_parent(new_parent);

                    let mut transform = transforms
                        .get_mut(from_entity)
                        .expect("Piece entity must have a transform");
                    transform.translation = Vec3::ZERO;
                }
                MovePart::Captured(pos) => {
                    let entity = self
                        .piece_entity(pos)
                        .expect("Can't capture piece that isn't there");
                    commands.entity(entity).despawn_recursive();
                    *self.piece_entity_mut(pos) = None;
                }
                MovePart::Promoted(pos, promotion) => {
                    let entity = self
                        .piece_entity(pos)
                        .expect("Can't promote a piece that isn't there");
                    let mut piece = pieces
                        .get_mut(entity)
                        .expect("Piece entites must have Piece component");
                    piece.0 = piece.0.promoted(promotion);

                    let sprite_entity = *children
                        .get(entity)
                        .expect("Piece entity must have a child")
                        .get(0)
                        .unwrap();

                    let mut sprite = sprites
                        .get_mut(sprite_entity)
                        .expect("Sprite entity must have a Sprite component");

                    chess_sprites.update_sprite(piece.0, &mut sprite);
                }
            }
        }

        validate_board_is_visually_in_sync(self, &pieces.transmute_lens::<&Piece>().query());
        self.update_debug_info(highlights);

        Some(outcome)
    }

    pub(crate) fn piece_entity(&self, pos: Pos) -> Option<Entity> {
        self.piece_entities[pos.to_flat_index() as usize]
    }

    pub(crate) fn piece_entity_mut(&mut self, pos: Pos) -> &mut Option<Entity> {
        &mut self.piece_entities[pos.to_flat_index() as usize]
    }

    pub(crate) fn set_hovered_piece(&self, highlights: &mut BitBoardHighlightEntities, pos: Pos) {
        highlights.bitboards.available_moves = self.board.legal_move_squares_from(pos);
        tracing::trace!(
            "Updated hovered piece. Highlight is now:\n{:?}",
            highlights.bitboards,
        );
    }

    pub(crate) fn clear_hovered_piece(&self, highlights: &mut BitBoardHighlightEntities) {
        tracing::trace!("Clearing hovered piece");
        highlights.bitboards.available_moves = BitBoard::none();
    }

    fn update_debug_info(&self, highlights: &mut BitBoardHighlightEntities) {
        highlights.bitboards.threatened_squares =
            self.board.raw.threatened_squares()[!self.board.turn_color];

        let get_check_path = |index: usize| {
            self.board
                .raw
                .check_paths()
                .get(index)
                .copied()
                .unwrap_or(BitBoard::none())
        };

        highlights.bitboards.check_path_1 = get_check_path(0);
        highlights.bitboards.check_path_2 = get_check_path(1);
        highlights.bitboards.check_path_3 = get_check_path(2);

        let pinned_squares = self
            .board
            .raw
            .pin_paths()
            .values()
            .map(IntoIterator::into_iter)
            .flatten()
            .copied()
            .fold(BitBoard::none(), |acc, val| acc | val);

        highlights.bitboards.pinned_squares = pinned_squares;

        highlights.needs_visual_update = true;
    }
}

#[derive(Debug, Clone, Component)]
pub(crate) struct PromotionPicker {
    position: Vec2,
    partial_move_data: PieceMoved,
}

#[derive(Debug, Clone, Copy, Event)]
pub(crate) struct PickedPromotion(PieceMoved);

pub(crate) fn promotion_picker(
    mut commands: Commands,
    mut contexts: EguiContexts,
    mut picked: EventWriter<PickedPromotion>,
    pickers: Query<(Entity, &PromotionPicker)>,
) {
    // TODO: Iteration order is maybe undefined?
    for (picker_entity, picker) in pickers.iter() {
        let mut promotion = Promotion::Queen;

        egui::Window::new("Select promotion")
            .id(Id::new(picker_entity))
            .movable(false)
            .title_bar(false)
            .current_pos(Into::<(f32, f32)>::into(picker.position))
            .show(contexts.ctx_mut(), |ui| {
                ui.label("Select promotion:");
                let resp = [
                    ui.radio_value(&mut promotion, Promotion::Queen, "Queen"),
                    ui.radio_value(&mut promotion, Promotion::Rook, "Rook"),
                    ui.radio_value(&mut promotion, Promotion::Knight, "Knight"),
                    ui.radio_value(&mut promotion, Promotion::Bishop, "Bishop"),
                ];
                if resp.iter().any(Response::clicked) {
                    let mut move_data = picker.partial_move_data;
                    move_data.mov = Move::new_promote(move_data.mov.movement(), promotion);

                    picked.send(PickedPromotion(move_data));
                    commands.entity(picker_entity).despawn_recursive();
                }
            });

        break;
    }
}

#[derive(Component)]
pub(crate) struct BoardControls {
    pub fen_to_load: String,
}

impl Default for BoardControls {
    fn default() -> Self {
        Self {
            fen_to_load: chers::board::STARTING_POSITION_FEN.to_owned(),
        }
    }
}

pub(crate) fn update_board_controls(
    mut commands: Commands,
    mut contexts: EguiContexts,
    mut boards: Query<(Entity, &mut Board, &mut BoardControls)>,
) {
    // TODO: Iteration order is maybe undefined?
    for (board_entity, mut board, mut controls) in boards.iter_mut() {
        let fen = &mut controls.fen_to_load;
        egui::Window::new("Board controls")
            .id(Id::new(board_entity))
            .movable(false)
            .anchor(Align2::RIGHT_TOP, [-30.0, 30.0])
            .title_bar(true)
            .show(contexts.ctx_mut(), |ui| {
                ui.label("Set board to FEN");
                let fen_input_field = ui.text_edit_singleline(fen);
                if ui.button("Load FEN").clicked()
                    || fen_input_field.lost_focus() && ui.input(|i| i.key_pressed(egui::Key::Enter))
                {
                    if board.board.fill_from_fen(&fen).is_none() {
                        tracing::error!("Invalid FEN string.");
                        return;
                    }

                    tracing::info!("Setting board to FEN:  {fen}");

                    commands
                        .entity(board_entity)
                        .insert(RemovePieces)
                        .insert(AddPieces);
                }

                let board_fen = board.board.to_fen();
                ui.label("Current board FEN (click to copy):");
                if ui.label(&board_fen).clicked() {
                    match arboard::Clipboard::new() {
                        Ok(mut clipboard) => match clipboard.set_text(board_fen) {
                            Ok(()) => tracing::info!("Current board FEN copied to clipboard."),
                            Err(error) => {
                                tracing::error!("Failed to set clipboard contents: {error}")
                            }
                        },
                        Err(error) => tracing::error!("Failed to access clipboard: {error}"),
                    }
                }
            });
    }
}

pub fn add_board(
    tile_width: f32,
    add_controls_window: bool,
) -> impl Fn(Commands, Res<ChessMeshes>) {
    move |mut commands: Commands, chess_meshes: Res<ChessMeshes>| {
        let grid = add_board_grid(&mut commands, &chess_meshes);

        let board = Board {
            mtch: Match {
                players: ByColor {
                    white: Player::Human,
                    black: Player::Ai(Box::new(brain::alphabeta::AlphaBeta {
                        max_depth_plies: 20,
                    })),
                },
                _god_mode: false,
            },
            board: Box::new(chers::Board::STARTING_POSITION),
            piece_entities: Box::new([None; 8 * 8]),
            grid,
        };
        let mut highlights = BitBoardHighlightEntities::new_empty(&mut commands, &chess_meshes);
        board.update_debug_info(&mut highlights);

        let mut board = commands.spawn((
            Transform::from_scale(Vec3::new(tile_width, tile_width, 1.0)),
            board,
            AddPieces,
        ));
        board.add_child(grid);
        for children_group in highlights.entities() {
            board.add_children(children_group);
        }
        board.insert(highlights);

        if add_controls_window {
            board.insert(BoardControls::default());
        }
    }
}

pub(crate) fn remove_pieces(
    mut commands: Commands,
    mut boards: Query<(Entity, &mut Board), With<RemovePieces>>,
) {
    for (board_entity, mut board) in boards.iter_mut() {
        let mut entity_counts = HashMap::new();
        for &entity in &*board.piece_entities {
            if let Some(entity) = entity {
                let count = entity_counts.entry(entity).or_insert(0);
                *count += 1;
            }
        }

        if entity_counts.values().any(|&count| count != 1) {
            for (entity, count) in entity_counts {
                if count == 1 {
                    continue;
                }
                tracing::error!("Entity {entity:?} exists in multiple positions on the board.");
            }
        }

        for pos in Pos::positions() {
            let Some(piece_entity) = board.piece_entities[pos.to_flat_index() as usize].take()
            else {
                continue;
            };
            tracing::trace!("Despawning piece {piece_entity:?} at position {pos}");
            commands.entity(piece_entity).despawn_recursive();
        }

        commands.entity(board_entity).remove::<RemovePieces>();
    }
}

pub(crate) fn add_pieces(
    mut commands: Commands,
    chess_sprites: Res<ChessSprites>,
    mut boards: Query<(Entity, &mut Board), With<AddPieces>>,
    grids: Query<&BoardGrid>,
    mut highlights: Query<&mut BitBoardHighlightEntities>,
) {
    for (board_entity, mut board) in boards.iter_mut() {
        let grid = grids
            .get(board.grid)
            .expect("Board must have valid grid entity");

        for (pos, piece) in board.board.pieces().collect::<Vec<_>>() {
            let Some(piece) = piece else { continue };

            let grid_square = grid.slots[pos.to_flat_index() as usize];

            commands.entity(grid_square).with_children(|parent| {
                let sprite = chess_sprites.get_sprite_scaled(piece, Transform::IDENTITY, 1.0);
                let piece_entity = parent
                    .spawn((
                        Transform::IDENTITY,
                        Piece(piece),
                        Drag::new(Rect::from_center_size(Vec2::ZERO, Vec2::ONE)),
                        SnapToSlot::new(grid.slots.clone()),
                    ))
                    .with_children(|parent| {
                        parent.spawn(sprite);
                    })
                    .id();
                board.piece_entities[pos.to_flat_index() as usize] = Some(piece_entity);
            });
            tracing::trace!("Spawning piece at position {pos}");
        }

        let mut highlights = highlights
            .get_mut(board_entity)
            .expect("Board entity must have a BitBoardHighlightEntities component");
        board.update_debug_info(&mut highlights);

        commands.entity(board_entity).remove::<AddPieces>();
    }
}

pub(crate) fn draw_en_passant_pos(
    mut gizmos: Gizmos,
    boards: Query<&Board>,
    grids: Query<&BoardGrid>,
    grid_squares: Query<(&Slot, &GlobalTransform), With<GridSquare>>,
) {
    for board in boards.iter() {
        let Some(en_passant_pos) = board.board.en_passant_target_square else {
            continue;
        };

        let grid = grids
            .get(board.grid)
            .expect("Board must have a valid BoardGrid");

        let grid_square_entity = grid.slots[en_passant_pos.to_flat_index() as usize];
        let (grid_square_slot, grid_square_global_transform) = grid_squares
            .get(grid_square_entity)
            .expect("GridSquare must have a GlobalTransform and a Slot");

        let (rect, rotation) = transform_rect(grid_square_slot.0, grid_square_global_transform);

        draw_cross_gizmo(
            &mut gizmos,
            rect,
            rotation,
            0.8,
            LinearRgba {
                red: 1.0,
                green: 0.5,
                blue: 0.0,
                alpha: 1.0,
            },
        );
    }
}
