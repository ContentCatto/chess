use bevy::{audio::Volume, prelude::*, utils::HashMap};

use super::LoadBoardProgress;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum ChessSoundAction {
    Move,
    Capture,
    Check,
    Victory,
    Defeat,
    Draw,
}

#[derive(Resource)]
pub struct ChessSounds {
    sounds: HashMap<ChessSoundAction, Handle<AudioSource>>,
}

impl ChessSounds {
    // const PLAYBACK_SETTINGS: PlaybackSettings = PlaybackSettings {
    //     mode: bevy::audio::PlaybackMode::Despawn,
    //     // volume: Volume::new(0.5),  // Bevy BUG: Non-const fn
    //     speed: 1.0,
    //     paused: false,
    //     spatial: false,
    //     spatial_scale: None,
    // };

    #[inline(always)]
    fn playback_settings() -> PlaybackSettings {
        PlaybackSettings {
            mode: bevy::audio::PlaybackMode::Despawn,
            volume: Volume::new(0.5), // Bevy BUG: Non-const fn
            speed: 1.0,
            paused: false,
            spatial: false,
            spatial_scale: None,
        }
    }

    fn sound_with_playback_settings(
        &self,
        sound: ChessSoundAction,
        settings: PlaybackSettings,
    ) -> (AudioPlayer, PlaybackSettings) {
        (AudioPlayer::new(self.sounds[&sound].clone()), settings)
    }

    pub fn move_sound(&self) -> (AudioPlayer, PlaybackSettings) {
        self.sound_with_playback_settings(
            ChessSoundAction::Move,
            PlaybackSettings {
                ..Self::playback_settings()
            },
        )
    }

    pub fn capture_sound(&self) -> (AudioPlayer, PlaybackSettings) {
        self.sound_with_playback_settings(
            ChessSoundAction::Capture,
            PlaybackSettings {
                ..Self::playback_settings()
            },
        )
    }

    pub fn check_sound(&self) -> (AudioPlayer, PlaybackSettings) {
        self.sound_with_playback_settings(
            ChessSoundAction::Check,
            PlaybackSettings {
                ..Self::playback_settings()
            },
        )
    }

    pub fn victory_sound(&self) -> (AudioPlayer, PlaybackSettings) {
        self.sound_with_playback_settings(
            ChessSoundAction::Victory,
            PlaybackSettings {
                ..Self::playback_settings()
            },
        )
    }

    pub fn defeat_sound(&self) -> (AudioPlayer, PlaybackSettings) {
        self.sound_with_playback_settings(
            ChessSoundAction::Defeat,
            PlaybackSettings {
                ..Self::playback_settings()
            },
        )
    }

    pub fn draw_sound(&self) -> (AudioPlayer, PlaybackSettings) {
        self.sound_with_playback_settings(
            ChessSoundAction::Draw,
            PlaybackSettings {
                ..Self::playback_settings()
            },
        )
    }
}

pub(crate) fn load_chess_sounds(mut commands: Commands, asset_server: Res<AssetServer>) {
    let sounds = [
        (
            ChessSoundAction::Move,
            asset_server.load("lichess/sound/standard/Move.ogg"),
        ),
        (
            ChessSoundAction::Capture,
            asset_server.load("lichess/sound/standard/Capture.ogg"),
        ),
        (
            ChessSoundAction::Check,
            asset_server.load("lichess/sound/standard/Check.ogg"),
        ),
        (
            ChessSoundAction::Victory,
            asset_server.load("lichess/sound/standard/Victory.ogg"),
        ),
        (
            ChessSoundAction::Defeat,
            asset_server.load("lichess/sound/standard/Defeat.ogg"),
        ),
        (
            ChessSoundAction::Draw,
            asset_server.load("lichess/sound/standard/Draw.ogg"),
        ),
    ]
    .into();

    let chess_sounds = ChessSounds { sounds };
    commands.insert_resource(chess_sounds);
}

pub(crate) fn check_chess_sounds_loaded(
    mut loading_progress: ResMut<LoadBoardProgress>,
    chess_sounds: Res<ChessSounds>,
    asset_server: Res<AssetServer>,
) {
    if loading_progress.sound_loaded {
        return;
    }

    if chess_sounds
        .sounds
        .values()
        .all(|handle| asset_server.is_loaded_with_dependencies(handle))
    {
        tracing::debug!("Chess sounds loaded.");
        loading_progress.sound_loaded = true;
    }
}
