pub mod board;
pub mod board_grid;
pub mod debug;
pub mod dragging;
pub mod meshes;
pub mod snapping;
pub mod sounds;
pub mod sprites;
pub mod z_ordering;

use bevy::prelude::*;
use bevy_egui::EguiPlugin;
use board::fetch_next_move;

use self::{
    board::{
        add_pieces, detect_piece_move, draw_en_passant_pos, handle_piece_move, promotion_picker,
        remove_pieces, update_bitboard_highlight_visibility, update_board_controls,
        update_hovered_piece, PickedPromotion, PieceMoved,
    },
    board_grid::draw_chesspos_flat_index,
    dragging::{
        draw_draggables, handle_dragging, update_dragging_enabled, CurrentDragInfo, DragContinues,
        DragFinished, DragStarted,
    },
    meshes::{check_chess_meshes_loaded, load_chess_meshes},
    snapping::{draw_drag_slots, handle_snap_to_slot, FailedToSnapToSlot, SnappedToSlot},
    sounds::{check_chess_sounds_loaded, load_chess_sounds},
    sprites::{check_chess_sprites_loaded, load_chess_sprites},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, States, Default)]
pub enum LoadingStage {
    #[default]
    Loading,
    Loaded,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default, Resource)]
pub struct LoadBoardProgress {
    pub sprites_loaded: bool,
    pub meshes_loaded: bool,
    pub sound_loaded: bool,
}

impl LoadBoardProgress {
    pub fn all_done(&self) -> bool {
        self.sprites_loaded && self.meshes_loaded && self.sound_loaded
    }
}

pub fn transform_rect(rect: Rect, global_transform: &GlobalTransform) -> (Rect, f32) {
    let (translation, rotation, _scale) = global_transform.to_scale_rotation_translation();
    let new_rect_min = global_transform
        .transform_point(rect.min.extend(translation.z))
        .xy();
    let new_rect_max = global_transform
        .transform_point(rect.max.extend(translation.z))
        .xy();
    (
        Rect::from_corners(new_rect_min, new_rect_max),
        rotation.to_euler(EulerRot::XYZ).2,
    )
}

pub fn draw_rect(
    gizmos: &mut Gizmos,
    global_transform: &GlobalTransform,
    rect: Rect,
    color: impl Into<Color>,
) {
    let (hitbox, rotation) = transform_rect(rect, global_transform);
    gizmos.rect_2d(
        Isometry2d {
            rotation: Rot2::radians(rotation),
            translation: hitbox.center(),
        },
        hitbox.size(),
        color,
    );
}

/// Change the parent of an entity while preserving its global transformation,
/// meaning its GlobalTransform stays the same.
pub fn reparent_inplace(
    commands: &mut Commands,
    transform: &mut Transform,
    global_transforms: &Query<&GlobalTransform>,
    entity: Entity,
    new_parent: Entity,
) {
    let current_global_transform = global_transforms
        .get(entity)
        .expect("Must have a global transform");
    let new_parent_global_transform = global_transforms
        .get(new_parent)
        .expect("Must have a global transform");

    tracing::debug!(
        "Changing parent of {entity:?} with position {} to {new_parent:?}",
        current_global_transform.translation(),
    );

    *transform = current_global_transform.reparented_to(new_parent_global_transform);
    commands.entity(entity).set_parent(new_parent);
}

fn check_loading_done(
    loading_progress: Res<LoadBoardProgress>,
    mut state: ResMut<NextState<LoadingStage>>,
) {
    if loading_progress.all_done() {
        state.set(LoadingStage::Loaded);
    } else {
        tracing::debug!("Progress: {:?}", *loading_progress);
    }
}

fn setup_load_progress(mut commands: Commands) {
    commands.insert_resource(LoadBoardProgress::default())
}

fn print_loaded() {
    tracing::info!("Asset loading complete");
}

pub struct BoardPlugin;

impl Plugin for BoardPlugin {
    fn build(&self, app: &mut App) {
        let loading_sub_checks = (
            check_chess_sprites_loaded,
            check_chess_meshes_loaded,
            check_chess_sounds_loaded,
        );
        let loading_overall_check = (loading_sub_checks, check_loading_done).chain();
        app
            .init_state::<LoadingStage>()
            .init_asset::<AudioSource>()
            .add_systems(OnEnter(LoadingStage::Loading), setup_load_progress)
            .add_systems(OnEnter(LoadingStage::Loading), (load_chess_sprites, load_chess_meshes, load_chess_sounds))
            .add_systems(Update, loading_overall_check.run_if(in_state(LoadingStage::Loading)))
            .add_systems(OnEnter(LoadingStage::Loaded), print_loaded)
            .add_systems(Update, update_board_controls)
            .add_systems(Update, remove_pieces.after(update_board_controls))
            .add_systems(Update, add_pieces.after(remove_pieces))
        // let it return
        ;
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, States)]
pub struct DebugEnabled(pub bool);

impl Default for DebugEnabled {
    fn default() -> Self {
        Self(false)
    }
}

fn toggle_debug_system(
    currently_enabled: Res<State<DebugEnabled>>,
    mut should_be_enabled: ResMut<NextState<DebugEnabled>>,
    keyboard: Res<ButtonInput<KeyCode>>,
) {
    if keyboard.just_pressed(KeyCode::Tab) {
        should_be_enabled.set(DebugEnabled(!currently_enabled.0))
    }
}

pub struct BoardInteraction;

impl Plugin for BoardInteraction {
    fn build(&self, app: &mut App) {
        let drag_parent = app
            .world_mut()
            .spawn((Transform::IDENTITY, Visibility::Inherited))
            .id();
        app.add_plugins(EguiPlugin)
            .init_state::<DebugEnabled>()
            .add_event::<DragStarted>()
            .add_event::<DragContinues>()
            .add_event::<DragFinished>()
            .add_event::<SnappedToSlot>()
            .add_event::<FailedToSnapToSlot>()
            .add_event::<PickedPromotion>()
            .add_event::<PieceMoved>()
            .insert_resource(CurrentDragInfo::new(drag_parent))
        // let it return
        ;
        app.add_systems(
            Update,
            (
                toggle_debug_system,
                (
                    draw_chesspos_flat_index,
                    draw_draggables,
                    draw_drag_slots,
                    draw_en_passant_pos,
                )
                    .chain()
                    .run_if(in_state(DebugEnabled(true))),
                promotion_picker,
                update_dragging_enabled,
                handle_dragging.after(update_dragging_enabled),
                update_hovered_piece.after(handle_dragging),
                update_bitboard_highlight_visibility.after(update_hovered_piece),
                handle_snap_to_slot.after(handle_dragging),
                detect_piece_move
                    .after(promotion_picker)
                    .after(handle_snap_to_slot),
                handle_piece_move.after(detect_piece_move),
                fetch_next_move
                    // .run_if(bevy::time::common_conditions::on_timer(
                    //     Duration::from_secs_f64(0.5),
                    // ))
                    .before(handle_piece_move),
            ),
        );
    }
}
