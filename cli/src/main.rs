use std::{
    ops::DerefMut,
    str::FromStr,
    sync::{
        Arc, Mutex,
        atomic::{self, AtomicBool},
    },
    thread::JoinHandle,
    time::{Duration, Instant},
};

use brain::{Brain, Response};
use chers::{Board, board::STARTING_POSITION_FEN};
use line_reader::{EXIT, KEEP_GOING, KeepGoing};
use tokio::io::AsyncWriteExt;
use tracing_subscriber::EnvFilter;
use uci_parser::{UciCommand, UciMove, UciOption, UciResponse};

mod line_reader;

struct ActiveSearch {
    search_start_time: Instant,
    target_time: Duration,
    thread: JoinHandle<Response>,
    should_end: Arc<AtomicBool>,
}

struct State {
    stdout: tokio::io::Stdout,
    engine: Arc<Mutex<dyn Brain>>,
    is_ready: bool,
    board: Board,
    current_search: Option<ActiveSearch>,
}

impl State {
    fn new() -> State {
        let stdout = tokio::io::stdout();
        let engine = Arc::new(Mutex::new(brain::alphabeta::AlphaBeta {
            max_depth_plies: 20,
        }));
        let is_ready = false;
        let board = Board::STARTING_POSITION;
        let current_search = None;

        Self {
            stdout,
            engine,
            is_ready,
            board,
            current_search,
        }
    }

    async fn send(&mut self, response: UciResponse) {
        self.stdout
            .write_all(format!("{}\n", response).as_bytes())
            .await
            .expect("Writing should succeed");
        self.stdout.flush().await.expect("Flushing should succeed")
    }

    fn access_engine(&self) -> impl DerefMut<Target = dyn Brain> + '_ {
        assert!(self.current_search.is_none());
        self.engine
            .try_lock()
            .expect("Engine should not be locked if it is not searching.")
    }

    async fn check_current_search(&mut self) -> KeepGoing {
        let Some(search) = self.current_search.as_mut() else {
            return KEEP_GOING;
        };

        if search.search_start_time.elapsed() > search.target_time {
            search.should_end.store(true, atomic::Ordering::Release);
        }

        if search.thread.is_finished() {
            let search = self
                .current_search
                .take()
                .expect("Already checked to be Some");

            let response = search
                .thread
                .join()
                .expect("Search thread should not panic");

            match response {
                brain::Response::Move(mov) => {
                    let uci_move = UciMove::try_from(mov).expect("Move should be valid UciMove");
                    self.send(UciResponse::BestMove {
                        bestmove: Some(uci_move.to_string()),
                        ponder: None,
                    })
                    .await;
                }
                // Not supported?
                brain::Response::Forfeit => panic!("Can't forfeit"),
            }
        }
        return KEEP_GOING;
    }

    async fn poll_update(&mut self) -> KeepGoing {
        if self.check_current_search().await == EXIT {
            return EXIT;
        }
        return KEEP_GOING;
    }

    async fn handle_command(&mut self, command: UciCommand) -> KeepGoing {
        match command {
            UciCommand::Uci => {
                self.send(UciResponse::Name("chers".to_owned())).await;

                // TODO: Use serde to figure out which struct fields exist, or something.
                self.send(UciResponse::Option(UciOption {
                    name: "search depth".to_owned(),
                    opt_type: uci_parser::UciOptionType::Spin {
                        default: 7,
                        min: 1,
                        max: 100,
                    },
                }))
                .await;

                self.send(UciResponse::UciOk).await;
            }
            UciCommand::Debug(debug_enabled) => {
                tracing::warn!(
                    "Ignoring 'uci debug' command setting debug to {debug_enabled} (not implemented)"
                )
            }
            UciCommand::IsReady => {
                if self.current_search.is_some() {
                    self.send(UciResponse::ReadyOk).await;
                } else if !self.is_ready {
                    let mut engine = self.access_engine();
                    engine.get_ready();
                    drop(engine);
                    self.is_ready = true;

                    self.send(UciResponse::ReadyOk).await;
                } else {
                    tracing::info!(
                        "Seemingly getting spurious ping 'isready', so responding with 'readyok'"
                    );
                    self.send(UciResponse::ReadyOk).await;
                }
            }
            UciCommand::SetOption { name, value } => {
                tracing::warn!(
                    "Ignoring setting option {name:?} with value {value:?} (not implemented)"
                )
            }
            UciCommand::Register { name: _, code: _ } => todo!(),
            UciCommand::UciNewGame => {
                if self.current_search.is_some() {
                    tracing::error!(
                        "Refusing to process 'ucinewgame' command while a search is ongoing."
                    );
                    return KEEP_GOING;
                }
                let mut engine = self.access_engine();
                engine.new_game();
                drop(engine);
                self.is_ready = false;
            }
            UciCommand::Position { fen, moves } => {
                let fen = fen.as_ref().map(String::as_str);
                self.board
                    .fill_from_fen(fen.unwrap_or(STARTING_POSITION_FEN));
                for uci_move in moves {
                    let mov = chers::Move::try_from(uci_move)
                        .unwrap_or_else(|error| panic!("Invalid move {uci_move:?}: {error}"));
                    self.board.try_make_move(mov, false).unwrap_or_else(|| {
                        panic!(
                            "Move {mov:?} is not valid on position {}",
                            self.board.to_fen()
                        )
                    });
                }
            }
            UciCommand::Go(uci_search_options) => {
                // FIXME: Read search options.
                if self.current_search.is_some() {
                    tracing::error!("Got 'go' while there is already a search ongoing");
                    return KEEP_GOING;
                }
                let our_color = self.board.turn_color;
                let our_remaining_time = match our_color {
                    chers::PieceColor::Black => uci_search_options.btime,
                    chers::PieceColor::White => uci_search_options.wtime,
                };
                let target_time = match our_remaining_time {
                    Some(remaining_time) => self.access_engine().target_time(remaining_time),
                    None => {
                        // A nice "thinky" amount of time, but still fast for when playing against humans.
                        Duration::from_secs(10)
                    }
                };

                let engine_arc = Arc::clone(&self.engine);
                let board = self.board.clone();
                let should_end = Arc::new(AtomicBool::new(false));
                let should_end_clone = Arc::clone(&should_end);

                let search_start_time = Instant::now();

                let thread = std::thread::spawn(move || {
                    engine_arc
                        .lock()
                        .unwrap()
                        .next_move(&board, &should_end_clone)
                });

                let search = ActiveSearch {
                    search_start_time,
                    target_time,
                    thread,
                    should_end,
                };
                assert!(self.current_search.is_none());
                self.current_search = Some(search);
            }
            UciCommand::Stop => match self.current_search.as_mut() {
                Some(search) => search.should_end.store(true, atomic::Ordering::Release),
                None => tracing::warn!(
                    "Ignoring 'stop' command because there is no active search. Buffering issues?"
                ),
            },
            UciCommand::PonderHit => {
                tracing::warn!("Ignoring 'ponderhit' command (not implemented)")
            }
            UciCommand::Quit => return EXIT,
            UciCommand::Bench(_uci_search_options) => todo!(),
        };

        return KEEP_GOING;
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    tracing_subscriber::fmt()
        .compact()
        .with_env_filter(
            EnvFilter::builder()
                .with_env_var("CHERS_LOG")
                .with_default_directive("chers=warn".parse().expect("Has been manually tested"))
                .with_default_directive("cli=info".parse().expect("Has been manually tested"))
                .from_env()
                .unwrap_or_else(|error| panic!("Invalid log specification: {error}")),
        )
        .init();

    let state = Arc::new(Mutex::new(State::new()));

    line_reader::for_each_line(tokio::io::stdin(), move |line| {
        let state = Arc::clone(&state);
        async move {
            if line.is_empty() {
                return state.lock().unwrap().poll_update().await;
            }

            let command = match UciCommand::from_str(&line) {
                Ok(command) => command,
                Err(error) => {
                    tracing::error!("{error}");
                    return EXIT;
                }
            };
            state.lock().unwrap().handle_command(command).await
        }
    })
    .await
}
