use std::time::Duration;

use tokio::{
    io::{AsyncBufReadExt, AsyncRead},
    time::error::Elapsed,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum KeepGoing {
    KeepGoing,
    Exit,
}

pub const KEEP_GOING: KeepGoing = KeepGoing::KeepGoing;
pub const EXIT: KeepGoing = KeepGoing::Exit;

pub async fn for_each_line<F: Future<Output = KeepGoing> + Sync>(
    source: impl AsyncRead + Unpin,
    mut line_func: impl FnMut(String) -> F,
) {
    const POLL_INTERVAL: Duration = Duration::from_millis(1);
    let mut lines = tokio::io::BufReader::new(source).lines();
    let mut keep_going = KEEP_GOING;
    while keep_going == KEEP_GOING {
        match tokio::time::timeout(POLL_INTERVAL, lines.next_line()).await {
            // Got a line in time, pass it along.
            Ok(Ok(Some(line))) => keep_going = line_func(line).await,
            // Timed out. Send an empty string just to let the line_func() do some work if it wants to.
            Err(Elapsed { .. }) => keep_going = line_func("".to_owned()).await,
            // No more lines (?)
            Ok(Ok(None)) => keep_going = KeepGoing::Exit,
            // Error reading line.
            Ok(Err(error)) => tracing::error!("{error}"),
        }
    }
}
