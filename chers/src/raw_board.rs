use std::{
    fmt::{Debug, Write},
    hash::Hash,
    ops::{Index, IndexMut},
    random::{DefaultRandomSource, RandomSource},
    sync::{
        atomic::{self, AtomicBool},
        Arc, LazyLock,
    },
};

use const_arrayvec::ArrayVec;

use crate::{
    bitboard::BitBoard,
    by_pos::ByPos,
    castling_masks, castling_moves, castling_positions,
    moves::MaybePromoteMovesFromPosIter,
    pos,
    pos_buffer::PosBuffer,
    position::Dir,
    threat_lookup::{get_threat_lookup, ThreatLookup},
    AssertEq, ByColor, ByKind, CastlingAvailability, ColPiece, Move, MovePart, Movement,
    PieceColor, PieceKind, Pos, Promotion, RawMoveKind, RawMoveOutcome,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum MoveInvalidReason {
    PieceCantMoveToThatPosition,
    BlockedByAnotherPiece,
    NoFromPiece,
    FromEqualsTo,
    OnlyPawnCanPromote,

    PawnCantCaptureWithThatMovement,
    PromotionSpecifiedButPawnCantPromote,

    KingCantCastleBecauseItOrTheRookHasMoved,
    KingCantCastleBecauseItIsBlockedByAnotherPiece,
}

#[derive(Clone, PartialEq, Eq)]
pub struct RawBoard {
    pieces: [Option<ColPiece>; 64],
    piece_positions: PosBuffer,
    piece_squares_by_color: ByColor<BitBoard>,
    piece_squares_by_kind: ByKind<BitBoard>,
    threatened_squares_by_color: ByColor<BitBoard>,
    check_paths: ArrayVec<BitBoard, 4>,
    pin_paths: ByColor<ArrayVec<BitBoard, 8>>,
    zobrist_numbers: &'static ZobristNumbers,
    zobrist_hash: ZobristHash,
}

impl Hash for RawBoard {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        state.write_u64(self.zobrist_hash);
    }
}

impl RawBoard {
    pub const STARTING_POSITION: LazyLock<RawBoard> = LazyLock::new(|| {
        use crate::piece::pieces_opt::*;
        // "e" for empty square.
        const E: Option<ColPiece> = None;
        #[rustfmt::skip]
        const PIECES: [Option<ColPiece>; 64] = [
            r, n, b, q, k, b, n, r,
            p, p, p, p, p, p, p, p,
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            P, P, P, P, P, P, P, P,
            R, N, B, Q, K, B, N, R,
        ];
        const RANK_1_OR_8: BitBoard = BitBoard(BitBoard::rank(1).0 | BitBoard::rank(8).0);
        const PIECE_POSITIONS: PosBuffer = {
            let mut buf = PosBuffer::new();
            buf.insert(pos!("a8"));
            buf.insert(pos!("b8"));
            buf.insert(pos!("c8"));
            buf.insert(pos!("d8"));
            buf.insert(pos!("e8"));
            buf.insert(pos!("f8"));
            buf.insert(pos!("g8"));
            buf.insert(pos!("h8"));
            buf.insert(pos!("a7"));
            buf.insert(pos!("b7"));
            buf.insert(pos!("c7"));
            buf.insert(pos!("d7"));
            buf.insert(pos!("e7"));
            buf.insert(pos!("f7"));
            buf.insert(pos!("g7"));
            buf.insert(pos!("h7"));
            buf.insert(pos!("a2"));
            buf.insert(pos!("b2"));
            buf.insert(pos!("c2"));
            buf.insert(pos!("d2"));
            buf.insert(pos!("e2"));
            buf.insert(pos!("f2"));
            buf.insert(pos!("g2"));
            buf.insert(pos!("h2"));
            buf.insert(pos!("a1"));
            buf.insert(pos!("b1"));
            buf.insert(pos!("c1"));
            buf.insert(pos!("d1"));
            buf.insert(pos!("e1"));
            buf.insert(pos!("f1"));
            buf.insert(pos!("g1"));
            buf.insert(pos!("h1"));
            buf
        };
        let mut slf = RawBoard {
            pieces: PIECES,
            piece_positions: PIECE_POSITIONS,
            piece_squares_by_color: ByColor {
                white: BitBoard(BitBoard::rank(1).0 | BitBoard::rank(2).0),
                black: BitBoard(BitBoard::rank(7).0 | BitBoard::rank(8).0),
            },
            piece_squares_by_kind: ByKind {
                pawn: BitBoard(BitBoard::rank(2).0 | BitBoard::rank(7).0),
                bishop: BitBoard(RANK_1_OR_8.0 & (BitBoard::file(3).0 | BitBoard::file(6).0)),
                knight: BitBoard(RANK_1_OR_8.0 & (BitBoard::file(2).0 | BitBoard::file(7).0)),
                rook: BitBoard(RANK_1_OR_8.0 & (BitBoard::file(1).0 | BitBoard::file(8).0)),
                queen: BitBoard(RANK_1_OR_8.0 & BitBoard::file(4).0),
                king: BitBoard(RANK_1_OR_8.0 & BitBoard::file(5).0),
            },
            threatened_squares_by_color: ByColor {
                white: BitBoard(
                    (BitBoard::rank(1).0 | BitBoard::rank(2).0 | BitBoard::rank(3).0)
                        & !BitBoard::from_position(pos!("a1")).0
                        & !BitBoard::from_position(pos!("h1")).0,
                ),
                black: BitBoard(
                    (BitBoard::rank(6).0 | BitBoard::rank(7).0 | BitBoard::rank(8).0)
                        & !BitBoard::from_position(pos!("a8")).0
                        & !BitBoard::from_position(pos!("h8")).0,
                ),
            },
            check_paths: ArrayVec::new(),
            pin_paths: ByColor::new_const(),
            zobrist_numbers: ZobristNumbers::get(),
            zobrist_hash: 0,
        };
        slf.recalculate_zobrist_hash();

        slf
    });

    #[inline]
    pub fn new() -> RawBoard {
        Self {
            pieces: [None; 8 * 8],
            piece_positions: PosBuffer::new(),
            piece_squares_by_color: ByColor::none(),
            piece_squares_by_kind: ByKind::none(),
            threatened_squares_by_color: ByColor::none(),
            check_paths: ArrayVec::new(),
            pin_paths: ByColor::new_const(),
            zobrist_numbers: ZobristNumbers::get(),
            zobrist_hash: 0,
        }
    }

    pub fn from_pieces(pieces: [Option<ColPiece>; 8 * 8]) -> RawBoard {
        let mut this = Self {
            pieces,
            piece_positions: PosBuffer::new(),
            piece_squares_by_color: ByColor::none(),
            piece_squares_by_kind: ByKind::none(),
            threatened_squares_by_color: ByColor::none(),
            check_paths: ArrayVec::new(),
            pin_paths: ByColor::new_const(),
        };
        this.inline_recalculate_everything();
        this
    }

    #[inline]
    pub fn inline_recalculate_everything(&mut self) {
        self.recalculate_zobrist_hash();
        self.inline_recalculate_piece_positions();
        self.inline_recalculate_piece_squares_by_color();
        self.inline_recalculate_piece_squares_by_kind();
        self.inline_recalculate_threats_and_checks();
    }

    pub fn recalculate_everything(&mut self) {
        self.inline_recalculate_everything()
    }

    #[inline]
    pub fn inline_recalculate_piece_positions(&mut self) {
        self.piece_positions.clear();

        for (index, piece) in self.pieces.iter().enumerate() {
            if piece.is_some() {
                self.piece_positions
                    .insert(unsafe { Pos::from_flat_index_unchecked(index as u8) });
            }
        }
    }

    pub fn recalculate_piece_positions(&mut self) {
        self.inline_recalculate_piece_positions()
    }

    #[inline]
    pub fn inline_recalculate_piece_squares_by_color(&mut self) {
        self.piece_squares_by_color = ByColor::none();

        for (pos, piece) in unsafe {
            self.piece_positions
                .iter_with_pieces_unchecked(&self.pieces)
        } {
            self.piece_squares_by_color[piece.color()].set_pos(pos, true);
        }
    }

    pub fn recalculate_has_piece_squares_by_color(&mut self) {
        self.inline_recalculate_piece_squares_by_color()
    }

    #[inline]
    pub fn inline_recalculate_piece_squares_by_kind(&mut self) {
        self.piece_squares_by_kind = ByKind::none();

        for (pos, piece) in unsafe {
            self.piece_positions
                .iter_with_pieces_unchecked(&self.pieces)
        } {
            self.piece_squares_by_kind[piece.kind()].set_pos(pos, true);
        }
    }

    pub fn recalculate_piece_piece_squares_by_kind(&mut self) {
        self.inline_recalculate_piece_squares_by_kind()
    }

    pub fn threatened_squares_from(
        &self,
        threat_lookup: &ThreatLookup,
        pos: Pos,
    ) -> Option<BitBoard> {
        self.inline_threatened_squares_from(threat_lookup, pos)
    }

    #[inline(always)]
    pub fn inline_threatened_squares_from(
        &self,
        threat_lookup: &ThreatLookup,
        pos: Pos,
    ) -> Option<BitBoard> {
        let Some(piece) = self.index(pos) else {
            return None;
        };
        let occupied_positions = self.piece_positions.bitboard();

        // FIXME: Inefficient implementation.
        let squares = match piece.kind() {
            PieceKind::Pawn => threat_lookup.lookup_pawn_threats(pos, piece.color()),
            PieceKind::Knight => threat_lookup.lookup_knight_threats(pos),
            PieceKind::King => threat_lookup.lookup_king_threats(pos),
            PieceKind::Bishop => threat_lookup.lookup_bishop_threats(pos, occupied_positions),
            PieceKind::Rook => threat_lookup.lookup_rook_threats(pos, occupied_positions),
            PieceKind::Queen => threat_lookup.lookup_queen_threats(pos, occupied_positions),
        };

        Some(squares)
    }

    // TODO: Benchmark against or remove?
    #[inline(always)]
    fn _threatened_squares_in_sliding_direction(
        &self,
        start_pos: Pos,
        dir: Dir,
        output: &mut BitBoard,
        check_path: &mut Option<BitBoard>,
        pin_path: &mut Option<BitBoard>,
    ) {
        let moving_piece = self.index(start_pos).unwrap();
        let opponent_color = !moving_piece.color();
        let opponent_king_piece = ColPiece::new(PieceKind::King, opponent_color);

        let mut is_blocked = false;
        let mut bb = BitBoard::none();
        let mut pierce_bb = BitBoard::none();
        let mut curr_pos = start_pos;
        while let Some(new_pos) = curr_pos.add_dir(dir) {
            if !is_blocked {
                bb.set_pos(new_pos, true);
            } else {
                pierce_bb.set_pos(new_pos, true);
            }

            if let Some(blocking_piece) = self.index(new_pos) {
                if blocking_piece == opponent_king_piece {
                    if !is_blocked {
                        debug_assert_eq!(*check_path, None);
                        *check_path = Some(
                            (bb | BitBoard::from_position(start_pos))
                                & !BitBoard::from_position(new_pos),
                        );
                    } else {
                        debug_assert_eq!(*pin_path, None);
                        *pin_path = Some(
                            (pierce_bb | BitBoard::from_position(start_pos))
                                & !BitBoard::from_position(new_pos),
                        );
                    }
                    break;
                } else if blocking_piece.color() == moving_piece.color() {
                    // Can't pin your own pieces.
                    break;
                }

                if is_blocked {
                    break;
                } else {
                    is_blocked = true;
                    pierce_bb = bb;
                }
            }

            curr_pos = new_pos;
        }

        *output |= bb;
    }

    #[inline(always)]
    pub fn inline_recalculate_threats_and_checks(&mut self) {
        self.threatened_squares_by_color = ByColor::none();
        self.check_paths.clear();
        self.pin_paths.clear();
        let mut check_paths = self.check_paths.clone();
        let mut pin_paths = self.pin_paths.clone();
        let threat_lookup = get_threat_lookup();

        for (pos, piece) in unsafe {
            self.piece_positions
                .iter_with_pieces_unchecked(&self.pieces)
        } {
            let threatened_squares = self
                .inline_threatened_squares_from(threat_lookup, pos)
                .expect("Position has a piece");
            self.threatened_squares_by_color[piece.color()] |= threatened_squares;
        }

        for king_pos in self.piece_squares_by_kind[PieceKind::King].positions() {
            let piece = self[king_pos].expect("The square must have a piece if the piece_squares_by_kind bitboard has the bit set");
            for check_or_pin in self.find_pinned_pieces_and_checks_for_king(threat_lookup, king_pos)
            {
                match check_or_pin {
                    CheckOrPin::Check {
                        check_threatened_squares,
                    } => check_paths.push(check_threatened_squares),
                    CheckOrPin::Pin {
                        pinned_piece_pos: _,
                        valid_move_squares,
                    } => pin_paths[piece.color()].push(valid_move_squares),
                }
            }
        }

        self.check_paths = check_paths;
        self.pin_paths = pin_paths;
    }

    pub fn recalculate_threats_and_checks(&mut self) {
        self.inline_recalculate_threats_and_checks()
    }

    fn find_pinned_pieces_and_checks_for_king(
        &self,
        threat_lookup: &ThreatLookup,
        king_pos: Pos,
    ) -> impl Iterator<Item = CheckOrPin> + '_ {
        let piece = self
            .index(king_pos)
            .expect("should only be called with a valid position");
        assert_eq!(
            piece.kind(),
            PieceKind::King,
            "should only be called with the position of a king"
        );
        let king_color = piece.color();
        let king_square = BitBoard::from_position(king_pos);

        #[inline(always)]
        fn sliding_piece_checks_and_pins(
            board: &RawBoard,
            king_pos: Pos,
            king_color: PieceColor,
            king_square: BitBoard,
        ) -> impl Iterator<Item = CheckOrPin> + '_ {
            Dir::ALL.into_iter().filter_map(move |dir| {
                let mut visited_squares = king_square;
                let mut prev_pos = king_pos;
                let mut pinned_piece_pos = None;

                // FIXME: Use a lookup table instead of relying on `pos::add_dir()` returning `None`.
                while let Some(new_pos) = prev_pos.add_dir(dir) {
                    visited_squares.set_pos(prev_pos, true);
                    prev_pos = new_pos;

                    let Some(blocking_piece) = board.index(new_pos) else {
                        continue;
                    };

                    // Is this the second piece we've encountered?
                    if let Some(pinned_piece_pos) = pinned_piece_pos {
                        if blocking_piece.color() != king_color
                            && blocking_piece.kind().can_attack_from_direction(dir)
                        {
                            visited_squares.set_pos(new_pos, true);
                            return Some(CheckOrPin::Pin {
                                pinned_piece_pos,
                                valid_move_squares: visited_squares & !king_square,
                            });
                        } else {
                            return None;
                        }
                    }

                    if blocking_piece.color() == king_color {
                        // Closest piece in this direction is a friendly piece, so it _could_ be pinned.
                        pinned_piece_pos = Some(new_pos);
                    } else {
                        // Closest piece in this direction is an enemy piece, so no piece is pinned.
                        // The king might be in check though.
                        if blocking_piece.kind().can_attack_from_direction(dir) {
                            visited_squares.set_pos(new_pos, true);
                            return Some(CheckOrPin::Check {
                                check_threatened_squares: visited_squares,
                            });
                        } else {
                            return None;
                        }
                    }
                }

                // No more pieces in this direction means no pin.
                return None;
            })
        }

        #[inline(always)]
        fn knight_checks<'b>(
            board: &'b RawBoard,
            threat_lookup: &ThreatLookup,
            king_pos: Pos,
            king_color: PieceColor,
            king_square: BitBoard,
        ) -> impl Iterator<Item = CheckOrPin> + 'b {
            // We can just use the threat lookup because knights can move both backwards and
            // forwards in any direction.
            threat_lookup
                .lookup_knight_threats(king_pos)
                .positions()
                .filter(move |&maybe_knight_pos| {
                    board.index(maybe_knight_pos)
                        == Some(ColPiece::new(PieceKind::Knight, !king_color))
                })
                .map(move |enemy_knight_pos| CheckOrPin::Check {
                    check_threatened_squares: BitBoard::from_position(enemy_knight_pos)
                        | king_square,
                })
        }

        #[inline(always)]
        fn pawn_checks(
            board: &RawBoard,
            king_pos: Pos,
            king_color: PieceColor,
            king_square: BitBoard,
        ) -> impl Iterator<Item = CheckOrPin> + '_ {
            [
                king_pos.add_dir(king_color.relative_dir(Dir::UpLeft)),
                king_pos.add_dir(king_color.relative_dir(Dir::UpRight)),
            ]
            .into_iter()
            .flatten()
            .filter(move |&maybe_pawn_pos| {
                board.index(maybe_pawn_pos) == Some(ColPiece::new(PieceKind::Pawn, !king_color))
            })
            .map(move |enemy_pawn_pos| CheckOrPin::Check {
                check_threatened_squares: BitBoard::from_position(enemy_pawn_pos) | king_square,
            })
        }

        std::iter::empty()
            .chain(sliding_piece_checks_and_pins(
                self,
                king_pos,
                king_color,
                king_square,
            ))
            .chain(knight_checks(
                self,
                threat_lookup,
                king_pos,
                king_color,
                king_square,
            ))
            .chain(pawn_checks(self, king_pos, king_color, king_square))
    }

    #[inline]
    pub fn piece_positions(&self) -> impl ExactSizeIterator<Item = Pos> + '_ {
        self.piece_positions.iter()
    }

    #[inline]
    pub fn pieces_with_positions(&self) -> impl ExactSizeIterator<Item = (Pos, ColPiece)> + '_ {
        // SAFETY: `self.piece_positions` is synced up with `self.pieces`, as all changes must be
        //         made through `self.inline_apply_move_buffer()`, which syncs it up.
        unsafe {
            self.piece_positions
                .iter_with_pieces_unchecked(&self.pieces)
        }
    }

    pub fn from_fen_part(part: &str) -> Option<Self> {
        let mut board = Self::new();

        let mut row = 0;
        let mut column = 0;

        for ch in part.chars() {
            match ch {
                '1'..='8' => {
                    let num_skip = (ch as u32 - '0' as u32) as u8;
                    column += num_skip;
                    continue;
                }
                '/' => {
                    if column != 8 {
                        tracing::error!("Invalid FEN; insufficient padding before end of rank.");
                        return None;
                    }
                    row += 1;
                    column = 0;
                    continue;
                }
                _ => {}
            }

            let Some(piece) = ColPiece::from_fen_letter(ch) else {
                tracing::error!("Unexpected FEN char {ch:?} when parsing piece positions.");
                return None;
            };

            let Some(pos) = Pos::from_coord_indices(column, row) else {
                let attempted_column_letter = b'a' + column;
                let attempted_row_number = b'1' + row;
                tracing::error!(
                    "Invalid FEN; attempting to write to invalid position \"{}{}\".",
                    attempted_column_letter,
                    attempted_row_number,
                );
                return None;
            };

            board[pos] = Some(piece);
            column += 1;
        }

        board.inline_recalculate_everything();

        Some(board)
    }

    #[inline]
    pub const fn index(&self, pos: Pos) -> Option<ColPiece> {
        unsafe {
            self.pieces
                .split_at_unchecked(pos.to_flat_index() as usize)
                .1[0]
        }
    }

    /// Should ONLY be called from `[RawBoard::inline_apply_moves_from_buffer]`
    /// Otherwise the internal state of this `RawBoard` may become corrupted.
    #[inline]
    const fn index_mut(&mut self, pos: Pos) -> &mut Option<ColPiece> {
        unsafe {
            &mut self
                .pieces
                .split_at_mut_unchecked(pos.to_flat_index() as usize)
                .1[0]
        }
    }

    #[inline]
    pub const fn is_pos_threatened(&self, pos: Pos, by_color: PieceColor) -> bool {
        self.threatened_squares_by_color
            .index(by_color)
            .get_pos(pos)
    }

    pub fn assert_available_moves_match_valid_moves(
        &self,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
        color_to_move: PieceColor,
    ) {
        let threat_lookup = get_threat_lookup();
        let pieces = self.pieces().filter_map(|(pos, piece)| match (pos, piece) {
            (pos, Some(piece)) if piece.color() == color_to_move => Some(pos),
            _ => None,
        });

        let fen = std::sync::LazyLock::new(|| {
            crate::Board {
                raw: self.clone(),
                turn_color: color_to_move,
                castling_availability,
                en_passant_target_square,
                num_half_moves: 0,
                num_full_moves: std::num::NonZeroUsize::new(1).expect("1 is nonzero"),
                last_move: Vec::new(),
            }
            .to_fen()
        });

        for from in pieces {
            let available_move_squares = self
                .inline_legal_move_squares_from(
                    threat_lookup,
                    from,
                    en_passant_target_square,
                    castling_availability,
                )
                .expect("from position has a piece");

            let mut valid_move_squares = BitBoard::none();

            for to in Pos::positions() {
                let movement = Movement { from, to };
                let is_valid = self.is_move_legal(
                    Move::new(movement),
                    en_passant_target_square,
                    castling_availability,
                );
                valid_move_squares.set_pos(to, is_valid);
            }

            if available_move_squares != valid_move_squares {
                tracing::error!(
                    ?from,
                    piece = ?self.index(from).unwrap(),
                    fen = *fen,
                    "movegen:\n{available_move_squares:?}\nvalidity:\n{valid_move_squares:?}",
                );
            }

            // assert_eq!(available_move_squares, valid_move_squares);
        }
    }

    /// Checks if the piece at the `mov.from` position can move to the `mov.to` position.
    /// Checks that there are no blocking pieces along the path, but excludes the final position.
    /// Does *not* check whether any potential capture is valid.
    pub fn is_move_valid_for_piece(
        &self,
        mov: Move,
        en_passant_pos: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Option<RawMoveKind> {
        self.is_move_valid_for_piece_inline(mov, en_passant_pos, castling_availability)
            .ok()
    }

    /// Checks if the piece at the `mov.from` position can move to the `mov.to` position.
    /// Checks that there are no blocking pieces along the path, but excludes the final position.
    /// Does *not* check whether any potential capture is valid.
    #[inline]
    pub const fn is_move_valid_for_piece_inline(
        &self,
        mov: Move,
        en_passant_pos: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Result<RawMoveKind, MoveInvalidReason> {
        use MoveInvalidReason as Reason;

        let promotion = mov.promotion();
        let mov = mov.movement();

        let Some(piece) = self.index(mov.from) else {
            return Err(Reason::NoFromPiece);
        };
        let diff = mov.to.sub_pos(mov.from);
        let to_piece = self.index(mov.to);
        let to_position_occupied = to_piece.is_some();

        if let (0, 0) = diff {
            return Err(Reason::FromEqualsTo);
        }
        let (diff_x, diff_y) = diff;

        #[derive(Clone, Copy)]
        struct DataType<'a> {
            board: &'a RawBoard,
            mov: Movement,
            promotion: Option<Promotion>,
            piece: ColPiece,
            diff_x: i8,
            diff_y: i8,
            to_position_occupied: bool,
            en_passant_pos: Option<Pos>,
            castling_availability: CastlingAvailability,
        }
        let data = DataType {
            board: self,
            mov,
            promotion,
            piece,
            diff_x,
            diff_y,
            to_position_occupied,
            en_passant_pos,
            castling_availability,
        };

        #[inline(always)]
        const fn path_is_free(data: DataType) -> bool {
            data.board
                .free_path_to_pos_along_dir(data.mov, (data.diff_x, data.diff_y))
        }

        #[inline(always)]
        const fn colorless_diff(data: DataType) -> (i8, i8) {
            data.piece
                .color()
                .colorless_offset((data.diff_x, data.diff_y))
        }

        #[inline(always)]
        const fn basic_action(data: DataType) -> RawMoveKind {
            match data.to_position_occupied {
                true => RawMoveKind::BasicCapture,
                false => RawMoveKind::BasicMovement,
            }
        }

        #[inline(always)]
        const fn check_pawn(data: DataType) -> Result<RawMoveKind, MoveInvalidReason> {
            #[inline(always)]
            const fn is_pawn_basic_movement(data: DataType) -> bool {
                matches!(colorless_diff(data), (0, 1))
            }

            #[inline(always)]
            const fn is_pawn_double_movement(data: DataType) -> bool {
                let starting_rank = match data.piece.color() {
                    PieceColor::Black => 7,
                    PieceColor::White => 2,
                };
                if data.mov.from.rank() != starting_rank {
                    return false;
                }
                if !matches!(colorless_diff(data), (0, 2)) {
                    return false;
                }
                let pos_in_the_middle = (data
                    .mov
                    .from
                    .add_dir(Dir::from_y_signum(data.diff_y.signum()).unwrap()))
                .unwrap();
                let piece_in_the_way = data.board.index(pos_in_the_middle);

                piece_in_the_way.is_none()
            }

            #[inline(always)]
            const fn is_pawn_capture_movement(data: DataType) -> bool {
                matches!(colorless_diff(data), (1, 1) | (-1, 1))
            }

            #[inline(always)]
            const fn is_pawn_en_passant(data: DataType) -> bool {
                let Some(en_passant_pos) = data.en_passant_pos else {
                    return false;
                };
                if !en_passant_pos.equals(data.mov.to) {
                    return false;
                }
                let en_passant_piece_pos = (en_passant_pos
                    .add_dir(Dir::from_y_signum(-data.piece.color().forward_y()).unwrap()))
                .expect("En passant offset should be valid");
                assert!(
                    !data.piece.color().equals(
                        data.board
                            .index(en_passant_piece_pos)
                            .expect("En passant piece pos must have a piece")
                            .color(),
                    ),
                    "En passant pos is set while checking moves for the same color.\n\
                    The opponent should have moved before this color moved again,\n\
                    and the position should have been reset when the the enemy moved."
                );
                is_pawn_capture_movement(data)
            }

            #[inline(always)]
            const fn is_pawn_promotion(data: DataType) -> bool {
                let promotion_rank = match data.piece.color() {
                    PieceColor::White => 8,
                    PieceColor::Black => 1,
                };

                data.mov.to.rank() == promotion_rank
                    && match data.to_position_occupied {
                        false => is_pawn_basic_movement(data),
                        true => is_pawn_capture_movement(data),
                    }
            }

            #[inline(always)]
            const fn check_pawn_double_movement(data: DataType) -> Option<RawMoveKind> {
                match is_pawn_double_movement(data) {
                    true => return Some(RawMoveKind::PawnDoubleMove),
                    false => None,
                }
            }

            #[inline(always)]
            const fn check_pawn_en_passant(data: DataType) -> Option<RawMoveKind> {
                match is_pawn_en_passant(data) {
                    true => return Some(RawMoveKind::PawnEnPassant),
                    false => None,
                }
            }

            if data.promotion.is_some() {
                if is_pawn_promotion(data) {
                    return Ok(RawMoveKind::Promote);
                } else {
                    return Err(Reason::PromotionSpecifiedButPawnCantPromote);
                }
            }

            if data.to_position_occupied {
                if is_pawn_capture_movement(data) {
                    return Ok(RawMoveKind::BasicCapture);
                }
                return Err(Reason::PawnCantCaptureWithThatMovement);
            }

            if is_pawn_basic_movement(data) {
                return Ok(RawMoveKind::BasicMovement);
            }
            if let Some(move_kind) = check_pawn_double_movement(data) {
                return Ok(move_kind);
            }
            if let Some(move_kind) = check_pawn_en_passant(data) {
                return Ok(move_kind);
            }
            Err(Reason::PieceCantMoveToThatPosition)
        }

        #[inline(always)]
        const fn is_rook_movement(data: DataType) -> bool {
            data.diff_x == 0 || data.diff_y == 0
        }

        #[inline(always)]
        const fn is_bishop_movement(data: DataType) -> bool {
            data.diff_x.abs() == data.diff_y.abs()
        }

        #[inline(always)]
        const fn is_king_movement(data: DataType) -> bool {
            data.diff_x.abs() <= 1 && data.diff_y.abs() <= 1
        }

        #[inline(always)]
        const fn check_king_castling(
            data: DataType,
        ) -> Option<Result<RawMoveKind, MoveInvalidReason>> {
            use castling_masks as masks;
            use castling_moves as moves;

            // TODO: Use bitboards to optimize this.

            let allowed_from_pos = match data.piece.color() {
                PieceColor::Black => moves::BLACK_KINGSIDE_KING.from,
                PieceColor::White => moves::WHITE_KINGSIDE_KING.from,
            };
            if !data.mov.from.equals(allowed_from_pos) {
                return Some(Err(Reason::KingCantCastleBecauseItOrTheRookHasMoved));
            }

            // No need to check king or rook positions, as the CastlingAvailability
            // will tell us whether the king or rook has moved.

            const KINGSIDE: usize = 0;
            const QUEENSIDE: usize = 1;

            let (available_kingside, available_queenside) =
                data.castling_availability.for_color(data.piece.color());
            let available: [bool; 2] = [available_kingside, available_queenside];

            let allowed_king_moves = match data.piece.color() {
                PieceColor::Black => [moves::BLACK_KINGSIDE_KING, moves::BLACK_QUEENSIDE_KING],
                PieceColor::White => [moves::WHITE_KINGSIDE_KING, moves::WHITE_QUEENSIDE_KING],
            };
            let nooccupy_squares = match data.piece.color() {
                PieceColor::Black => [
                    masks::BLACK_KINGSIDE_NOTOCCUPIED_SQUARES,
                    masks::BLACK_QUEENSIDE_NOTOCCUPIED_SQUARES,
                ],
                PieceColor::White => [
                    masks::WHITE_KINGSIDE_NOTOCCUPIED_SQUARES,
                    masks::WHITE_QUEENSIDE_NOTOCCUPIED_SQUARES,
                ],
            };
            let nothreat_squares = match data.piece.color() {
                PieceColor::Black => [
                    masks::BLACK_KINGSIDE_NOTHREAT_SQUARES,
                    masks::BLACK_QUEENSIDE_NOTHREAT_SQUARES,
                ],
                PieceColor::White => [
                    masks::WHITE_KINGSIDE_NOTHREAT_SQUARES,
                    masks::WHITE_QUEENSIDE_NOTHREAT_SQUARES,
                ],
            };
            let threatened_squares = *data
                .board
                .threatened_squares_by_color
                .index(data.piece.color().not());

            #[inline(always)]
            const fn try_side(
                board: &RawBoard,
                castling_is_available: [bool; 2],
                allowed_king_move: [Movement; 2],
                nooccupy_squares: [BitBoard; 2],
                nothreat_squares: [BitBoard; 2],
                threatened_squares: BitBoard,
                mov: Movement,
                side: usize,
            ) -> Option<Result<RawMoveKind, MoveInvalidReason>> {
                let king_move = allowed_king_move[side];
                if !mov.to.equals(king_move.to) {
                    return None;
                }

                if !castling_is_available[side] {
                    return Some(Err(Reason::KingCantCastleBecauseItOrTheRookHasMoved));
                }
                #[cfg(debug_assertions)]
                if !mov.from.equals(king_move.from) {
                    panic!("Castling is available but the king has somehow moved");
                }

                let path_is_free = (nooccupy_squares[side].0
                    & board.piece_squares_by_color.either().0)
                    == BitBoard::none().0;
                let king_can_move_without_threat = (nothreat_squares.split_at(side).1[0].0
                    & threatened_squares.0)
                    == BitBoard::none().0;

                if !path_is_free || !king_can_move_without_threat {
                    return Some(Err(Reason::KingCantCastleBecauseItIsBlockedByAnotherPiece));
                }

                Some(Ok(RawMoveKind::Castle))
            }

            if let Some(result) = try_side(
                data.board,
                available,
                allowed_king_moves,
                nooccupy_squares,
                nothreat_squares,
                threatened_squares,
                data.mov,
                KINGSIDE,
            ) {
                return Some(result);
            }
            if let Some(result) = try_side(
                data.board,
                available,
                allowed_king_moves,
                nooccupy_squares,
                nothreat_squares,
                threatened_squares,
                data.mov,
                QUEENSIDE,
            ) {
                return Some(result);
            }

            None
        }

        #[inline(always)]
        const fn is_knight_movement(data: DataType) -> bool {
            (data.diff_x.abs() == 2 && data.diff_y.abs() == 1)
                || (data.diff_x.abs() == 1 && data.diff_y.abs() == 2)
        }

        if promotion.is_some() && !matches!(piece.kind(), PieceKind::Pawn) {
            return Err(Reason::OnlyPawnCanPromote);
        }

        match piece.kind() {
            PieceKind::Pawn => check_pawn(data),
            PieceKind::Bishop => {
                if !is_bishop_movement(data) {
                    return Err(Reason::PieceCantMoveToThatPosition);
                }
                if !path_is_free(data) {
                    return Err(Reason::BlockedByAnotherPiece);
                }
                Ok(basic_action(data))
            }
            PieceKind::Knight => {
                if !is_knight_movement(data) {
                    return Err(Reason::PieceCantMoveToThatPosition);
                }
                Ok(basic_action(data))
            }
            PieceKind::Rook => {
                if !is_rook_movement(data) {
                    return Err(Reason::PieceCantMoveToThatPosition);
                }
                if !path_is_free(data) {
                    return Err(Reason::BlockedByAnotherPiece);
                }
                Ok(basic_action(data))
            }
            PieceKind::Queen => {
                if !is_rook_movement(data) && !is_bishop_movement(data) {
                    return Err(Reason::PieceCantMoveToThatPosition);
                }
                if !path_is_free(data) {
                    return Err(Reason::BlockedByAnotherPiece);
                }
                Ok(basic_action(data))
            }
            PieceKind::King => {
                if is_king_movement(data) {
                    return Ok(basic_action(data));
                }
                if let Some(result) = check_king_castling(data) {
                    return result;
                }
                Err(Reason::PieceCantMoveToThatPosition)
            }
        }
    }

    #[inline(always)]
    pub fn inline_calculate_outcome(
        &self,
        mov: Move,
        en_passant_pos: Option<Pos>,
        castling_availability: CastlingAvailability,
        mut move_buffer: Option<&mut Vec<MovePart>>,
        log_logic: bool,
    ) -> Option<(RawMoveKind, Option<Pos>)> {
        let movement = mov.movement();

        let Some(from_piece) = self.index(movement.from) else {
            tracing::error!("No piece in the position to move from.");
            return None;
        };

        let to_piece = self.index(movement.to);
        if let Some(to_piece) = to_piece {
            if from_piece.color() == to_piece.color() {
                if log_logic {
                    tracing::info!("Can't capture an ally piece.");
                }
                return None;
            }
        }

        let maybe_raw_move_kind =
            self.is_move_valid_for_piece_inline(mov, en_passant_pos, castling_availability);

        let raw_move_kind = match maybe_raw_move_kind {
            Ok(raw_move_kind) => raw_move_kind,
            Err(reason) => {
                if log_logic {
                    tracing::info!("Move is not valid:  {reason:#?}");
                }
                return None;
            }
        };

        let _raw_move_kind_span = tracing::info_span!("raw_move_kind", ?raw_move_kind).entered();
        // FIXME: Remove debugging.
        tracing::trace!("\n{self:?}");

        let mut new_en_passant_pos = None;

        if let Some(move_buffer) = move_buffer.as_mut() {
            move_buffer.clear();
            if to_piece.is_some() {
                move_buffer.push(MovePart::Captured(movement.to));
            }
            move_buffer.push(MovePart::Movement(movement));
        }

        match raw_move_kind {
            RawMoveKind::BasicMovement => {}
            RawMoveKind::BasicCapture => {}
            RawMoveKind::PawnDoubleMove => {
                let (diff_x, diff_y) = movement.to - movement.from;
                let dir = (diff_x.signum(), diff_y.signum());
                let middle_pos = (movement.from + dir).expect("Middle position should be valid");
                new_en_passant_pos = Some(middle_pos);
            }
            RawMoveKind::PawnEnPassant => {
                let captured_pawn_offset = match from_piece.color() {
                    PieceColor::Black => (0, -1),
                    PieceColor::White => (0, 1),
                };
                let captured_pawn_pos = (movement.to + captured_pawn_offset)
                    .expect("Offset from en-passant:ing pawn should be valid");
                // FIXME: Remove debugging.
                tracing::trace!(captured_pawn = ?self[captured_pawn_pos], expected_captured_pawn = ?Some(ColPiece::new(PieceKind::Pawn, !from_piece.color())));
                debug_assert_eq!(
                    self[captured_pawn_pos],
                    Some(ColPiece::new(PieceKind::Pawn, !from_piece.color()))
                );
                if let Some(move_buffer) = move_buffer.as_mut() {
                    move_buffer.push(MovePart::Captured(captured_pawn_pos));
                }
            }
            RawMoveKind::Castle => {
                use castling_moves as moves;
                use castling_positions as pos;

                let rook_move = match movement.to {
                    pos::BLACK_KINGSIDE_KING_POS => moves::BLACK_KINGSIDE_ROOK,
                    pos::BLACK_QUEENSIDE_KING_POS => moves::BLACK_QUEENSIDE_ROOK,
                    pos::WHITE_KINGSIDE_KING_POS => moves::WHITE_KINGSIDE_ROOK,
                    pos::WHITE_QUEENSIDE_KING_POS => moves::WHITE_QUEENSIDE_ROOK,
                    _ => unreachable!(),
                };

                if let Some(move_buffer) = move_buffer.as_mut() {
                    move_buffer.push(MovePart::Movement(rook_move));
                }
            }
            RawMoveKind::Promote => {
                let promotion = mov
                    .promotion()
                    .expect("RawMoveKind is Promote, so promotion shouldn't be None");
                if let Some(move_buffer) = move_buffer.as_mut() {
                    move_buffer.push(MovePart::Promoted(movement.to, promotion));
                }
            }
        };

        // TODO: Check if a check on our king prevents this move because the king could be captured.
        // TODO: Handle setting the 'check' flag if the enemy king is now in check.

        Some((raw_move_kind, new_en_passant_pos))
    }

    #[inline]
    pub(crate) fn inline_apply_full_move(
        &mut self,
        move_buffer: &[MovePart],
        en_passant_pos: &mut Option<Pos>,
        new_en_passant_pos: Option<Pos>,
        castling_availability: &mut CastlingAvailability,
    ) {
        self.inline_apply_moves_from_buffer(move_buffer, castling_availability);
        *en_passant_pos = new_en_passant_pos;
    }

    /// Updates internal state with the information that the piece at the given position is no
    /// longer there.
    ///
    /// This does not mean the piece has been captured, only that it is no longer on that square.
    #[inline]
    fn update_state_with_piece_removal(&mut self, pos: Pos, piece: ColPiece) {
        self.piece_squares_by_color[piece.color()].set_pos(pos, false);
        self.piece_squares_by_kind[piece.kind()].set_pos(pos, false);

        self.piece_positions.remove(pos);
    }

    /// Updates internal state with the information that the empty square at the given position
    /// now contains the given piece.
    ///
    /// This does not mean a piece has spawed out of thin air, it was likely moved from another
    /// square.
    #[inline]
    fn update_state_with_piece_addition(&mut self, pos: Pos, piece: ColPiece) {
        self.piece_squares_by_color[piece.color()].set_pos(pos, true);
        self.piece_squares_by_kind[piece.kind()].set_pos(pos, true);

        self.piece_positions.insert(pos);
    }

    /// Removes the piece at the given position and updates internal state to match.
    ///
    /// If there is a piece at the given position the piece is removed, things are
    /// recalculated and the removed piece is returned.
    /// If there is no piece at the given position, nothing is done and `None` is returned.
    #[inline]
    fn raw_remove_piece_from_square(&mut self, pos: Pos) -> Option<ColPiece> {
        tracing::trace!("Removing piece {:?} from square {pos}", self.index(pos));

        let piece = self.index_mut(pos).take()?;
        self.update_state_with_piece_removal(pos, piece);
        Some(piece)
    }

    /// Set the piece at the given position to the given piece and updates internal state to match.
    ///
    /// If there is a piece at the given position the piece is removed, things are recalculated,
    /// the new piece is added, things are recalculated and the removed piece is returned.
    /// If there is no piece at the given position, the new piece is added, things are recalculated
    /// and `None` is returned.
    #[inline]
    fn raw_set_piece_at_square(&mut self, pos: Pos, piece: ColPiece) -> Option<ColPiece> {
        let existing_piece = self.raw_remove_piece_from_square(pos);

        match existing_piece {
            Some(existing_piece) => tracing::trace!(
                "Replacing piece {existing_piece:?} with piece {piece:?} on square {pos}"
            ),
            None => tracing::trace!("Adding piece {piece:?} to square {pos}"),
        }

        *self.index_mut(pos) = Some(piece);
        self.update_state_with_piece_addition(pos, piece);

        existing_piece
    }

    #[inline(always)]
    pub(crate) fn inline_apply_moves_from_buffer(
        &mut self,
        move_buffer: &[MovePart],
        castling_availability: &mut CastlingAvailability,
    ) {
        tracing::trace!("Performing moves: {move_buffer:?}");

        for part in move_buffer.iter().copied() {
            match part {
                MovePart::Movement(mov) => {
                    let to_piece = self.index(mov.to);
                    debug_assert_eq!(to_piece, None);

                    let from_piece = self
                        .raw_remove_piece_from_square(mov.from)
                        .expect("Movement from-position does not have a piece");

                    self.raw_set_piece_at_square(mov.to, from_piece);

                    if !castling_availability.is_none() {
                        use castling_positions as pos;

                        // Revoke castling rights if a king or rook moves from their original position.
                        match mov.from {
                            pos::BLACK_KING_START_POS => {
                                castling_availability.black_kingside = false;
                                castling_availability.black_queenside = false;
                            }
                            pos::WHITE_KING_START_POS => {
                                castling_availability.white_kingside = false;
                                castling_availability.white_queenside = false;
                            }
                            pos::BLACK_KINGSIDE_ROOK_START_POS => {
                                castling_availability.black_kingside = false;
                            }
                            pos::BLACK_QUEENSIDE_ROOK_START_POS => {
                                castling_availability.black_queenside = false;
                            }
                            pos::WHITE_KINGSIDE_ROOK_START_POS => {
                                castling_availability.white_kingside = false;
                            }
                            pos::WHITE_QUEENSIDE_ROOK_START_POS => {
                                castling_availability.white_queenside = false;
                            }
                            _ => {}
                        };
                    }
                }
                MovePart::Captured(pos) => {
                    self.raw_remove_piece_from_square(pos)
                        .expect("There is no piece on the captured position");

                    if !castling_availability.is_none() {
                        use castling_positions as pos;

                        // Revoke castling rights if a rook is captured.
                        // Kings can't be captured, so no need to check for that.
                        match pos {
                            pos::BLACK_KINGSIDE_ROOK_START_POS => {
                                castling_availability.black_kingside = false;
                            }
                            pos::BLACK_QUEENSIDE_ROOK_START_POS => {
                                castling_availability.black_queenside = false;
                            }
                            pos::WHITE_KINGSIDE_ROOK_START_POS => {
                                castling_availability.white_kingside = false;
                            }
                            pos::WHITE_QUEENSIDE_ROOK_START_POS => {
                                castling_availability.white_queenside = false;
                            }
                            _ => {}
                        };
                    }
                }
                MovePart::Promoted(pos, promotion) => {
                    let piece = self
                        .raw_remove_piece_from_square(pos)
                        .expect("No piece on the square that promoted");

                    let new_piece = ColPiece::new(promotion.to_piece_kind(), piece.color());

                    tracing::trace!("Promoting {piece:?} to {new_piece:?}");

                    self.raw_set_piece_at_square(pos, new_piece);
                }
            }
        }

        // TODO: Optimize.
        self.inline_recalculate_threats_and_checks();
    }

    pub fn make_pseudolegal_move(
        &mut self,
        mov: Move,
        en_passant_pos: &mut Option<Pos>,
        castling_availability: &mut CastlingAvailability,
        move_buffer: &mut Vec<MovePart>,
    ) -> Option<RawMoveKind> {
        let outcome = self.inline_calculate_outcome(
            mov,
            *en_passant_pos,
            *castling_availability,
            Some(move_buffer),
            true,
        );

        let Some((outcome, new_en_passant_pos)) = outcome else {
            return None;
        };

        self.inline_apply_full_move(
            move_buffer,
            en_passant_pos,
            new_en_passant_pos,
            castling_availability,
        );

        Some(outcome)
    }

    #[inline(always)]
    pub fn inline_make_move(
        &mut self,
        mov: Move,
        en_passant_pos: &mut Option<Pos>,
        castling_availability: &mut CastlingAvailability,
        move_buffer: &mut Vec<MovePart>,
        allow_illegal: bool,
        log_logic: bool,
    ) -> Option<RawMoveOutcome> {
        tracing::trace!("Begin making move");
        let Some(from_piece) = self.index(mov.movement().from) else {
            tracing::trace!("End making move (invalid)");
            return None;
        };
        let color_moving = from_piece.color();

        let outcome = self.inline_calculate_outcome(
            mov,
            *en_passant_pos,
            *castling_availability,
            Some(move_buffer),
            log_logic,
        );

        let Some((move_kind, new_en_passant_pos)) = outcome else {
            tracing::trace!("End making move (invalid)");
            return None;
        };

        if !allow_illegal {
            let is_legal = || {
                let our_king_squares = self.piece_squares_by_color[color_moving]
                    & self.piece_squares_by_kind[PieceKind::King];

                // TODO: Is this an improvement?
                if std::hint::unlikely(our_king_squares.get_pos(mov.movement().from)) {
                    // King is moving.
                    // This means pins don't matter, the king can't be pinned.
                    if self.threatened_squares_by_color[!color_moving].get_pos(mov.movement().to) {
                        // Don't let the king walk into a threat.
                        return false;
                    }

                    let is_capture = self.piece_positions.bitboard().get_pos(mov.movement().to);

                    for &check_path in &self.check_paths {
                        if check_path.get_pos(mov.movement().to) {
                            if !is_capture {
                                // The king needs to actually move out of the check if it isn't a capture.
                                return false;
                            }
                        } else {
                            // The king can't flee in a straight line away from sliding pieces.
                            let mut checking_piece_squares_it =
                                (check_path & self.piece_positions.bitboard() & !our_king_squares)
                                    .positions();
                            let checking_piece_pos = checking_piece_squares_it
                                .next()
                                .expect("Check path must contain the checking piece");
                            debug_assert_eq!(checking_piece_squares_it.next(), None);

                            let checking_piece = self
                                .index(checking_piece_pos)
                                .expect("RawBoard.piece_positions.bitboard() must be up-to-date");

                            if checking_piece.kind().sliding_directions().is_none() {
                                continue;
                            }

                            let prev_pos = mov.movement().from;
                            let new_pos = mov.movement().to;

                            let offset_from_prev_to_new_pos = (
                                new_pos.column_index() as i8 - prev_pos.column_index() as i8,
                                new_pos.row_index() as i8 - prev_pos.row_index() as i8,
                            );
                            let offset_from_check_to_prev_pos = (
                                prev_pos.column_index() as i8
                                    - checking_piece_pos.column_index() as i8,
                                prev_pos.row_index() as i8 - checking_piece_pos.row_index() as i8,
                            );

                            let aligned_column = offset_from_prev_to_new_pos.0.signum()
                                == offset_from_check_to_prev_pos.0.signum();
                            let aligned_row = offset_from_prev_to_new_pos.1.signum()
                                == offset_from_check_to_prev_pos.1.signum();

                            if aligned_column && aligned_row {
                                // The movement was aligned.
                                return false;
                            }
                        }
                    }
                } else {
                    // King is not moving.
                    for &pin_path in &self.pin_paths[color_moving] {
                        if pin_path.get_pos(mov.movement().from)
                            && !pin_path.get_pos(mov.movement().to)
                        {
                            // A pinned piece moved out of the pin and the pinning piece was not captured.
                            return false;
                        }
                    }

                    // All checks must also be blocked, since the king isn't moving out of them.
                    for &check_path in &self.check_paths {
                        if !check_path.get_pos(mov.movement().to) {
                            // This check wasn't blocked or captured.
                            return false;
                        }
                    }
                }

                return true;
            };

            // TODO: is this an improvement?
            if !std::hint::likely(is_legal()) {
                return None;
            }
        }

        self.inline_apply_full_move(
            move_buffer,
            en_passant_pos,
            new_en_passant_pos,
            castling_availability,
        );

        // TODO: Check if this move causes check or checkmate.
        let is_check = false;
        let is_checkmate = false;

        let is_castle = matches!(move_kind, RawMoveKind::Castle);

        debug_assert_eq!(mov.promotion().is_some(), move_kind == RawMoveKind::Promote);
        let promotion = mov.promotion();

        let outcome = RawMoveOutcome::new(
            move_kind.is_capture(),
            is_check,
            is_checkmate,
            is_castle,
            promotion,
        );

        tracing::trace!("End making move");
        Some(outcome)
    }

    pub fn make_move(
        &mut self,
        mov: Move,
        en_passant_pos: &mut Option<Pos>,
        castling_availability: &mut CastlingAvailability,
        move_buffer: &mut Vec<MovePart>,
        interactive: bool,
    ) -> Option<RawMoveOutcome> {
        let Some(piece) = self.index(mov.movement().from) else {
            return None;
        };
        let moving_color = piece.color();

        let Some(outcome) = self.inline_make_move(
            mov,
            en_passant_pos,
            castling_availability,
            move_buffer,
            false,
            interactive,
        ) else {
            return None;
        };

        if interactive || cfg!(debug_assertions) {
            self.assert_available_moves_match_valid_moves(
                *en_passant_pos,
                *castling_availability,
                !moving_color,
            );
        }

        Some(outcome)
    }

    #[inline]
    pub fn inline_is_move_legal(
        &self,
        mov: Move,
        mut en_passant_target_square: Option<Pos>,
        mut castling_availability: CastlingAvailability,
    ) -> bool {
        // FIXME: Inefficient.
        let mut move_buffer = Vec::with_capacity(8);
        let mut board = self.clone();

        board
            .inline_make_move(
                mov,
                &mut en_passant_target_square,
                &mut castling_availability,
                &mut move_buffer,
                false,
                false,
            )
            .is_some()
    }

    pub fn is_move_legal(
        &self,
        mov: Move,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> bool {
        self.inline_is_move_legal(mov, en_passant_target_square, castling_availability)
    }

    #[inline]
    pub fn inline_is_move_pseudolegal(
        &self,
        mov: Move,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> bool {
        let outcome = self.inline_calculate_outcome(
            mov,
            en_passant_target_square,
            castling_availability,
            None,
            false,
        );

        outcome.is_some()
    }

    pub fn is_move_pseudolegal(
        &self,
        mov: Move,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> bool {
        self.inline_is_move_pseudolegal(mov, en_passant_target_square, castling_availability)
    }

    #[inline(always)]
    pub fn inline_possible_move_squares_from(
        &self,
        threat_lookup: &ThreatLookup,
        from: Pos,
    ) -> Option<BitBoard> {
        let Some(piece) = self.index(from) else {
            return None;
        };
        let mut squares = self
            .inline_threatened_squares_from(threat_lookup, from)
            .unwrap();
        let ally_piece_squares = self.piece_squares_by_color[piece.color()];

        match piece.kind() {
            PieceKind::Pawn => {
                let forward = piece.color().forward();
                if let Some(forward_pos) = from + forward {
                    squares.set_pos(forward_pos, true);

                    // Check for double-move.
                    if from.rank() == PieceKind::Pawn.starting_rank(piece.color())
                        && self.index(forward_pos).is_none()
                    {
                        let double_forward_pos = (forward_pos + forward).unwrap();
                        squares.set_pos(double_forward_pos, true);
                    }
                }
            }
            _ => {}
        }

        squares &= !ally_piece_squares;

        Some(squares)
    }

    pub fn possible_move_squares_from(
        &self,
        threat_lookup: &ThreatLookup,
        from: Pos,
    ) -> Option<BitBoard> {
        self.inline_possible_move_squares_from(threat_lookup, from)
    }

    #[inline(always)]
    pub fn inline_pseudolegal_move_squares_from(
        &self,
        threat_lookup: &ThreatLookup,
        from: Pos,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Option<BitBoard> {
        let Some(piece) = self.index(from) else {
            return None;
        };

        #[inline]
        fn pawn_filter(
            board: &RawBoard,
            from: Pos,
            en_passant_target_square: Option<Pos>,
            mut bb: BitBoard,
        ) -> BitBoard {
            #[inline]
            fn get_straight_move_squares(file: u8) -> BitBoard {
                BitBoard::file(file)
            }

            // SAFETY: Was checked above that piece is not None.
            let piece = unsafe { board.index(from).unwrap_unchecked() };

            let en_passant_squares = en_passant_target_square
                .map(BitBoard::from_position)
                .unwrap_or(BitBoard::none());
            let opponent_piece_squares = board.piece_squares_by_color[!piece.color()];
            let straight_move_squares = get_straight_move_squares(from.file());

            bb &= !(straight_move_squares & opponent_piece_squares);
            bb &= straight_move_squares | opponent_piece_squares | en_passant_squares;

            if from.rank() == PieceKind::Pawn.starting_rank(piece.color())
                && board
                    .index((from + piece.color().forward()).unwrap())
                    .is_some()
            {
                bb &= !BitBoard::from_position(from).shift_rank(-2 * piece.color().forward_y());
            }

            bb
        }

        #[inline]
        fn king_filter(
            board: &RawBoard,
            from: Pos,
            castling_availability: CastlingAvailability,
            mut bb: BitBoard,
        ) -> BitBoard {
            // SAFETY: Was checked above that piece is not None.
            let piece = unsafe { board.index(from).unwrap_unchecked() };

            use crate::castling_masks as masks;
            use crate::castling_positions as pos;

            let threatened_squares = board.threatened_squares_by_color[!piece.color()];
            let occupied_squares = board.piece_squares_by_color.either();

            match piece.color() {
                PieceColor::White => {
                    if castling_availability.white_kingside
                        && (threatened_squares & masks::WHITE_KINGSIDE_NOTHREAT_SQUARES)
                            == BitBoard::none()
                        && (occupied_squares & masks::WHITE_KINGSIDE_NOTOCCUPIED_SQUARES)
                            == BitBoard::none()
                    {
                        bb.set_pos(pos::WHITE_KINGSIDE_KING_POS, true);
                    }
                    if castling_availability.white_queenside
                        && (threatened_squares & masks::WHITE_QUEENSIDE_NOTHREAT_SQUARES)
                            == BitBoard::none()
                        && (occupied_squares & masks::WHITE_QUEENSIDE_NOTOCCUPIED_SQUARES)
                            == BitBoard::none()
                    {
                        bb.set_pos(pos::WHITE_QUEENSIDE_KING_POS, true);
                    }
                }
                PieceColor::Black => {
                    if castling_availability.black_kingside
                        && (threatened_squares & masks::BLACK_KINGSIDE_NOTHREAT_SQUARES)
                            == BitBoard::none()
                        && (occupied_squares & masks::BLACK_KINGSIDE_NOTOCCUPIED_SQUARES)
                            == BitBoard::none()
                    {
                        bb.set_pos(pos::BLACK_KINGSIDE_KING_POS, true);
                    }
                    if castling_availability.black_queenside
                        && (threatened_squares & masks::BLACK_QUEENSIDE_NOTHREAT_SQUARES)
                            == BitBoard::none()
                        && (occupied_squares & masks::BLACK_QUEENSIDE_NOTOCCUPIED_SQUARES)
                            == BitBoard::none()
                    {
                        bb.set_pos(pos::BLACK_QUEENSIDE_KING_POS, true);
                    }
                }
            }

            bb
        }

        let bb = self
            .inline_possible_move_squares_from(threat_lookup, from)
            .unwrap();

        Some(match piece.kind() {
            PieceKind::Pawn => pawn_filter(self, from, en_passant_target_square, bb),
            PieceKind::King => king_filter(self, from, castling_availability, bb),
            _ => bb,
        })
    }

    pub fn pseudolegal_move_squares_from(
        &self,
        threat_lookup: &ThreatLookup,
        from: Pos,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Option<BitBoard> {
        self.inline_pseudolegal_move_squares_from(
            threat_lookup,
            from,
            en_passant_target_square,
            castling_availability,
        )
    }

    #[inline]
    pub fn inline_checkresolving_move_squares_from(
        &self,
        from: Pos,
        king_pos: Pos,
    ) -> Option<BitBoard> {
        let Some(piece) = self.index(from) else {
            return None;
        };

        if from == king_pos {
            return Some(!self.threatened_squares_by_color[!piece.color()]);
        }

        if self.check_paths.len() == 1 {
            return Some(self.check_paths[0]);
        }

        Some(BitBoard::none())
    }

    #[inline]
    pub fn inline_pinkeeping_move_squares_from(&self, from: Pos) -> Option<BitBoard> {
        let Some(piece) = self.index(from) else {
            return None;
        };

        for pin_path in self.pin_paths.index(piece.color()).iter().copied() {
            if pin_path.get_pos(from) {
                return Some(pin_path);
            }
        }

        Some(BitBoard::all())
    }

    #[inline(always)]
    #[tracing::instrument(level = "trace", skip_all)]
    pub fn inline_illegal_en_passant_move_squares_from(
        &self,
        from: Pos,
        en_passant_target_square: Option<Pos>,
    ) -> Option<BitBoard> {
        let Some(piece) = self.index(from) else {
            tracing::trace!("No piece at location.");
            return None;
        };

        let Some(target_square) = en_passant_target_square else {
            tracing::trace!("No en passant square.");
            return Some(BitBoard::none());
        };

        if piece.kind() != PieceKind::Pawn {
            tracing::trace!("Moving piece is not a pawn.");
            return Some(BitBoard::none());
        }

        let en_passant_capture_squares =
            BitBoard::rank(from.rank()) & BitBoard::file(target_square.file());
        let ally_color = piece.color();
        let opponent_color = !ally_color;

        if self.threatened_squares_by_color[opponent_color] & en_passant_capture_squares
            == BitBoard::none()
        {
            tracing::debug!("Captured piece is not threatened by its ally.");
            return Some(BitBoard::none());
        }

        let ally_king_bb =
            self.piece_squares_by_kind[PieceKind::King] & self.piece_squares_by_color[ally_color];
        tracing::trace!("ally_king_bb:\n{ally_king_bb:?}");

        // FIXME: Super inefficient.
        let mut board = self.clone();

        let movement = Movement {
            from,
            to: target_square,
        };
        let mut move_buffer = Vec::with_capacity(8);
        board.make_pseudolegal_move(
            Move::new(movement),
            &mut Some(target_square),
            &mut CastlingAvailability::none(),
            &mut move_buffer,
        );
        tracing::trace!(
            "board.threatened_squares[opponent_color]:\n{:?}",
            board.threatened_squares_by_color[opponent_color],
        );

        if board.threatened_squares_by_color[opponent_color] & ally_king_bb == BitBoard::none() {
            tracing::debug!("En passant is legal.");
            return Some(BitBoard::none());
        } else {
            tracing::debug!("En passant is illegal.");
            return Some(BitBoard::from_position(target_square));
        }
    }

    #[inline(always)]
    pub fn inline_illegal_king_fleeing_squares(&self, king_pos: Pos) -> Option<BitBoard> {
        let mut illegal_squares = BitBoard::none();

        let mut search = {
            #[inline]
            |directions: &[Dir; 4], piece_kind_predicate: fn(PieceKind) -> bool| {
                let ally_color = self.index(king_pos).unwrap().color();

                for &move_dir in directions {
                    let Some(move_pos) = king_pos.add_dir(move_dir) else {
                        continue;
                    };
                    let threat_dir = move_dir.inverse();

                    let mut pos = king_pos;
                    while let Some(new_pos) = pos.add_dir(threat_dir) {
                        if let Some(piece) = self.index(new_pos) {
                            if piece.color() == ally_color {
                                break;
                            }

                            if piece_kind_predicate(piece.kind()) {
                                illegal_squares.set_pos(move_pos, true);
                            }
                            break;
                        }
                        pos = new_pos;
                    }
                }
            }
        };

        search(&Dir::ORTHOGONAL, |kind| {
            matches!(kind, PieceKind::Queen | PieceKind::Rook)
        });
        search(&Dir::DIAGONAL, |kind| {
            matches!(kind, PieceKind::Queen | PieceKind::Bishop)
        });

        Some(illegal_squares)
    }

    #[inline(always)]
    pub fn inline_legal_move_squares_from(
        &self,
        threat_lookup: &ThreatLookup,
        from: Pos,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Option<BitBoard> {
        let Some(piece) = self.index(from) else {
            return None;
        };

        let king_is_in_check = self.check_paths.len() != 0;

        let resolves_check = if king_is_in_check {
            let king_squares = self.piece_squares_by_kind[PieceKind::King]
                & self.piece_squares_by_color[piece.color()];
            let mut king_positions = king_squares.positions();
            let king_pos = king_positions.next().expect("Should have exactly 1 king");
            debug_assert_eq!(king_positions.next(), None);

            let legal = self
                .inline_checkresolving_move_squares_from(from, king_pos)
                .unwrap();
            if legal == BitBoard::none() {
                // No need to check the pseudolegality of the move if it cannot possibly resolve the check,
                // since the move will always be illegal.
                return Some(BitBoard::none());
            }
            legal
        } else {
            BitBoard::all()
        };

        let king_moving_into_danger = if piece.kind() == PieceKind::King {
            self.threatened_squares_by_color[!piece.color()]
        } else {
            BitBoard::none()
        };

        let king_illegal_fleeing = if piece.kind() == PieceKind::King {
            self.inline_illegal_king_fleeing_squares(from).unwrap()
        } else {
            BitBoard::none()
        };

        let pinkeeping = self.inline_pinkeeping_move_squares_from(from).unwrap();

        let pseudolegal = self
            .inline_pseudolegal_move_squares_from(
                threat_lookup,
                from,
                en_passant_target_square,
                castling_availability,
            )
            .unwrap();

        let illegal_en_passant = self
            .inline_illegal_en_passant_move_squares_from(from, en_passant_target_square)
            .unwrap();

        let legal_move_squares = pseudolegal
            & resolves_check
            & pinkeeping
            & !king_moving_into_danger
            & !king_illegal_fleeing
            & !illegal_en_passant;
        Some(legal_move_squares)
    }

    pub fn legal_move_squares_from(
        &self,
        threat_lookup: &ThreatLookup,
        from: Pos,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Option<BitBoard> {
        self.inline_legal_move_squares_from(
            threat_lookup,
            from,
            en_passant_target_square,
            castling_availability,
        )
    }

    #[inline]
    pub fn inline_moves_from_move_squares(
        &self,
        from: Pos,
        to_squares: BitBoard,
    ) -> Option<impl ExactSizeIterator<Item = Move>> {
        let Some(piece) = self.index(from) else {
            return None;
        };

        let it = if piece.kind() == PieceKind::Pawn {
            let promotion_rank = match piece.color() {
                PieceColor::White => 8,
                PieceColor::Black => 1,
            };
            let promotion_squares = BitBoard::rank(promotion_rank);

            let non_promotion_squares = to_squares & !promotion_squares;
            let promotion_squares = to_squares & promotion_squares;

            MaybePromoteMovesFromPosIter::new_with_promotions(
                from,
                non_promotion_squares.positions(),
                promotion_squares.positions(),
            )
        } else {
            MaybePromoteMovesFromPosIter::new_without_promotions(from, to_squares.positions())
        };
        Some(it)
    }

    #[inline(always)]
    pub fn inline_pseudolegal_moves_from(
        &self,
        threat_lookup: &ThreatLookup,
        from: Pos,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Option<impl ExactSizeIterator<Item = Move>> {
        let Some(pseudolegal_move_squares) = self.inline_pseudolegal_move_squares_from(
            threat_lookup,
            from,
            en_passant_target_square,
            castling_availability,
        ) else {
            return None;
        };
        Some(
            self.inline_moves_from_move_squares(from, pseudolegal_move_squares)
                .expect("Already checked that from position has a piece"),
        )
    }

    #[inline(always)]
    pub fn inline_legal_moves_from(
        &self,
        threat_lookup: &ThreatLookup,
        from: Pos,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Option<impl ExactSizeIterator<Item = Move>> {
        let Some(legal_move_squares) = self.inline_legal_move_squares_from(
            threat_lookup,
            from,
            en_passant_target_square,
            castling_availability,
        ) else {
            return None;
        };
        Some(
            self.inline_moves_from_move_squares(from, legal_move_squares)
                .expect("Already checked that from position has a piece"),
        )
    }

    #[inline(always)]
    pub fn inline_pseudolegal_moves(
        &self,
        threat_lookup: &ThreatLookup,
        turn: PieceColor,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Vec<Move> {
        let allied_piece_squares = self.piece_squares_by_color[turn];
        allied_piece_squares
            .positions()
            .filter_map(|from_pos| {
                self.inline_pseudolegal_moves_from(
                    threat_lookup,
                    from_pos,
                    en_passant_target_square,
                    castling_availability,
                )
            })
            .flatten()
            .collect()
    }

    pub fn pseudolegal_moves(
        &self,
        threat_lookup: &ThreatLookup,
        turn: PieceColor,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Vec<Move> {
        self.inline_pseudolegal_moves(
            threat_lookup,
            turn,
            en_passant_target_square,
            castling_availability,
        )
    }

    pub fn legal_moves(
        &self,
        threat_lookup: &ThreatLookup,
        turn: PieceColor,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Vec<Move> {
        self.inline_legal_moves(
            threat_lookup,
            turn,
            en_passant_target_square,
            castling_availability,
        )
    }

    #[inline(always)]
    pub fn inline_legal_moves(
        &self,
        threat_lookup: &ThreatLookup,
        turn: PieceColor,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
    ) -> Vec<Move> {
        let allied_piece_squares = self.piece_squares_by_color[turn];
        allied_piece_squares
            .positions()
            .filter_map(|from_pos| {
                self.inline_legal_moves_from(
                    threat_lookup,
                    from_pos,
                    en_passant_target_square,
                    castling_availability,
                )
            })
            .flatten()
            .collect()
    }

    pub fn count_legal_moves_recursive(
        &self,
        threat_lookup: &ThreatLookup,
        turn: PieceColor,
        en_passant_target_square: Option<Pos>,
        castling_availability: CastlingAvailability,
        move_buffer: &mut Vec<MovePart>,
        plies: u16,
    ) -> usize {
        if plies == 0 {
            return 1;
        }

        let mut legal_moves = 0;
        let mut board;
        let mut new_en_passant_target_square;
        let mut new_castling_availability;

        let allied_piece_squares = self.piece_squares_by_color[turn];
        for from_pos in allied_piece_squares.positions() {
            let Some(moves) = self.inline_legal_moves_from(
                threat_lookup,
                from_pos,
                en_passant_target_square,
                castling_availability,
            ) else {
                continue;
            };
            if plies == 1 {
                legal_moves += moves.len();
                continue;
            }

            for mov in moves {
                new_en_passant_target_square = en_passant_target_square;
                new_castling_availability = castling_availability;
                board = self.clone();
                let outcome = board.inline_make_move(
                    mov,
                    &mut new_en_passant_target_square,
                    &mut new_castling_availability,
                    move_buffer,
                    true,
                    false,
                );
                outcome.expect("Move should already be legal");

                legal_moves += board.count_legal_moves_recursive(
                    threat_lookup,
                    !turn,
                    new_en_passant_target_square,
                    new_castling_availability,
                    move_buffer,
                    plies - 1,
                );
            }
        }

        legal_moves
    }

    #[inline]
    const fn free_path_to_pos_along_dir(&self, mov: Movement, (diff_x, diff_y): (i8, i8)) -> bool {
        assert!(diff_x.abs() == diff_y.abs() || diff_x == 0 || diff_y == 0);
        let dir = Dir::from_xy_signum((diff_x.signum(), diff_y.signum()))
            .expect("There must be a direction to move to");
        let mut curr_pos = mov.from;
        loop {
            let Some(new_pos) = curr_pos.add_dir(dir) else {
                return false;
            };
            curr_pos = new_pos;

            if curr_pos.equals(mov.to) {
                return true;
            }

            if self.index(curr_pos).is_some() {
                return false;
            }
        }
    }

    #[inline]
    pub fn pieces<'a>(&'a self) -> impl Iterator<Item = (Pos, Option<ColPiece>)> + 'a {
        self.pieces
            .iter()
            .copied()
            .enumerate()
            .map(|(index, piece)| {
                (
                    // SAFETY: There are exactly 64 positions, and all flat indices in the range 0..64 are valid.
                    unsafe { Pos::from_flat_index_unchecked(index as u8) },
                    piece,
                )
            })
    }

    /// NOTE: Does not update castling.
    pub fn change_piece_position_unchecked(&mut self, mov: Movement) {
        self.inline_apply_moves_from_buffer(
            &[MovePart::Movement(mov)],
            &mut CastlingAvailability::all(),
        );
    }

    #[inline]
    pub const fn has_piece(&self) -> ByColor<BitBoard> {
        self.piece_squares_by_color
    }

    #[inline]
    pub const fn threatened_squares(&self) -> ByColor<BitBoard> {
        self.threatened_squares_by_color
    }

    #[inline]
    pub const fn piece_locations(&self) -> ByKind<BitBoard> {
        self.piece_squares_by_kind
    }

    #[inline]
    pub const fn piece_colors(&self) -> ByColor<BitBoard> {
        self.piece_squares_by_color
    }

    #[inline]
    pub const fn check_paths(&self) -> &ArrayVec<BitBoard, 4> {
        &self.check_paths
    }

    #[inline]
    pub const fn pin_paths(&self) -> &ByColor<ArrayVec<BitBoard, 8>> {
        &self.pin_paths
    }

    #[inline]
    pub const fn piece_count(&self) -> u8 {
        self.piece_positions.len() as u8
    }

    pub fn fen_part(&self) -> String {
        let mut s = String::with_capacity(40);
        let mut num_empty_spaces = 0;

        for rank in (1..=8).rev() {
            for file in 1..=8 {
                let pos = Pos::from_rank_and_file(rank, file).unwrap();

                let Some(piece) = self.index(pos) else {
                    num_empty_spaces += 1;
                    continue;
                };

                if num_empty_spaces != 0 {
                    write!(&mut s, "{}", num_empty_spaces).unwrap();
                    num_empty_spaces = 0;
                }
                s.push(piece.fen_letter());
            }

            if num_empty_spaces != 0 {
                write!(&mut s, "{}", num_empty_spaces).unwrap();
                num_empty_spaces = 0;
            }
            if rank != 1 {
                s.push('/');
            }
        }

        s
    }
}

impl Index<Pos> for RawBoard {
    type Output = Option<ColPiece>;

    fn index(&self, index: Pos) -> &Self::Output {
        self.pieces
            .get(index.to_flat_index() as usize)
            .expect("Index out of bounds")
    }
}

impl IndexMut<Pos> for RawBoard {
    fn index_mut(&mut self, index: Pos) -> &mut Self::Output {
        self.pieces
            .get_mut(index.to_flat_index() as usize)
            .expect("Index out of bounds")
    }
}

impl Debug for RawBoard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in (0..8).rev() {
            write!(f, "{} ", row + 1)?;

            for col in 0..8 {
                let pos = Pos::from_coords(col, row).expect("Coordinates should be valid");
                if let Some(piece) = self[pos] {
                    f.write_char(piece.fen_letter())?;
                } else {
                    f.write_char('.')?;
                }
            }
            f.write_char('\n')?;
        }
        f.write_str("  ")?;
        for col in 0..8 {
            let pos = Pos::from_coord_indices(col, 0).expect("Coordinates should be valid");
            f.write_char(pos.column_letter_char())?;
        }
        // No trailing newline

        if f.alternate() {
            f.write_char('\n')?;
            writeln!(
                f,
                "\nhas piece:  ({})\n{:?}",
                self.piece_squares_by_color.numeric(),
                self.piece_squares_by_color
            )?;
            writeln!(
                f,
                "\npiece locations:  ({})\n{:?}",
                self.piece_squares_by_kind.numeric(),
                self.piece_squares_by_kind
            )?;
            writeln!(
                f,
                "\nthreatened squares:  ({})\n{:?}",
                self.threatened_squares_by_color.numeric(),
                self.threatened_squares_by_color
            )?;
            for (index, check_path) in self.check_paths.iter().copied().enumerate() {
                writeln!(
                    f,
                    "\ncheck path {}:  ({})\n{:?}",
                    index + 1,
                    check_path.0,
                    check_path
                )?;
            }
            for color in [PieceColor::White, PieceColor::Black] {
                let pin_paths_for_color = self.pin_paths.index(color);
                for (index, pin_path) in pin_paths_for_color.iter().copied().enumerate() {
                    write!(
                        f,
                        "\npin path {color:#?} {}:  ({})\n{:?}",
                        index + 1,
                        pin_path.0,
                        pin_path
                    )?;
                    if index + 1 != pin_paths_for_color.len() || color == PieceColor::White {
                        f.write_char('\n')?;
                    }
                }
            }
            // No trailing newline
        }

        Ok(())
    }
}

macro_rules! write_field {
    ($write:expr, $val:expr, $map:tt) => {
        let val = ($map)($val);
        let val_lines = val.lines();
        for val_line in val_lines {
            writeln!($write, "{}: {}", stringify!($val), val_line).unwrap();
        }
    };
}

impl AssertEq for RawBoard {
    fn string_representation(&self) -> String {
        #![deny(unused_variables)]

        let mut buf = String::with_capacity(1024);
        let o = &mut buf;
        let Self {
            pieces,
            piece_positions,
            piece_squares_by_color,
            piece_squares_by_kind,
            threatened_squares_by_color,
            check_paths,
            pin_paths,
        } = self;

        for row_index in 0..8 {
            write_field!(
                o,
                pieces,
                (|pieces: &[Option<ColPiece>; 64]| pieces
                    .array_chunks::<8>()
                    .nth(row_index)
                    .expect("64 pieces can be split into 8 groups of 8 pieces")
                    .iter()
                    .map(|piece| piece.map(ColPiece::fen_letter).unwrap_or('.'))
                    .collect::<String>())
            );
        }

        write_field!(
            o,
            piece_positions,
            (|piece_positions: &PosBuffer| piece_positions.bitboard().string_representation())
        );

        write_field!(
            o,
            piece_squares_by_color,
            (ByColor::<BitBoard>::string_representation)
        );

        write_field!(
            o,
            piece_squares_by_kind,
            (ByKind::<BitBoard>::string_representation)
        );

        write_field!(
            o,
            threatened_squares_by_color,
            (ByColor::<BitBoard>::string_representation)
        );

        write_field!(
            o,
            check_paths,
            (<[BitBoard] as AssertEq>::string_representation)
        );

        write_field!(
            o,
            pin_paths,
            (<ByColor::<ArrayVec<BitBoard, 8>> as AssertEq>::string_representation)
        );

        buf
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CheckOrPin {
    Check {
        check_threatened_squares: BitBoard,
    },
    Pin {
        pinned_piece_pos: Pos,
        valid_move_squares: BitBoard,
    },
}

pub fn relevant_block_positions_for_sliding_piece(pos: Pos) -> BitBoard {
    let mut result = BitBoard::none();

    for dir in Dir::ALL {
        let mut current = pos;
        while let Some(new) = current.add_dir(dir) {
            current = new;
            if current.add_dir(dir).is_none() {
                break;
            }
            result ^= BitBoard::from_position(current);
        }
    }

    result
}

pub fn relevant_block_positions_for_pawn_double_move(pos: Pos) -> BitBoard {
    let double_move_dir = match pos.rank() {
        2 => Dir::Up,
        7 => Dir::Down,
        _ => return BitBoard::none(),
    };
    let single_move_pos = pos
        .add_dir(double_move_dir)
        .expect("Because of the rank of the position, it must be able to move both up and down");
    BitBoard::from_position(single_move_pos)
}

pub fn calculate_threatened_squares_for_pawn(pos: Pos, color: PieceColor) -> BitBoard {
    let (relative_up_left_dir, relative_up_right_dir) = match color {
        PieceColor::Black => (Dir::DownLeft, Dir::DownRight),
        PieceColor::White => (Dir::UpLeft, Dir::UpRight),
    };
    let up_left_square = if let Some(attack_pos) = pos.add_dir(relative_up_left_dir) {
        BitBoard::from_position(attack_pos)
    } else {
        BitBoard::none()
    };
    let up_right_square = if let Some(attack_pos) = pos.add_dir(relative_up_right_dir) {
        BitBoard::from_position(attack_pos)
    } else {
        BitBoard::none()
    };
    up_left_square | up_right_square
}

pub fn calculate_movement_only_squares_for_pawn(
    pos: Pos,
    color: PieceColor,
    occupied_squares: BitBoard,
) -> BitBoard {
    let relative_up_dir = match color {
        PieceColor::Black => Dir::Down,
        PieceColor::White => Dir::Up,
    };
    let single_move_pos = pos.add_dir(relative_up_dir);

    let single_move_square = BitBoard::from_optional_position(single_move_pos);
    let double_move_square = BitBoard::from_optional_position(
        single_move_pos.and_then(|pos| pos.add_dir(relative_up_dir)),
    );

    let squares_if_unoccupied = if pos.rank() != color.pawn_double_move_rank() {
        single_move_square
    } else {
        single_move_square | double_move_square
    };
    squares_if_unoccupied & !occupied_squares
}

pub fn calculate_threatened_squares_for_knight(pos: Pos) -> BitBoard {
    let mut squares = BitBoard::none();
    for x in [1, -1] {
        for y in [2, -2] {
            for swap in [false, true] {
                let offset = if !swap { (x, y) } else { (y, x) };

                if let Some(threat_pos) = pos.add_offset(offset) {
                    squares.set_pos(threat_pos, true);
                }
            }
        }
    }
    squares
}

pub fn calculate_threatened_squares_for_king(pos: Pos) -> BitBoard {
    Dir::ALL
        .into_iter()
        .filter_map(|dir| pos.add_dir(dir))
        .map(BitBoard::from_position)
        .fold(BitBoard::none(), BitBoard::bitor)
}

pub fn calculate_threatened_squares_for_sliding_piece(
    pos: Pos,
    occupied_squares: BitBoard,
    sliding_directions: &[Dir],
) -> BitBoard {
    let mut result = BitBoard::none();

    for &dir in sliding_directions {
        let mut current = pos;
        while let Some(new) = current.add_dir(dir) {
            current = new;
            result ^= BitBoard::from_position(current);
            if occupied_squares.get_pos(current) {
                break;
            }
        }
    }

    result
}

pub fn calculate_threatened_squares_for_rook(pos: Pos, occupied_squares: BitBoard) -> BitBoard {
    calculate_threatened_squares_for_sliding_piece(pos, occupied_squares, &Dir::ORTHOGONAL)
}

pub fn calculate_threatened_squares_for_bishop(pos: Pos, occupied_squares: BitBoard) -> BitBoard {
    calculate_threatened_squares_for_sliding_piece(pos, occupied_squares, &Dir::DIAGONAL)
}

pub fn calculate_threatened_squares_for_queen(pos: Pos, occupied_squares: BitBoard) -> BitBoard {
    calculate_threatened_squares_for_sliding_piece(pos, occupied_squares, &Dir::ALL)
}

pub type ZobristHash = u64;

pub struct ZobristNumbers {
    pieces: ByKind<ByColor<ByPos<ZobristHash>>>,
    side_to_move_is_black: ZobristHash,
    // FIXME: Should use a `BySide` struct.
    /// First value is white, second value is black.
    castling_rights: ByColor<[ZobristHash; 2]>,
    // FIXME: Should use a `ByFile` struct.
    en_passant_file: [ZobristHash; 8],
}

impl PartialEq for ZobristNumbers {
    fn eq(&self, _other: &Self) -> bool {
        debug_assert_eq!(
            self as *const ZobristNumbers as usize,
            _other as *const ZobristNumbers as usize
        );
        // There can only ever be a single instance of ZobristNumbers, so all instances are always equal.
        return true;
    }
}

impl Eq for ZobristNumbers {}

impl ZobristNumbers {
    pub fn get() -> &'static Self {
        static GLOBAL_NUMBERS: LazyLock<ZobristNumbers> = LazyLock::new(|| {
            static HAS_BEEN_CREATED: AtomicBool = AtomicBool::new(false);
            let Ok(false) = HAS_BEEN_CREATED.compare_exchange(
                false,
                true,
                atomic::Ordering::SeqCst,
                atomic::Ordering::SeqCst,
            ) else {
                panic!("Zobrist numbers are being recreated");
            };

            let mut rng = DefaultRandomSource;
            let mut random = || {
                let mut number = ZobristHash::default();
                const ZOBRIST_HASH_SIZE: usize = std::mem::size_of::<ZobristHash>();
                // SAFETY: u64 or u128 are just plain-ol-data, and all bit patterns are valid.
                let as_array = unsafe {
                    std::mem::transmute::<&mut ZobristHash, &mut [u8; ZOBRIST_HASH_SIZE]>(
                        &mut number,
                    )
                };
                rng.fill_bytes(as_array);
                number
            };

            let mut pieces_by_color = || ByColor {
                white: ByPos::from_single_value_generator(&mut random),
                black: ByPos::from_single_value_generator(&mut random),
            };
            ZobristNumbers {
                pieces: ByKind {
                    pawn: pieces_by_color(),
                    bishop: pieces_by_color(),
                    knight: pieces_by_color(),
                    rook: pieces_by_color(),
                    queen: pieces_by_color(),
                    king: pieces_by_color(),
                },
                side_to_move_is_black: random(),
                castling_rights: ByColor {
                    white: [random(), random()],
                    black: [random(), random()],
                },
                en_passant_file: [
                    random(),
                    random(),
                    random(),
                    random(),
                    random(),
                    random(),
                    random(),
                    random(),
                ],
            }
        });
        &GLOBAL_NUMBERS
    }
}

#[cfg(test)]
mod light_tests {
    use test_log::test;

    use crate::{assert_move_is_legal, Board};

    use super::*;

    #[test]
    fn example_queen_threatened_squares() {
        // FEN: 3q1k1r/pQ1n1ppp/2p5/2b5/2B5/8/PPP1NnPP/RNBQK2R b KQ - 0 1
        #[rustfmt::skip]
        let occupied_squares = BitBoard::from_visual([
            "...Q.#.#",
            "##.#.###",
            "..#.....",
            "..#.....",
            "..#.....",
            "........",
            "###.####",
            "#####..#",
        ]);
        #[rustfmt::skip]
        let expected_threatened_squares = BitBoard::from_visual([
            "###.##..",
            "..###...",
            ".#...#..",
            "#.....#.",
            ".......#",
            "........",
            "........",
            "........",
        ]);

        let result = calculate_threatened_squares_for_queen(pos!("d8"), occupied_squares);

        let magic_lookup = get_threat_lookup().lookup_queen_threats(pos!("d8"), occupied_squares);
        assert_eq!(magic_lookup, result, "Inconsistent return");

        assert_eq!(result, expected_threatened_squares);
    }

    #[test]
    fn example_bishop_threatened_squares() {
        // FEN: Q2q1k1r/pp1nbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R b KQ - 1 8
        #[rustfmt::skip]
        let occupied_squares = BitBoard::from_visual([
            "#..#.#.#",
            "##.#B###",
            "..#.....",
            "........",
            "..#.....",
            "........",
            "###.####",
            "#####..#",
        ]);
        #[rustfmt::skip]
        let expected_threatened_squares = BitBoard::from_visual([
            "...#.#..",
            "........",
            "...#.#..",
            "..#...#.",
            ".#.....#",
            "#.......",
            "........",
            "........",
        ]);

        let result = calculate_threatened_squares_for_bishop(pos!("e7"), occupied_squares);

        let magic_lookup = get_threat_lookup().lookup_bishop_threats(pos!("e7"), occupied_squares);
        assert_eq!(magic_lookup, result, "Inconsistent return");

        assert_eq!(result, expected_threatened_squares);
    }

    #[test]
    fn has_piece_correct_for_starting_position() {
        let mut board = RawBoard::STARTING_POSITION;
        board.inline_recalculate_piece_squares_by_color();
        assert_eq!(
            board.piece_squares_by_color,
            RawBoard::STARTING_POSITION.piece_squares_by_color
        );
    }

    #[test]
    fn piece_locations_correct_for_starting_position() {
        let mut board = RawBoard::STARTING_POSITION;
        board.inline_recalculate_piece_squares_by_kind();
        assert_eq!(
            board.piece_squares_by_color,
            RawBoard::STARTING_POSITION.piece_squares_by_color
        );
    }

    #[test]
    fn threatened_squares_correct_for_starting_position() {
        let mut board = RawBoard::STARTING_POSITION;
        board.inline_recalculate_threats_and_checks();
        assert_eq!(
            board.threatened_squares_by_color,
            RawBoard::STARTING_POSITION.threatened_squares_by_color
        );
    }

    #[test]
    fn debug_correct_for_starting_position() {
        #[rustfmt::skip]
        let expected = [
           "8 rnbqkbnr\n",
           "7 pppppppp\n",
           "6 ........\n",
           "5 ........\n",
           "4 ........\n",
           "3 ........\n",
           "2 PPPPPPPP\n",
           "1 RNBQKBNR\n",
           "  abcdefgh",
        ].join("");
        let actual = format!("{:?}", RawBoard::STARTING_POSITION);

        assert!(actual.starts_with(&expected));
    }

    #[test]
    fn regression_king_cant_flee_from_sliding_pieces_when_there_is_an_enemy_in_between() {
        assert_move_is_legal!("d2" -> "d1"; "r1Bq1k1r/pp1nbppp/2p5/8/2B5/8/PPPKN1PP/RNBn3R w - - 1 8");
        assert_move_is_legal!("f2" -> "f1"; "rnBqkr2/pp2bppp/2p5/8/2B5/8/PPP1NKPP/RNBQ3R w - - 1 8");
        assert_move_is_legal!("f2" -> "f1"; "rnB2qkr/pp2bppp/2p5/8/2B5/8/PPP1NKPP/RNBQ3R w - - 1 8");
        assert_move_is_legal!("d2" -> "d1"; "rnBq1k1r/pp3ppp/2pb4/8/2B5/8/PPPKN1PP/RNBn3R w - - 1 8");
        assert_move_is_legal!("f2" -> "g1"; "rnB2k1r/pp2bppp/1q6/2p5/2B5/8/PPP1NKPP/RNBQ3R w - - 1 8");
        assert_move_is_legal!("d2" -> "d1"; "r1Nq1k1r/pp1nbppp/2p5/8/2B5/8/PPPKN1PP/RNBn3R w - - 1 8");
        assert_move_is_legal!("f2" -> "f1"; "rnNqkr2/pp2bppp/2p5/8/2B5/8/PPP1NKPP/RNBQ3R w - - 1 8");
        assert_move_is_legal!("f2" -> "f1"; "rnN2qkr/pp2bppp/2p5/8/2B5/8/PPP1NKPP/RNBQ3R w - - 1 8");
        assert_move_is_legal!("d2" -> "d1"; "rnNq1k1r/pp3ppp/2pb4/8/2B5/8/PPPKN1PP/RNBn3R w - - 1 8");
    }

    #[test]
    fn regression_cant_castle_queenside_when_queenside_bishop_position_is_threatened() {
        assert_move_is_legal!("e8" -> "c8"; "r3k1r1/p1pNqpb1/bn1Ppnp1/4P3/1p6/2N2Q1p/PPPBBPPP/R3K2R b KQq - 0 1");
        assert_move_is_legal!("e8" -> "c8"; "r3k1r1/p1pNqpb1/bn1Ppnp1/1N6/1p2P3/5Q1p/PPPBBPPP/R3K2R b KQq - 0 1");
        assert_move_is_legal!("e8" -> "c8"; "r3k1r1/p1pNqpb1/bn1Ppnp1/3N4/1p2P3/5Q1p/PPPBBPPP/R3K2R b KQq - 0 1");
        assert_move_is_legal!("e8" -> "c8"; "r3k1r1/p1pNqpb1/bn1Ppnp1/8/Np2P3/5Q1p/PPPBBPPP/R3K2R b KQq - 0 1");
        assert_move_is_legal!("e8" -> "c8"; "r3k1r1/p1pNqpb1/bn1Ppnp1/8/1p2P3/5Q1p/PPPBBPPP/RN2K2R b KQq - 0 1");
        assert_move_is_legal!("e8" -> "c8"; "r3k1r1/p1pNqpb1/bn1Ppnp1/8/1p2P3/5Q1p/PPPBBPPP/R2NK2R b KQq - 0 1");
        assert_move_is_legal!("e8" -> "c8"; "r3k1r1/p1pNqpb1/bn1PpQp1/8/1p2P3/2N4p/PPPBBPPP/R3K2R b KQq - 0 1");
        assert_move_is_legal!("e8" -> "c8"; "r3k1r1/p1pNqpb1/bn1Ppnp1/5Q2/1p2P3/2N4p/PPPBBPPP/R3K2R b KQq - 0 1");
    }
}

#[cfg(test)]
mod heavy_tests {
    use std::time::Instant;

    use test_log::test;

    use super::*;

    macro_rules! perft_test_suite {
        ($full_board:expr, $([$depth:expr, $expected:expr],)*) => {
            let mut move_buffer = Vec::with_capacity(8);
            let threat_lookup = $crate::threat_lookup::get_threat_lookup();

            let mut calc = |pli| {
                $full_board.raw.count_legal_moves_recursive(
                    threat_lookup,
                    PieceColor::White,
                    $full_board.en_passant_target_square,
                    $full_board.castling_availability,
                    &mut move_buffer,
                    pli,
                )
            };

            let mut calc_with_time = |pli| {
                let start = Instant::now();
                let result = calc(pli);
                let time_taken = start.elapsed();
                (result, time_taken)
            };

            let mut test = |pli, expected| {
                let (result, time_taken) = calc_with_time(pli);
                let ok = if result == expected { "OK" } else { "ERR" };
                println!(
                    "{}: {:<3}  (took {:7} ms to find {:10} leaf nodes)",
                    pli,
                    ok,
                    time_taken.as_millis(),
                    result,
                );
                assert_eq!(result, expected);
            };

            $(
                test($depth, $expected);
            )*
        };
    }

    #[test]
    fn perft_starting_position() {
        perft_test_suite!(
            crate::Board::STARTING_POSITION,
            [1, 20],
            [2, 400],
            [3, 8_902],
            [4, 197_281],
            [5, 4_865_609],
            [6, 119_060_324],
            [7, 3_195_901_860],
        );
    }

    /// From https://www.chessprogramming.org/Perft_Results#Position_2
    #[test]
    fn perft_kiwipete_position() {
        perft_test_suite!(
            crate::Board::from_fen(
                "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1"
            )
            .unwrap(),
            [1, 48],
            [2, 2_039],
            [3, 97_862],
            [4, 4_085_603],
            [5, 193_690_690],
            [6, 8_031_647_685],
        );
    }

    /// From https://www.chessprogramming.org/Perft_Results#Position_5
    #[test]
    fn perft_bugfinder_position() {
        perft_test_suite!(
            crate::Board::from_fen("rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8")
                .unwrap(),
            [1, 44],
            [2, 1_486],
            [3, 62_379],
            [4, 2_103_487],
            [5, 89_941_194],
        );
    }
}

#[cfg(test)]
mod disagreements {
    use test_log::test;

    use crate::disagreement::find_all_disagreements_from;

    /// From https://www.chessprogramming.org/Perft_Results#Position_5
    #[test]
    fn list_bugfinder_position_disagreements() {
        assert_eq!(
            find_all_disagreements_from(
                "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8",
                5,
            )
            .len(),
            0
        );
    }

    /// From https://www.chessprogramming.org/Perft_Results#Position_2
    #[test]
    fn list_kiwipete_position_disagreements() {
        assert_eq!(
            find_all_disagreements_from(
                "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1",
                6,
            )
            .len(),
            0
        );
    }
}
