#[macro_export]
macro_rules! const_for {
    ($var:ident in &mut $arr:expr => $body:block) => {
        let arr = &mut $arr;
        let mut index = 0;
        while index < arr.len() {
            let $var = &mut arr[index];
            $body;
            index += 1;
        }
    };
    ($var:ident in $arr:expr => $body:block) => {
        let arr = $arr;
        let mut index = 0;
        while index < arr.len() {
            let $var = arr[index];
            $body;
            index += 1;
        }
    };
}

pub const fn flatten_nested_array<T: Copy, const INNER: usize, const OUTER: usize>(
    value: &[[T; INNER]; OUTER],
) -> [T; INNER * OUTER] {
    *unsafe {
        value
            .as_flattened()
            .split_first_chunk::<{ INNER * OUTER }>()
            .unwrap_unchecked()
            .0
    }
}

#[cfg(test)]
mod light_tests {
    use super::*;

    #[test]
    fn test_flatten_nested_array_2_by_4() {
        const ARR1: [i32; 4] = [1, 2, 3, 4];
        const ARR2: [i32; 4] = [5, 6, 7, 8];
        const EXPECTED: [i32; 8] = [1, 2, 3, 4, 5, 6, 7, 8];
        const FLATTENED: [i32; 8] = flatten_nested_array(&[ARR1, ARR2]);

        assert_eq!(FLATTENED, EXPECTED);
    }

    #[test]
    fn test_flatten_nested_array_3_by_4() {
        const ARR1: [i32; 4] = [1, 2, 3, 4];
        const ARR2: [i32; 4] = [5, 6, 7, 8];
        const ARR3: [i32; 4] = [9, 10, 11, 12];
        const EXPECTED: [i32; 12] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        const FLATTENED: [i32; 12] = flatten_nested_array(&[ARR1, ARR2, ARR3]);

        assert_eq!(FLATTENED, EXPECTED);
    }
}
