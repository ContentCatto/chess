#![feature(debug_closure_helpers)]
#![feature(stmt_expr_attributes)]
#![feature(const_trait_impl)]
#![feature(const_for)]
#![feature(const_closures)]
#![feature(generic_const_exprs)]
#![feature(fn_traits)]
#![feature(unboxed_closures)]
#![feature(exact_size_is_empty)]
#![feature(iter_map_windows)]
#![feature(array_chunks)]
#![feature(likely_unlikely)]
#![feature(random)]
#![allow(incomplete_features)]
#![deny(unsafe_op_in_unsafe_fn)]

pub mod bitboard;
pub mod board;
pub mod by_color;
pub mod by_kind;
pub mod by_pos;
pub mod const_helpers;
pub mod magic_bitboard;
pub mod moves;
pub mod perft_utils;
pub mod piece;
pub mod pos_buffer;
pub mod position;
pub mod raw_board;
pub mod threat_lookup;

#[cfg(test)]
mod disagreement;
#[cfg(test)]
pub mod stockfish;

use std::{fmt::Debug, ops::Deref};

pub use bitboard::BitBoard;
pub use board::{Board, CastlingAvailability, Promotion, RawMoveKind, RawMoveOutcome};
pub use by_color::ByColor;
pub use by_kind::ByKind;
pub use moves::{Move, MovePart, Movement};
pub use piece::{ColPiece, PieceColor, PieceKind};
pub use position::Pos;
pub use raw_board::RawBoard;

pub mod castling_positions {
    use crate::{pos, Pos};

    pub const WHITE_KING_START_POS: Pos = pos!("e1");
    pub const BLACK_KING_START_POS: Pos = pos!("e8");

    pub const WHITE_KINGSIDE_ROOK_START_POS: Pos = pos!("h1");
    pub const BLACK_KINGSIDE_ROOK_START_POS: Pos = pos!("h8");
    pub const WHITE_QUEENSIDE_ROOK_START_POS: Pos = pos!("a1");
    pub const BLACK_QUEENSIDE_ROOK_START_POS: Pos = pos!("a8");

    pub const WHITE_KINGSIDE_KING_POS: Pos = pos!("g1");
    pub const WHITE_KINGSIDE_ROOK_POS: Pos = pos!("f1");
    pub const WHITE_QUEENSIDE_KING_POS: Pos = pos!("c1");
    pub const WHITE_QUEENSIDE_ROOK_POS: Pos = pos!("d1");

    pub const BLACK_KINGSIDE_KING_POS: Pos = pos!("g8");
    pub const BLACK_KINGSIDE_ROOK_POS: Pos = pos!("f8");
    pub const BLACK_QUEENSIDE_KING_POS: Pos = pos!("c8");
    pub const BLACK_QUEENSIDE_ROOK_POS: Pos = pos!("d8");
}

pub mod castling_masks {
    use crate::BitBoard;

    const WHITE_RANK: BitBoard = BitBoard::rank(1);
    const BLACK_RANK: BitBoard = BitBoard::rank(8);

    const KINGSIDE_NOTHREAT_FILES: BitBoard =
        BitBoard(BitBoard::above_file(4).0 & BitBoard::below_file(8).0);
    const QUEENSIDE_NOTHREAT_FILES: BitBoard =
        BitBoard(BitBoard::above_file(2).0 & BitBoard::below_file(6).0);

    pub const WHITE_KINGSIDE_NOTHREAT_SQUARES: BitBoard =
        BitBoard(WHITE_RANK.0 & KINGSIDE_NOTHREAT_FILES.0);
    pub const WHITE_QUEENSIDE_NOTHREAT_SQUARES: BitBoard =
        BitBoard(WHITE_RANK.0 & QUEENSIDE_NOTHREAT_FILES.0);

    pub const BLACK_KINGSIDE_NOTHREAT_SQUARES: BitBoard =
        BitBoard(BLACK_RANK.0 & KINGSIDE_NOTHREAT_FILES.0);
    pub const BLACK_QUEENSIDE_NOTHREAT_SQUARES: BitBoard =
        BitBoard(BLACK_RANK.0 & QUEENSIDE_NOTHREAT_FILES.0);

    const KINGSIDE_NOTOCCUPIED_FILES: BitBoard =
        BitBoard(BitBoard::above_file(5).0 & BitBoard::below_file(8).0);
    const QUEENSIDE_NOTOCCUPIED_FILES: BitBoard =
        BitBoard(BitBoard::above_file(1).0 & BitBoard::below_file(5).0);

    pub const WHITE_KINGSIDE_NOTOCCUPIED_SQUARES: BitBoard =
        BitBoard(WHITE_RANK.0 & KINGSIDE_NOTOCCUPIED_FILES.0);
    pub const WHITE_QUEENSIDE_NOTOCCUPIED_SQUARES: BitBoard =
        BitBoard(WHITE_RANK.0 & QUEENSIDE_NOTOCCUPIED_FILES.0);

    pub const BLACK_KINGSIDE_NOTOCCUPIED_SQUARES: BitBoard =
        BitBoard(BLACK_RANK.0 & KINGSIDE_NOTOCCUPIED_FILES.0);
    pub const BLACK_QUEENSIDE_NOTOCCUPIED_SQUARES: BitBoard =
        BitBoard(BLACK_RANK.0 & QUEENSIDE_NOTOCCUPIED_FILES.0);

    #[cfg(test)]
    mod light_tests {
        use crate::pos;

        use super::*;

        #[test]
        fn correct_notoccupied_squares_white_kingside() {
            assert_eq!(
                WHITE_KINGSIDE_NOTOCCUPIED_SQUARES,
                BitBoard::from_positions([pos!("f1"), pos!("g1")]),
            );
        }

        #[test]
        fn correct_notoccupied_squares_white_queenside() {
            assert_eq!(
                WHITE_QUEENSIDE_NOTOCCUPIED_SQUARES,
                BitBoard::from_positions([pos!("b1"), pos!("c1"), pos!("d1")]),
            );
        }

        #[test]
        fn correct_notoccupied_squares_black_kingside() {
            assert_eq!(
                BLACK_KINGSIDE_NOTOCCUPIED_SQUARES,
                BitBoard::from_positions([pos!("f8"), pos!("g8")]),
            );
        }

        #[test]
        fn correct_notoccupied_squares_black_queenside() {
            assert_eq!(
                BLACK_QUEENSIDE_NOTOCCUPIED_SQUARES,
                BitBoard::from_positions([pos!("b8"), pos!("c8"), pos!("d8")]),
            );
        }

        #[test]
        fn correct_nothreat_squares_white_kingside() {
            assert_eq!(
                WHITE_KINGSIDE_NOTHREAT_SQUARES,
                BitBoard::from_positions([pos!("e1"), pos!("f1"), pos!("g1")]),
            );
        }

        #[test]
        fn correct_nothreat_squares_white_queenside() {
            assert_eq!(
                WHITE_QUEENSIDE_NOTHREAT_SQUARES,
                BitBoard::from_positions([pos!("c1"), pos!("d1"), pos!("e1")]),
            );
        }

        #[test]
        fn correct_nothreat_squares_black_kingside() {
            assert_eq!(
                BLACK_KINGSIDE_NOTHREAT_SQUARES,
                BitBoard::from_positions([pos!("e8"), pos!("f8"), pos!("g8")]),
            );
        }

        #[test]
        fn correct_nothreat_squares_black_queenside() {
            assert_eq!(
                BLACK_QUEENSIDE_NOTHREAT_SQUARES,
                BitBoard::from_positions([pos!("c8"), pos!("d8"), pos!("e8")]),
            );
        }
    }
}

pub mod castling_moves {
    use super::castling_positions::*;
    use crate::Movement;

    pub const WHITE_KINGSIDE_KING: Movement = Movement {
        from: WHITE_KING_START_POS,
        to: WHITE_KINGSIDE_KING_POS,
    };
    pub const WHITE_QUEENSIDE_KING: Movement = Movement {
        from: WHITE_KING_START_POS,
        to: WHITE_QUEENSIDE_KING_POS,
    };

    pub const BLACK_KINGSIDE_KING: Movement = Movement {
        from: BLACK_KING_START_POS,
        to: BLACK_KINGSIDE_KING_POS,
    };
    pub const BLACK_QUEENSIDE_KING: Movement = Movement {
        from: BLACK_KING_START_POS,
        to: BLACK_QUEENSIDE_KING_POS,
    };

    pub const WHITE_KINGSIDE_ROOK: Movement = Movement {
        from: WHITE_KINGSIDE_ROOK_START_POS,
        to: WHITE_KINGSIDE_ROOK_POS,
    };
    pub const WHITE_QUEENSIDE_ROOK: Movement = Movement {
        from: WHITE_QUEENSIDE_ROOK_START_POS,
        to: WHITE_QUEENSIDE_ROOK_POS,
    };

    pub const BLACK_KINGSIDE_ROOK: Movement = Movement {
        from: BLACK_KINGSIDE_ROOK_START_POS,
        to: BLACK_KINGSIDE_ROOK_POS,
    };
    pub const BLACK_QUEENSIDE_ROOK: Movement = Movement {
        from: BLACK_QUEENSIDE_ROOK_START_POS,
        to: BLACK_QUEENSIDE_ROOK_POS,
    };
}

pub(crate) fn simple_type_name<T>() -> &'static str {
    let name = std::any::type_name::<T>();
    match name.rsplit_once("::") {
        Some((_, name_without_path)) => name_without_path,
        None => name,
    }
}

pub trait AssertEq: Eq + Debug {
    fn string_representation(&self) -> String;

    fn assert_eq(&self, rhs: &Self) {
        let lhs_repr = self.string_representation();
        let rhs_repr = rhs.string_representation();

        if [&lhs_repr, &rhs_repr]
            .iter()
            .any(|repr| repr.lines().count() > 1)
        {
            let mut lhs_lines = lhs_repr.lines();
            let mut rhs_lines = rhs_repr.lines();

            let mut last_unequal_index = None;

            for line_index in 0usize.. {
                if last_unequal_index.map(|uneq_index| uneq_index + 3) == Some(line_index) {
                    break;
                }

                let lhs_line = lhs_lines.next();
                let rhs_line = rhs_lines.next();

                if lhs_line.is_none() && rhs_line.is_none() {
                    break;
                }

                if lhs_line == rhs_line {
                    eprintln!(
                        "{line_index:03}: {}",
                        lhs_line.expect("Just checked that it isn't None")
                    );
                    continue;
                }

                last_unequal_index = Some(line_index);

                let lhs_line = lhs_line.unwrap_or("<repr ended>");
                let rhs_line = rhs_line.unwrap_or("<repr ended>");

                eprintln!("lhs: {}", lhs_line);
                eprintln!("rhs: {}", rhs_line);
            }

            if last_unequal_index.is_some() {
                assert_ne!(self, rhs, "Representations aren't equal but values are");
                panic!("Representations were not equal");
            } else {
                assert_eq!(self, rhs, "Values aren't equal but representations are");
            }
        }
    }
}

impl<T: AssertEq + ?Sized, R: Deref<Target = T> + Eq + Debug> AssertEq for R {
    fn string_representation(&self) -> String {
        self.deref().string_representation()
    }
}

impl<T: AssertEq> AssertEq for [T] {
    fn string_representation(&self) -> String {
        use std::fmt::Write;

        let mut buf = String::with_capacity(1024);

        for (index, elem) in self.into_iter().enumerate() {
            for line in elem.string_representation().lines() {
                writeln!(buf, "{index:03}: {line}").unwrap();
            }
        }

        buf
    }
}

// impl<T: AssertEq, const N: usize> AssertEq for const_arrayvec::ArrayVec<T, N> {
//     fn string_representation(&self) -> String {
//         self.deref().string_representation()
//     }
// }

pub(crate) fn string_representation_by<K: AsRef<str>, V: AssertEq>(
    it: impl IntoIterator<Item = (K, V)>,
) -> String {
    use std::fmt::Write;

    const INDENT: &str = "   ";

    let mut buf = String::with_capacity(1024);

    for (key, value) in it {
        let key = key.as_ref();
        writeln!(buf, "{key}:").unwrap();
        for value_line in value.string_representation().lines() {
            writeln!(buf, "{INDENT}{value_line}").unwrap();
        }
    }

    buf
}
