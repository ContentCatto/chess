use std::{
    error::Error,
    fmt::{Debug, Display},
    num::{NonZeroU8, NonZeroUsize},
    ops::{Index, IndexMut},
};

use crate::{
    bitboard::BitBoard, simple_type_name, threat_lookup::get_threat_lookup, ColPiece, Move,
    MovePart, Movement, PieceColor, PieceKind, Pos, RawBoard,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct CastlingAvailability {
    pub white_kingside: bool,
    pub white_queenside: bool,
    pub black_kingside: bool,
    pub black_queenside: bool,
}

impl CastlingAvailability {
    pub const fn all() -> CastlingAvailability {
        Self {
            white_kingside: true,
            white_queenside: true,
            black_kingside: true,
            black_queenside: true,
        }
    }

    pub const fn is_all(self) -> bool {
        self.white_kingside && self.white_queenside && self.black_kingside && self.black_queenside
    }

    pub const fn none() -> CastlingAvailability {
        Self {
            white_kingside: false,
            white_queenside: false,
            black_kingside: false,
            black_queenside: false,
        }
    }

    pub const fn is_none(self) -> bool {
        !(self.white_kingside
            || self.white_queenside
            || self.black_kingside
            || self.black_queenside)
    }

    pub fn from_fen_part(part: &str) -> Option<Self> {
        if part.is_empty() {
            tracing::error!("FEN string may not have an empty field for castling abilities.");
            return None;
        }

        if part == "-" {
            tracing::debug!("FEN string denotes no possible castling for any side.");
            return Some(Self::none());
        }

        fn count_char(string: &str, target: char) -> usize {
            string.matches(target).count()
        }

        let num_white_king_chars = count_char(&part, 'K');
        let num_white_queen_chars = count_char(&part, 'Q');
        let num_black_king_chars = count_char(&part, 'k');
        let num_black_queen_chars = count_char(&part, 'q');

        if num_white_king_chars > 1
            || num_white_queen_chars > 1
            || num_black_king_chars > 1
            || num_black_queen_chars > 1
        {
            tracing::error!("FEN string contains duplicate characters in field for castling abilities: {part:?}");
            return None;
        }

        let res = Self {
            white_kingside: num_white_king_chars == 1,
            white_queenside: num_white_queen_chars == 1,
            black_kingside: num_black_king_chars == 1,
            black_queenside: num_black_queen_chars == 1,
        };
        tracing::debug!("FEN string denotes {res:?}");

        Some(res)
    }

    #[inline]
    pub const fn for_color(&self, color: PieceColor) -> (bool, bool) {
        match color {
            PieceColor::White => (self.white_kingside, self.white_queenside),
            PieceColor::Black => (self.black_kingside, self.black_queenside),
        }
    }
    #[inline]
    pub const fn king_move_squares_for_color(&self, color: PieceColor) -> BitBoard {
        use crate::castling_positions as pos;

        let (kingside, queenside) = self.for_color(color);
        match (color, kingside, queenside) {
            (PieceColor::White, true, true) => BitBoard(
                BitBoard::from_position(pos::WHITE_KINGSIDE_KING_POS).0
                    | BitBoard::from_position(pos::WHITE_QUEENSIDE_KING_POS).0,
            ),
            (PieceColor::Black, true, true) => BitBoard(
                BitBoard::from_position(pos::BLACK_KINGSIDE_KING_POS).0
                    | BitBoard::from_position(pos::BLACK_QUEENSIDE_KING_POS).0,
            ),
            (PieceColor::White, true, false) => {
                BitBoard::from_position(pos::WHITE_KINGSIDE_KING_POS)
            }
            (PieceColor::Black, true, false) => {
                BitBoard::from_position(pos::BLACK_KINGSIDE_KING_POS)
            }
            (PieceColor::White, false, true) => {
                BitBoard::from_position(pos::WHITE_QUEENSIDE_KING_POS)
            }
            (PieceColor::Black, false, true) => {
                BitBoard::from_position(pos::BLACK_QUEENSIDE_KING_POS)
            }
            (_, false, false) => BitBoard::none(),
        }
    }

    pub fn fen_part(&self) -> String {
        let mut s = String::with_capacity(4);

        if self.white_kingside {
            s.push('K');
        }
        if self.white_queenside {
            s.push('Q');
        }
        if self.black_kingside {
            s.push('k');
        }
        if self.black_queenside {
            s.push('q');
        }

        if s == "" {
            s.push('-');
        }

        s
    }
}

/// Numeric value matches that of [`PieceKind`].
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[repr(u8)]
pub enum Promotion {
    Bishop = 2, // 0b010
    Knight = 3, // 0b011
    Rook = 4,   // 0b100
    Queen = 5,  // 0b101
}

impl Promotion {
    pub const ALL_VALUES: [Promotion; 4] = [
        Promotion::Bishop,
        Promotion::Knight,
        Promotion::Rook,
        Promotion::Queen,
    ];

    #[inline]
    pub const fn from_kind(kind: PieceKind) -> Option<Self> {
        if PieceKind::Bishop as u8 <= kind as u8 && kind as u8 <= PieceKind::Queen as u8 {
            Some(unsafe { Self::from_kind_unchecked(kind) })
        } else {
            None
        }
    }

    #[inline]
    pub const unsafe fn from_kind_unchecked(kind: PieceKind) -> Self {
        unsafe { std::mem::transmute::<u8, Promotion>(kind as u8) }
    }

    #[inline]
    pub const fn to_piece_kind(self) -> PieceKind {
        unsafe { std::mem::transmute::<u8, PieceKind>(self as u8) }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct InvalidPromotionPieceKind(pub PieceKind);

impl Error for InvalidPromotionPieceKind {}

impl Display for InvalidPromotionPieceKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Invalid piece to promote to: {:?}", self.0)
    }
}

impl TryFrom<uci_parser::Piece> for Promotion {
    type Error = InvalidPromotionPieceKind;

    fn try_from(value: uci_parser::Piece) -> Result<Self, Self::Error> {
        let kind = PieceKind::from(value);
        Self::from_kind(kind).ok_or(InvalidPromotionPieceKind(kind))
    }
}

impl From<Promotion> for uci_parser::Piece {
    fn from(value: Promotion) -> Self {
        value.to_piece_kind().into()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[repr(u8)]
pub enum RawMoveKind {
    /// Move to a new square, but capture nothing.
    BasicMovement = 1,

    /// Move to a new square and capture the piece on that square.
    BasicCapture = 2,

    /// Pawn moves 2 squares forward, opening the possibility of en
    /// passant from an enemy pawn. Nothing is captured in this move.
    PawnDoubleMove = 3,

    /// Pawn performs an en passant move on an enemy pawn, capturing
    /// it without stepping on the enemy pawn's current square.
    PawnEnPassant = 4,

    /// King and rook pass eachother. Nothing is captured in this
    /// move.
    Castle = 5,

    /// Pawn moves forward and promotes to a stronger piece.
    Promote = 6,
}

impl RawMoveKind {
    pub const fn is_capture(self) -> bool {
        match self {
            RawMoveKind::BasicCapture => true,
            RawMoveKind::PawnEnPassant => true,
            _ => false,
        }
    }

    pub const fn could_be_capture(self) -> bool {
        self.is_capture()
            || match self {
                RawMoveKind::Promote => true,
                _ => false,
            }
    }

    pub const fn is_basic(self) -> bool {
        match self {
            RawMoveKind::BasicMovement => true,
            RawMoveKind::BasicCapture => true,
            _ => false,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct RawMoveOutcome(NonZeroU8);

#[rustfmt::skip]
impl RawMoveOutcome {
    const CAPTURE: u8        = 0b00001000;
    const CHECK: u8          = 0b00010000;
    const CHECKMATE: u8      = 0b00100000;
    const CASTLE: u8         = 0b01000000;
    const PROMOTE_MASK: u8   = 0b00000111;
    const NO_PROMOTION: u8   = 0b00000111; // Nonzero so that the combined number is always nonzero.
}

impl RawMoveOutcome {
    #[inline]
    pub const fn new(
        is_capture: bool,
        is_check: bool,
        is_checkmate: bool,
        is_castle: bool,
        promotion: Option<Promotion>,
    ) -> Self {
        let mut num = 0;

        if is_capture {
            num |= Self::CAPTURE;
        }

        if is_check {
            num |= Self::CHECK;
        }

        if is_checkmate {
            num |= Self::CHECKMATE;
        }

        if is_castle {
            num |= Self::CASTLE;
        }

        num |= match promotion {
            Some(prom) => prom as u8,
            None => Self::NO_PROMOTION,
        };

        Self(unsafe { NonZeroU8::new_unchecked(num) })
    }

    #[inline]
    pub const fn is_capture(self) -> bool {
        (self.0.get() & Self::CAPTURE) != 0
    }

    #[inline]
    pub const fn is_check(self) -> bool {
        (self.0.get() & Self::CHECK) != 0
    }

    #[inline]
    pub const fn is_checkmate(self) -> bool {
        (self.0.get() & Self::CHECKMATE) != 0
    }

    #[inline]
    pub const fn is_castle(self) -> bool {
        (self.0.get() & Self::CASTLE) != 0
    }

    #[inline]
    pub const fn is_promotion(self) -> bool {
        (self.0.get() & Self::PROMOTE_MASK) != Self::NO_PROMOTION
    }

    #[inline]
    pub const fn is_promotion_to(self, promotion: Promotion) -> bool {
        (self.0.get() & Self::PROMOTE_MASK) == promotion as u8
    }

    #[inline]
    pub const fn promotion(self) -> Option<Promotion> {
        if self.is_promotion() {
            let num = self.0.get() & Self::PROMOTE_MASK;
            Some(unsafe { std::mem::transmute::<u8, Promotion>(num) })
        } else {
            None
        }
    }
}

impl Debug for RawMoveOutcome {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct(simple_type_name::<RawMoveOutcome>())
            .field("is_capture", &self.is_capture())
            .field("is_check", &self.is_check())
            .field("is_checkmate", &self.is_checkmate())
            .field("is_castle", &self.is_castle())
            .field("promotion", &self.promotion())
            .finish()
    }
}

pub const STARTING_POSITION_FEN: &str = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Board {
    pub raw: RawBoard,
    pub turn_color: PieceColor,
    pub castling_availability: CastlingAvailability,
    pub en_passant_target_square: Option<Pos>,
    pub num_half_moves: usize,
    pub num_full_moves: NonZeroUsize,

    pub last_move: Vec<MovePart>,
}

impl Board {
    pub const STARTING_POSITION: Board = Board {
        raw: RawBoard::STARTING_POSITION,
        turn_color: PieceColor::White,
        castling_availability: CastlingAvailability::all(),
        en_passant_target_square: None,
        num_half_moves: 0,
        num_full_moves: NonZeroUsize::new(1).unwrap(),
        last_move: Vec::new(),
    };

    pub const fn new() -> Self {
        Self {
            raw: RawBoard::new(),
            turn_color: PieceColor::White,
            castling_availability: CastlingAvailability {
                white_queenside: false,
                white_kingside: false,
                black_queenside: false,
                black_kingside: false,
            },
            en_passant_target_square: None,
            num_half_moves: 0,
            num_full_moves: unsafe { NonZeroUsize::new_unchecked(1) },
            last_move: Vec::new(),
        }
    }

    pub fn from_fen(fen: &str) -> Option<Self> {
        let fields: Vec<&str> = fen.split(' ').collect();
        if fields.len() != 6 {
            tracing::error!("FEN string must have exactly 6 fields.");
            return None;
        }

        let pieces = RawBoard::from_fen_part(fields[0])?;

        let turn_color = match fields[1] {
            "w" => PieceColor::White,
            "b" => PieceColor::Black,
            _ => {
                tracing::error!("Invalid FEN active color {:?}", fields[1]);
                return None;
            }
        };

        let castling_availability = CastlingAvailability::from_fen_part(fields[2])?;

        let en_passant_target_square = match fields[3] {
            "-" => None,
            pos_str => match Pos::from_str(pos_str) {
                Some(pos) => Some(pos),
                None => {
                    tracing::error!("Invalid FEN en passant target square {pos_str:?}.");
                    return None;
                }
            },
        };

        let num_half_moves = match fields[4].parse() {
            Ok(num) => num,
            Err(error) => {
                tracing::error!("Invalid FEN number of halfmoves {:?}: {error}", fields[4]);
                return None;
            }
        };
        let num_full_moves = match fields[5].parse() {
            Ok(num) => num,
            Err(error) => {
                tracing::error!("Invalid FEN number of fullmoves {:?}: {error}", fields[5]);
                return None;
            }
        };

        let last_move = Vec::new();

        Some(Self {
            raw: pieces,
            turn_color,
            castling_availability,
            en_passant_target_square,
            num_half_moves,
            num_full_moves,
            last_move,
        })
    }

    pub fn fill_from_fen(&mut self, fen: &str) -> Option<()> {
        *self = Self::from_fen(fen)?;
        Some(())
    }

    pub const fn is_check(&self) -> bool {
        let color_to_move = self.turn_color;
        let king_mask = BitBoard(
            self.raw.piece_locations().index(PieceKind::King).0
                & self.raw.piece_colors().index(color_to_move).0,
        );
        self.raw.threatened_squares().index(color_to_move.not()).0 & king_mask.0
            != BitBoard::none().0
    }

    pub fn is_checkmate(&self) -> bool {
        self.is_game_finished() && self.is_check()
    }

    pub fn is_draw(&self) -> bool {
        self.is_game_finished() && !self.is_check()
    }

    pub fn is_game_finished(&self) -> bool {
        self.legal_moves().is_empty() || self.raw.piece_count() == 2
    }

    pub fn is_move_legal(&self, mov: Move) -> bool {
        self.higher_level_preconditions(mov.movement(), false)
            && self.raw.inline_is_move_legal(
                mov,
                self.en_passant_target_square,
                self.castling_availability,
            )
    }

    pub fn is_move_pseudolegal(&self, mov: Move) -> bool {
        self.higher_level_preconditions(mov.movement(), false)
            && self.raw.inline_is_move_pseudolegal(
                mov,
                self.en_passant_target_square,
                self.castling_availability,
            )
    }

    #[inline]
    fn higher_level_preconditions(&self, mov: Movement, log_issues: bool) -> bool {
        let Some(from_piece) = self.raw[mov.from] else {
            if log_issues {
                tracing::error!("No piece in from position of move:  {mov}");
            }
            return false;
        };

        if from_piece.color() != self.turn_color {
            if log_issues {
                tracing::info!("It is {:#?}'s turn to move.", self.turn_color);
            }
            return false;
        }

        true
    }

    pub fn try_make_move(&mut self, mov: Move, interactive: bool) -> Option<RawMoveOutcome> {
        if !self.higher_level_preconditions(mov.movement(), true) {
            return None;
        }

        let Some(outcome) = self.raw.make_move(
            mov,
            &mut self.en_passant_target_square,
            &mut self.castling_availability,
            &mut self.last_move,
            interactive,
        ) else {
            return None;
        };

        self.turn_color = !self.turn_color;

        Some(outcome)
    }

    pub fn legal_move_squares_from(&self, from: Pos) -> BitBoard {
        let Some(piece) = self[from] else {
            return BitBoard::none();
        };

        if piece.color() != self.turn_color {
            return BitBoard::none();
        }

        self.raw
            .inline_legal_move_squares_from(
                get_threat_lookup(),
                from,
                self.en_passant_target_square,
                self.castling_availability,
            )
            .expect("already checked that there is a piece there")
    }

    pub fn pseudolegal_move_squares_from(&self, from: Pos) -> BitBoard {
        let Some(piece) = self[from] else {
            return BitBoard::none();
        };

        if piece.color() != self.turn_color {
            return BitBoard::none();
        }

        self.raw
            .inline_pseudolegal_move_squares_from(
                get_threat_lookup(),
                from,
                self.en_passant_target_square,
                self.castling_availability,
            )
            .expect("already checked that there is a piece there")
    }

    pub fn legal_moves(&self) -> Vec<Move> {
        self.raw.legal_moves(
            get_threat_lookup(),
            self.turn_color,
            self.en_passant_target_square,
            self.castling_availability,
        )
    }

    pub fn after_each_legal_move(&self) -> impl Iterator<Item = (Move, Board)> + '_ {
        self.after_each_sorted_legal_move(&mut |_, _| {})
    }

    pub fn after_each_sorted_legal_move(
        &self,
        sort_moves: &mut impl FnMut(&Board, &mut [Move]),
    ) -> impl Iterator<Item = (Move, Board)> + '_ {
        let mut legal_moves = self.legal_moves();
        sort_moves(self, &mut legal_moves);
        legal_moves.into_iter().map(|mov| {
            let mut new_board = self.clone();
            new_board
                .try_make_move(mov, false)
                .expect("Move was returned by Board::legal_moves() and so must be legal");
            (mov, new_board)
        })
    }

    pub fn pieces<'a>(&'a self) -> impl Iterator<Item = (Pos, Option<ColPiece>)> + 'a {
        self.raw.pieces()
    }

    pub const fn raw_board<'a>(&'a self) -> &'a RawBoard {
        &self.raw
    }

    pub fn change_piece_position_unchecked(&mut self, mov: Movement) {
        self.raw.change_piece_position_unchecked(mov)
    }

    pub fn to_fen(&self) -> String {
        let piece_placement = self.raw.fen_part();
        let turn = match self.turn_color {
            PieceColor::White => 'w',
            PieceColor::Black => 'b',
        };
        let castling_availability = self.castling_availability.fen_part();
        let en_passant_target_square = match self.en_passant_target_square {
            Some(pos) => {
                assert!(matches!(pos.rank(), 3 | 6));
                format!("{}", pos)
            }
            None => "-".to_owned(),
        };
        let halfmove_clock = self.num_half_moves;
        let fullmove_clock = self.num_full_moves;

        format!("{piece_placement} {turn} {castling_availability} {en_passant_target_square} {halfmove_clock} {fullmove_clock}")
    }

    pub fn count_legal_moves_recursive_with_provided_move_buffer(
        &self,
        plies: u16,
        move_buffer: &mut Vec<MovePart>,
    ) -> usize {
        self.raw.count_legal_moves_recursive(
            get_threat_lookup(),
            self.turn_color,
            self.en_passant_target_square,
            self.castling_availability,
            move_buffer,
            plies,
        )
    }

    pub fn count_legal_moves_recursive(&self, plies: u16) -> usize {
        self.count_legal_moves_recursive_with_provided_move_buffer(
            plies,
            &mut Vec::with_capacity(8),
        )
    }
}

impl Index<Pos> for Board {
    type Output = Option<ColPiece>;

    fn index(&self, index: Pos) -> &Self::Output {
        &self.raw[index]
    }
}

impl IndexMut<Pos> for Board {
    fn index_mut(&mut self, index: Pos) -> &mut Self::Output {
        &mut self.raw[index]
    }
}

impl Debug for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{:?}", self.raw)?;

        match self.turn_color {
            PieceColor::White => writeln!(f, "white to move")?,
            PieceColor::Black => writeln!(f, "black to move")?,
        };

        write!(f, "available castle: ")?;
        let mut available_castles = Vec::new();
        if self.castling_availability.white_kingside {
            available_castles.push("white kingside");
        }
        if self.castling_availability.white_queenside {
            available_castles.push("white queenside");
        }
        if self.castling_availability.black_kingside {
            available_castles.push("black kingside");
        }
        if self.castling_availability.black_queenside {
            available_castles.push("black queenside");
        }
        if available_castles.is_empty() {
            write!(f, "none")?;
        } else {
            write!(f, "{}", available_castles.join(", "))?;
        }
        writeln!(f)?;

        if let Some(pos) = self.en_passant_target_square {
            writeln!(f, "en passant square: {}", pos)?;
        } else {
            writeln!(f, "no en passant possible")?;
        }

        writeln!(f, "halfmoves: {}", self.num_half_moves)?;
        writeln!(f, "fullmoves: {}", self.num_full_moves.get())?;

        Ok(())
    }
}

#[cfg(test)]
mod light_tests {
    use test_log::test;

    use crate::{pos, AssertEq};

    use super::*;

    #[test]
    fn board_starting_position_is_correct() {
        let board = Board::STARTING_POSITION;
        assert_eq!(pos!("a8").to_flat_index(), 0);
        assert_eq!(
            board[pos!("a8")],
            Some(ColPiece::new(PieceKind::Rook, PieceColor::Black)),
        );
        assert_eq!(pos!("h8").to_flat_index(), 7);
        assert_eq!(
            board[pos!("h8")],
            Some(ColPiece::new(PieceKind::Rook, PieceColor::Black)),
        );
        assert_eq!(pos!("a1").to_flat_index(), 56);
        assert_eq!(
            board[pos!("a1")],
            Some(ColPiece::new(PieceKind::Rook, PieceColor::White)),
        );
        assert_eq!(pos!("h1").to_flat_index(), 63);
        assert_eq!(
            board[pos!("h1")],
            Some(ColPiece::new(PieceKind::Rook, PieceColor::White)),
        );
    }

    #[test]
    fn board_from_starting_position_fen() {
        let expected = Board::STARTING_POSITION;
        let from_fen = Board::from_fen(STARTING_POSITION_FEN).expect("FEN should be valid");
        from_fen.raw.assert_eq(&expected.raw);
        assert_eq!(from_fen, expected);
    }

    #[test]
    fn board_from_e4_start_fen() {
        let mut expected = Board::STARTING_POSITION;
        expected.change_piece_position_unchecked(Movement {
            from: pos!("e2"),
            to: pos!("e4"),
        });
        expected.en_passant_target_square = Some(pos!("e3"));
        expected.turn_color = PieceColor::Black;

        let from_fen =
            Board::from_fen("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1")
                .expect("FEN should be valid");
        assert_eq!(from_fen, expected);
    }

    #[test]
    fn start_pos_to_fen() {
        assert_eq!(&Board::STARTING_POSITION.to_fen(), STARTING_POSITION_FEN);
    }

    #[test]
    fn e4_start_pos_to_fen() {
        let mut board = Board::STARTING_POSITION;
        board.change_piece_position_unchecked(Movement {
            from: pos!("e2"),
            to: pos!("e4"),
        });
        board.en_passant_target_square = Some(pos!("e3"));
        board.turn_color = PieceColor::Black;

        assert_eq!(
            &board.to_fen(),
            "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1"
        );
    }

    #[test]
    fn promotion_same_value_as_kind() {
        assert_eq!(Promotion::Bishop as u8, PieceKind::Bishop as u8);
        assert_eq!(Promotion::Knight as u8, PieceKind::Knight as u8);
        assert_eq!(Promotion::Rook as u8, PieceKind::Rook as u8);
        assert_eq!(Promotion::Queen as u8, PieceKind::Queen as u8);
    }

    #[test]
    fn correct_size_promotion() {
        assert_eq!(std::mem::size_of::<Promotion>(), 1);
        assert_eq!(std::mem::size_of::<Option<Promotion>>(), 1);
    }

    #[test]
    fn correct_size_raw_move_kind() {
        assert_eq!(std::mem::size_of::<RawMoveKind>(), 1);
        assert_eq!(std::mem::size_of::<Option<RawMoveKind>>(), 1);
    }

    #[test]
    fn correct_size_raw_move_outcome() {
        assert_eq!(std::mem::size_of::<RawMoveOutcome>(), 1);
        assert_eq!(std::mem::size_of::<Option<RawMoveOutcome>>(), 1);
    }
}
