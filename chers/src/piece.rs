use std::{fmt::Debug, num::NonZeroU8, ops::Not};

use crate::{const_for, position::Dir, simple_type_name, Promotion};

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[repr(u8)]
pub enum PieceKind {
    Pawn = 1,
    Bishop = 2,
    Knight = 3,
    Rook = 4,
    Queen = 5,
    King = 6,
}

const KIND_MASK: u8 = 0b0000_0111;

impl PieceKind {
    pub const ALL_VALUES: [PieceKind; 6] = [
        PieceKind::Pawn,
        PieceKind::Bishop,
        PieceKind::Knight,
        PieceKind::Rook,
        PieceKind::Queen,
        PieceKind::King,
    ];

    pub const fn from_fen_letter(ch: char) -> Option<Self> {
        match ch.to_ascii_lowercase() {
            'p' => Some(PieceKind::Pawn),
            'b' => Some(PieceKind::Bishop),
            'n' => Some(PieceKind::Knight),
            'r' => Some(PieceKind::Rook),
            'q' => Some(PieceKind::Queen),
            'k' => Some(PieceKind::King),
            _ => None,
        }
    }

    pub const fn fen_letter_without_case(self) -> char {
        match self {
            PieceKind::Pawn => 'p',
            PieceKind::Bishop => 'b',
            PieceKind::Knight => 'n',
            PieceKind::Rook => 'r',
            PieceKind::Queen => 'q',
            PieceKind::King => 'k',
        }
    }

    pub const fn fen_letter(self, color: PieceColor) -> char {
        let ch = self.fen_letter_without_case();
        match color {
            PieceColor::White => ch.to_ascii_uppercase(),
            PieceColor::Black => ch,
        }
    }

    pub const fn starting_rank(self, color: PieceColor) -> u8 {
        match self {
            PieceKind::Pawn => match color {
                PieceColor::White => 2,
                PieceColor::Black => 7,
            },
            PieceKind::Bishop
            | PieceKind::Knight
            | PieceKind::Rook
            | PieceKind::Queen
            | PieceKind::King => match color {
                PieceColor::White => 1,
                PieceColor::Black => 8,
            },
        }
    }

    pub const fn can_attack_in_direction(self, dir: Dir) -> bool {
        const LOOKUP: [[bool; Dir::ALL_VALUES.len()]; PieceKind::King as usize + 1] = {
            let mut lookup = [[false; Dir::ALL_VALUES.len()]; PieceKind::King as usize + 1];

            const_for!(dir in Dir::DIAGONAL => {
                lookup[PieceKind::Bishop as usize][dir as usize] = true;
            });
            const_for!(dir in Dir::ORTHOGONAL => {
                lookup[PieceKind::Rook as usize][dir as usize] = true;
            });
            const_for!(dir in Dir::ALL => {
                lookup[PieceKind::Queen as usize][dir as usize] = true;
            });

            lookup
        };

        LOOKUP[self as usize][dir as usize]
    }

    pub const fn can_attack_from_direction(self, dir: Dir) -> bool {
        const LOOKUP: [[bool; Dir::ALL_VALUES.len()]; PieceKind::King as usize + 1] = {
            let mut lookup = [[false; Dir::ALL_VALUES.len()]; PieceKind::King as usize + 1];

            const_for!(piece_kind in PieceKind::ALL_VALUES => {
                const_for!(dir in Dir::ALL => {
                    lookup[piece_kind as usize][dir as usize] = piece_kind.can_attack_in_direction(dir.inverse());
                });
            });

            lookup
        };

        LOOKUP[self as usize][dir as usize]
    }

    pub const fn sliding_directions(self) -> Option<&'static [Dir]> {
        match self {
            PieceKind::Bishop => Some(&Dir::DIAGONAL),
            PieceKind::Rook => Some(&Dir::ORTHOGONAL),
            PieceKind::Queen => Some(&Dir::ALL),
            PieceKind::Knight | PieceKind::Pawn | PieceKind::King => None,
        }
    }
}

impl From<uci_parser::Piece> for PieceKind {
    fn from(value: uci_parser::Piece) -> Self {
        match value {
            uci_parser::Piece::Pawn => PieceKind::Pawn,
            uci_parser::Piece::Knight => PieceKind::Knight,
            uci_parser::Piece::Bishop => PieceKind::Bishop,
            uci_parser::Piece::Rook => PieceKind::Rook,
            uci_parser::Piece::Queen => PieceKind::Queen,
            uci_parser::Piece::King => PieceKind::King,
        }
    }
}

impl From<PieceKind> for uci_parser::Piece {
    fn from(value: PieceKind) -> Self {
        match value {
            PieceKind::Pawn => uci_parser::Piece::Pawn,
            PieceKind::Knight => uci_parser::Piece::Knight,
            PieceKind::Bishop => uci_parser::Piece::Bishop,
            PieceKind::Rook => uci_parser::Piece::Rook,
            PieceKind::Queen => uci_parser::Piece::Queen,
            PieceKind::King => uci_parser::Piece::King,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[repr(u8)]
pub enum PieceColor {
    Black = 8,
    White = 16,
}

impl PieceColor {
    #[inline]
    pub const fn forward_y(self) -> i8 {
        match self {
            PieceColor::White => -1,
            PieceColor::Black => 1,
        }
    }

    #[inline]
    pub const fn forward(self) -> (i8, i8) {
        (0, self.forward_y())
    }

    #[inline]
    pub const fn colorless_y_offset(self, y: i8) -> i8 {
        match self {
            PieceColor::White => -1 * y,
            PieceColor::Black => y,
        }
    }

    #[inline]
    pub const fn colorless_offset(self, (x, y): (i8, i8)) -> (i8, i8) {
        (x, self.colorless_y_offset(y))
    }

    pub const fn equals(self, rhs: Self) -> bool {
        match (self, rhs) {
            (PieceColor::White, PieceColor::Black) => false,
            (PieceColor::White, PieceColor::White) => true,
            (PieceColor::Black, PieceColor::Black) => true,
            (PieceColor::Black, PieceColor::White) => false,
        }
    }

    #[inline]
    pub const fn not(self) -> Self {
        unsafe {
            std::mem::transmute::<u8, Self>(self as u8 ^ (Self::White as u8 + Self::Black as u8))
        }
    }

    #[inline(always)]
    pub const fn relative_dir(self, dir: Dir) -> Dir {
        match self {
            PieceColor::White => dir,
            PieceColor::Black => dir.inverse(),
        }
    }

    #[inline]
    pub const fn pawn_double_move_rank(self) -> u8 {
        match self {
            PieceColor::Black => 7,
            PieceColor::White => 2,
        }
    }
}

const COLOR_MASK: u8 = 0b0001_1000;
pub const ALL_COLORS: [PieceColor; 2] = [PieceColor::Black, PieceColor::White];

impl Not for PieceColor {
    type Output = PieceColor;

    fn not(self) -> Self::Output {
        self.not()
    }
}

pub mod pieces {
    #![allow(non_upper_case_globals)]

    use crate::{ColPiece, PieceColor, PieceKind};

    pub const P: ColPiece = ColPiece::new(PieceKind::Pawn, PieceColor::White);
    pub const B: ColPiece = ColPiece::new(PieceKind::Bishop, PieceColor::White);
    pub const N: ColPiece = ColPiece::new(PieceKind::Knight, PieceColor::White);
    pub const R: ColPiece = ColPiece::new(PieceKind::Rook, PieceColor::White);
    pub const Q: ColPiece = ColPiece::new(PieceKind::Queen, PieceColor::White);
    pub const K: ColPiece = ColPiece::new(PieceKind::King, PieceColor::White);
    pub const p: ColPiece = ColPiece::new(PieceKind::Pawn, PieceColor::Black);
    pub const b: ColPiece = ColPiece::new(PieceKind::Bishop, PieceColor::Black);
    pub const n: ColPiece = ColPiece::new(PieceKind::Knight, PieceColor::Black);
    pub const r: ColPiece = ColPiece::new(PieceKind::Rook, PieceColor::Black);
    pub const q: ColPiece = ColPiece::new(PieceKind::Queen, PieceColor::Black);
    pub const k: ColPiece = ColPiece::new(PieceKind::King, PieceColor::Black);
}

pub mod pieces_opt {
    #![allow(non_upper_case_globals)]

    use crate::{piece::pieces, ColPiece};

    pub const P: Option<ColPiece> = Some(pieces::P);
    pub const B: Option<ColPiece> = Some(pieces::B);
    pub const N: Option<ColPiece> = Some(pieces::N);
    pub const R: Option<ColPiece> = Some(pieces::R);
    pub const Q: Option<ColPiece> = Some(pieces::Q);
    pub const K: Option<ColPiece> = Some(pieces::K);
    pub const p: Option<ColPiece> = Some(pieces::p);
    pub const b: Option<ColPiece> = Some(pieces::b);
    pub const n: Option<ColPiece> = Some(pieces::n);
    pub const r: Option<ColPiece> = Some(pieces::r);
    pub const q: Option<ColPiece> = Some(pieces::q);
    pub const k: Option<ColPiece> = Some(pieces::k);
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct ColPiece(NonZeroU8);

impl Debug for ColPiece {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let struct_name = simple_type_name::<Self>();
        f.debug_struct(struct_name)
            .field("kind", &self.kind())
            .field("color", &self.color())
            .finish()
    }
}

impl ColPiece {
    #[inline]
    pub const fn new(kind: PieceKind, color: PieceColor) -> Self {
        let num = kind as u8 + color as u8;
        Self(unsafe { NonZeroU8::new_unchecked(num) })
    }

    #[inline]
    pub const fn from_fen_letter(ch: char) -> Option<Self> {
        let kind = match PieceKind::from_fen_letter(ch) {
            Some(kind) => kind,
            None => return None,
        };
        let color = match ch.is_ascii_uppercase() {
            true => PieceColor::White,
            false => PieceColor::Black,
        };

        Some(Self::new(kind, color))
    }

    #[inline]
    pub const fn promoted(self, promotion: Promotion) -> Self {
        Self::new(promotion.to_piece_kind(), self.color())
    }

    #[inline]
    pub const fn fen_letter(self) -> char {
        self.kind().fen_letter(self.color())
    }

    #[inline]
    pub const fn kind(self) -> PieceKind {
        unsafe { std::mem::transmute::<u8, PieceKind>(self.0.get() & KIND_MASK) }
    }

    #[inline]
    pub const fn color(self) -> PieceColor {
        unsafe { std::mem::transmute::<u8, PieceColor>(self.0.get() & COLOR_MASK) }
    }
}

#[cfg(test)]
mod light_tests {
    use super::*;

    #[test]
    fn correct_kind_and_color() {
        for (kind, color) in PieceKind::ALL_VALUES.into_iter().zip(ALL_COLORS) {
            let colpiece = ColPiece::new(kind, color);
            assert_eq!(colpiece.kind(), kind);
            assert_eq!(colpiece.color(), color);
        }
    }

    #[test]
    fn correct_size_kind() {
        assert_eq!(std::mem::size_of::<PieceKind>(), 1);
        assert_eq!(std::mem::size_of::<Option<PieceKind>>(), 1);
    }

    #[test]
    fn correct_size_color() {
        assert_eq!(std::mem::size_of::<PieceColor>(), 1);
        assert_eq!(std::mem::size_of::<Option<PieceColor>>(), 1);
    }

    #[test]
    fn correct_size_colored_piece() {
        assert_eq!(std::mem::size_of::<ColPiece>(), 1);
        assert_eq!(std::mem::size_of::<Option<ColPiece>>(), 1);
    }

    #[test]
    fn correct_debug_format_colpiece() {
        let s = format!("{:?}", ColPiece::new(PieceKind::Knight, PieceColor::Black));

        assert!(s.starts_with("ColPiece"));
        assert!(s.contains(&format!("kind: {:?}", PieceKind::Knight)));
        assert!(s.contains(&format!("color: {:?}", PieceColor::Black)));
    }

    #[test]
    fn correct_simple_type_name() {
        assert_eq!(simple_type_name::<PieceKind>(), "PieceKind");
        assert_eq!(simple_type_name::<PieceColor>(), "PieceColor");
        assert_eq!(simple_type_name::<ColPiece>(), "ColPiece");
    }
}
