use std::{collections::HashSet, fmt::Display, io::Write};

use crate::{stockfish::Stockfish, threat_lookup::get_threat_lookup, Move, MovePart};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Disagreement {
    fen: String,
    mov: Move,
    should_be_allowed: bool,
}

impl Display for Disagreement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let correction = if self.should_be_allowed {
            "allow"
        } else {
            "disallow"
        };
        write!(f, "Should {correction} {} on {}", self.mov, self.fen)
    }
}

pub fn find_all_disagreements_from(fen: &str, plies: u16) -> HashSet<Disagreement> {
    let mut total_found = 0;
    let disagreements = find_all_disagreements_from_impl(
        &mut Stockfish::new(),
        &mut Vec::with_capacity(8),
        &mut total_found,
        fen,
        plies,
    );

    // Extra space at the end to clear the 1 extra character that the status message writes.
    eprintln!("Total found: {total_found} ");

    disagreements
}

fn find_all_disagreements_from_impl(
    stockfish: &mut Stockfish,
    move_buffer: &mut Vec<MovePart>,
    total_found: &mut usize,
    fen: &str,
    plies: u16,
) -> HashSet<Disagreement> {
    let mut stockfish_counts = stockfish.run_perft_for_fen_and_reset(fen, plies);

    let mut disagreements = HashSet::new();

    tracing::debug!(fen);
    let board = crate::Board::from_fen(fen).unwrap();
    tracing::trace!(?board);

    for mov in board.raw.legal_moves(
        // TODO: Should not need to be part of the public API.
        get_threat_lookup(),
        board.turn_color,
        board.en_passant_target_square,
        board.castling_availability,
    ) {
        tracing::debug!(%mov, "Trying move");
        let mut board = board.clone();

        board
            .try_make_move(mov, false)
            .expect("Already checked to be valid");

        let our_result =
            board.count_legal_moves_recursive_with_provided_move_buffer(plies - 1, move_buffer);

        let stockfish_expected = stockfish_counts.remove(&mov);

        let Some(stockfish_expected) = stockfish_expected else {
            // Stockfish doesn't count this one as valid.
            let disagreement = Disagreement {
                fen: fen.to_owned(),
                mov,
                should_be_allowed: false,
            };
            disagreement_found(disagreement, &mut disagreements, total_found);
            continue;
        };

        // Our algorithm and stockfish's algorithm seem to agree (might be a false negative still).
        if our_result == stockfish_expected {
            continue;
        }

        let fen = board.to_fen();
        tracing::info!("Expected {stockfish_expected} not {our_result} for fen {fen}");

        let added =
            find_all_disagreements_from_impl(stockfish, move_buffer, total_found, &fen, plies - 1);
        disagreements.extend(added.iter().cloned());
        assert_eq!(added.len(), our_result);
    }

    for (should_be_valid_move, _) in stockfish_counts {
        let disagreement = Disagreement {
            fen: fen.to_owned(),
            mov: should_be_valid_move,
            should_be_allowed: true,
        };
        disagreement_found(disagreement, &mut disagreements, total_found);
    }

    disagreements
}

fn disagreement_found(
    disagreement: Disagreement,
    disagreements: &mut HashSet<Disagreement>,
    total_found: &mut usize,
) {
    if disagreements.contains(&disagreement) {
        // No need to print or increment the counter again, this one is already counted.
        return;
    }
    // A new disagreement.
    println!("{disagreement}");
    disagreements.insert(disagreement);
    *total_found += 1;
    eprint!("Found so far: {total_found}\r");
    std::io::stdout().flush().unwrap();
}

#[macro_export]
macro_rules! move_is_legal {
    ($from:literal -> $to:literal; $fen:literal) => {
        Board::from_fen($fen)
            .unwrap()
            .legal_move_squares_from(pos!($from))
            .get_pos(pos!($to))
    };
}

#[macro_export]
macro_rules! assert_move_is_legal {
    ($from:literal -> $to:literal; $fen:literal) => {
        assert!($crate::move_is_legal!($from -> $to; $fen));
    };
}
