use std::{
    fmt::{Debug, Display, Write},
    iter::Map,
    num::NonZeroU8,
    ops::{Add, Range, Sub},
};

use serde::{Deserialize, Serialize};

use crate::{const_helpers, simple_type_name};

/// A position on an 8x8 chess board.
///
/// Display implementation shows the position in the classic letter-number format.
/// ```
/// # use chers::Pos;
/// let pos = Pos::from_str("e4").unwrap();
/// assert_eq!(&format!("{pos}"), "e4");
/// ```
///
/// Layout of the byte:  UARRRCCC
/// U - Unused.
/// A - Always 1.
/// R - Row, the number part of the position.
/// C - Column, the letter part of the position.
///
/// Optimized to utilize the niche optimization of [`NonZeroU8`],
/// letting a [`Pos`] be stored in an [`Option`] of the same size.
/// ```
/// # use chers::Pos;
/// assert_eq!(std::mem::size_of::<Pos>(), 1);
/// assert_eq!(std::mem::size_of::<Option<Pos>>(), 1);
/// ```
#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[repr(transparent)]
pub struct Pos(NonZeroU8);

impl Pos {
    #[inline(always)]
    pub const fn from_flat_index(index: u8) -> Option<Self> {
        if index > 63 {
            return None;
        }
        Some(unsafe { Self::from_flat_index_unchecked(index) })
    }

    #[inline(always)]
    pub const fn from_flat_index_likely(index: u8) -> Option<Self> {
        if std::hint::unlikely(index > 63) {
            return None;
        }
        Some(unsafe { Self::from_flat_index_unchecked(index) })
    }

    #[inline(always)]
    pub const unsafe fn from_flat_index_unchecked(index: u8) -> Self {
        Self(unsafe { NonZeroU8::new_unchecked(index ^ 64) })
    }

    #[inline(always)]
    pub const fn to_flat_index(self) -> u8 {
        let val = self.0.get() ^ 64;
        debug_assert!(val < 64);
        // SAFETY: value was checked to be less than 64 when this Pos was created.
        unsafe { std::hint::assert_unchecked(val < 64) };
        val
    }

    #[inline]
    pub const fn from_rank_and_file(rank: u8, file: u8) -> Option<Self> {
        if file == 0 || rank == 0 {
            return None;
        }
        Self::from_coords(file - 1, rank - 1)
    }

    #[inline]
    pub const fn from_rank_and_file_likely(rank: u8, file: u8) -> Option<Self> {
        if std::hint::unlikely(file == 0 || rank == 0) {
            return None;
        }
        Self::from_coords_likely(file - 1, rank - 1)
    }

    #[inline]
    pub const fn from_coords(column: u8, row: u8) -> Option<Self> {
        Self::from_coord_indices(column, 7 - row)
    }

    #[inline]
    pub const fn from_coords_likely(column: u8, row: u8) -> Option<Self> {
        Self::from_coord_indices_likely(column, 7 - row)
    }

    #[inline]
    pub const fn from_coord_indices(column_index: u8, row_index: u8) -> Option<Self> {
        if column_index > 7 || row_index > 7 {
            return None;
        }
        Some(unsafe { Self::from_coord_indices_unchecked(column_index, row_index) })
    }

    #[inline]
    pub const fn from_coord_indices_likely(column_index: u8, row_index: u8) -> Option<Self> {
        if std::hint::unlikely(column_index > 7 || row_index > 7) {
            return None;
        }
        Some(unsafe { Self::from_coord_indices_unchecked(column_index, row_index) })
    }

    #[inline]
    pub const unsafe fn from_coord_indices_unchecked(column_index: u8, row_index: u8) -> Self {
        let flat_index = (row_index << 3) + column_index;
        unsafe { Self::from_flat_index_unchecked(flat_index) }
    }

    #[inline]
    pub const fn from_str_bytes(column_letter_byte: u8, row_number_byte: u8) -> Option<Self> {
        if column_letter_byte < b'a' || column_letter_byte > b'h' {
            return None;
        }
        if row_number_byte < b'1' || row_number_byte > b'8' {
            return None;
        }

        let column_index = column_letter_byte - b'a';
        let row_number = row_number_byte - b'1';

        // Invert row so that "a8" is the first position.
        let row_index = 7 - row_number;

        Some(unsafe { Self::from_coord_indices_unchecked(column_index, row_index) })
    }

    #[inline]
    pub const fn from_str(string: &str) -> Option<Self> {
        if string.len() != 2 || !string.is_ascii() {
            return None;
        }

        let column_letter_byte = string.as_bytes()[0];
        let row_number_byte = string.as_bytes()[1];

        Self::from_str_bytes(column_letter_byte, row_number_byte)
    }

    #[inline]
    pub const fn file(self) -> u8 {
        self.column() + 1
    }

    #[inline]
    pub const fn rank(self) -> u8 {
        self.row() + 1
    }

    #[inline]
    pub const fn column(self) -> u8 {
        self.column_index()
    }

    #[inline]
    pub const fn row(self) -> u8 {
        7 - self.row_index()
    }

    #[inline]
    pub const fn column_index(self) -> u8 {
        self.to_flat_index() & 7
    }

    #[inline]
    pub const fn row_index(self) -> u8 {
        (self.to_flat_index() >> 3) & 7
    }

    #[inline]
    pub const fn column_letter_char(self) -> char {
        let ascii_byte = b'a' + self.column();
        unsafe { char::from_u32_unchecked(ascii_byte as u32) }
    }

    #[inline]
    pub const fn row_number_char(self) -> char {
        let ascii_byte = b'1' + self.row();
        unsafe { char::from_u32_unchecked(ascii_byte as u32) }
    }

    #[inline]
    pub const fn equals(self, rhs: Pos) -> bool {
        self.0.get() == rhs.0.get()
    }

    #[inline]
    pub const fn sub_pos(self, rhs: Pos) -> (i8, i8) {
        (
            self.column_index() as i8 - rhs.column_index() as i8,
            self.row_index() as i8 - rhs.row_index() as i8,
        )
    }

    #[inline]
    pub const fn add_offset(self, rhs: (i8, i8)) -> Option<Pos> {
        let new_pos = (
            self.column_index() as i8 + rhs.0,
            self.row_index() as i8 + rhs.1,
        );

        if new_pos.0 < 0 || new_pos.1 < 0 {
            return None;
        }

        Pos::from_coord_indices(new_pos.0 as u8, new_pos.1 as u8)
    }

    #[inline]
    pub const fn add_dir(self, rhs: Dir) -> Option<Pos> {
        Pos::ADD_DIR_LOOKUP_TABLES[rhs as usize][self.to_flat_index() as usize]
    }

    const ADD_DIR_LOOKUP_TABLES: [[Option<Pos>; 64]; 8] = {
        let mut table = [[None; 64]; 8];

        let mut d = 0;
        while d < Dir::ALL_VALUES.len() {
            let dir = Dir::ALL_VALUES[d];

            let mut p = 0;
            while p < Self::POSITIONS.len() {
                let start_pos = Self::POSITIONS[p];
                let end_pos = start_pos.add_offset(dir.as_offset());
                table[dir as usize][start_pos.to_flat_index() as usize] = end_pos;

                p += 1;
            }

            d += 1;
        }

        table
    };

    #[inline]
    pub const fn sub_dir(self, rhs: Dir) -> Option<Pos> {
        self.add_dir(rhs.inverse())
    }

    #[inline]
    pub const fn sub_offset(self, rhs: (i8, i8)) -> Option<Pos> {
        let new_pos = (
            self.column_index() as i8 - rhs.0,
            self.row_index() as i8 - rhs.1,
        );

        if new_pos.0 < 0 || new_pos.1 < 0 {
            return None;
        }

        Pos::from_coord_indices(new_pos.0 as u8, new_pos.1 as u8)
    }

    pub const POSITIONS: [Pos; 64] = {
        let mut positions = [Pos::from_flat_index(0).unwrap(); 64];
        let mut i = 0;
        while i < 64 {
            positions[i as usize] = Pos::from_flat_index(i).unwrap();
            i += 1;
        }
        positions
    };

    #[inline]
    pub fn positions() -> Map<Range<u8>, fn(u8) -> Pos> {
        (0..64).map(|flat_index| unsafe { Pos::from_flat_index_unchecked(flat_index) })
    }
}

impl Debug for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple(simple_type_name::<Self>())
            .field_with(|f| {
                f.write_char('"')?;
                f.write_char(self.column_letter_char())?;
                f.write_char(self.row_number_char())?;
                f.write_char('"')
            })
            .finish()
    }
}

impl Display for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_char(self.column_letter_char())?;
        f.write_char(self.row_number_char())
    }
}

impl Serialize for Pos {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl<'de> Deserialize<'de> for Pos {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        use serde::de::{Error, Unexpected};

        let string = String::deserialize(deserializer)?;

        Pos::from_str(&string).ok_or_else(|| {
            D::Error::invalid_value(
                Unexpected::Str(&string),
                &"a valid letter-number combination to denote a chessboard position",
            )
        })
    }
}

impl Sub<Pos> for Pos {
    type Output = (i8, i8);

    #[inline]
    fn sub(self, rhs: Pos) -> Self::Output {
        self.sub_pos(rhs)
    }
}

impl Add<(i8, i8)> for Pos {
    type Output = Option<Pos>;

    #[inline]
    fn add(self, rhs: (i8, i8)) -> Self::Output {
        self.add_offset(rhs)
    }
}

impl Sub<(i8, i8)> for Pos {
    type Output = Option<Pos>;

    #[inline]
    fn sub(self, rhs: (i8, i8)) -> Self::Output {
        self.sub_offset(rhs)
    }
}

impl From<uci_parser::Square> for Pos {
    fn from(value: uci_parser::Square) -> Self {
        Pos::from_rank_and_file(
            match value.1 {
                uci_parser::Rank::One => 1,
                uci_parser::Rank::Two => 2,
                uci_parser::Rank::Three => 3,
                uci_parser::Rank::Four => 4,
                uci_parser::Rank::Five => 5,
                uci_parser::Rank::Six => 6,
                uci_parser::Rank::Seven => 7,
                uci_parser::Rank::Eight => 8,
            },
            match value.0 {
                uci_parser::File::A => 1,
                uci_parser::File::B => 2,
                uci_parser::File::C => 3,
                uci_parser::File::D => 4,
                uci_parser::File::E => 5,
                uci_parser::File::F => 6,
                uci_parser::File::G => 7,
                uci_parser::File::H => 8,
            },
        )
        .unwrap()
    }
}

impl From<Pos> for uci_parser::Square {
    fn from(value: Pos) -> Self {
        let (rank, file) = (value.rank(), value.file());
        uci_parser::Square(
            match file {
                1 => uci_parser::File::A,
                2 => uci_parser::File::B,
                3 => uci_parser::File::C,
                4 => uci_parser::File::D,
                5 => uci_parser::File::E,
                6 => uci_parser::File::F,
                7 => uci_parser::File::G,
                8 => uci_parser::File::H,
                _ => unreachable!(),
            },
            match rank {
                1 => uci_parser::Rank::One,
                2 => uci_parser::Rank::Two,
                3 => uci_parser::Rank::Three,
                4 => uci_parser::Rank::Four,
                5 => uci_parser::Rank::Five,
                6 => uci_parser::Rank::Six,
                7 => uci_parser::Rank::Seven,
                8 => uci_parser::Rank::Eight,
                _ => unreachable!(),
            },
        )
    }
}

#[macro_export]
macro_rules! pos {
    ($val:literal) => {
        $crate::Pos::from_str($val).expect("Not a valid chess position")
    };
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[repr(u8)]
pub enum Dir {
    UpLeft = 0,
    Up = 1,
    UpRight = 2,
    Left = 3,
    Right = 4,
    DownLeft = 5,
    Down = 6,
    DownRight = 7,
}

impl Dir {
    pub const ALL_VALUES: [Dir; 8] = [
        Dir::UpLeft,
        Dir::Up,
        Dir::UpRight,
        Dir::Left,
        Dir::Right,
        Dir::DownLeft,
        Dir::Down,
        Dir::DownRight,
    ];
    pub const ORTHOGONAL: [Dir; 4] = [Dir::Up, Dir::Left, Dir::Right, Dir::Down];
    pub const DIAGONAL: [Dir; 4] = [Dir::UpLeft, Dir::UpRight, Dir::DownLeft, Dir::DownRight];
    pub const ALL: [Dir; 8] =
        const_helpers::flatten_nested_array(&[Self::ORTHOGONAL, Self::DIAGONAL]);

    pub const FLAT_INDEX_OFFSETS: [i8; 8] = [
        -9, // UpLeft
        -8, // Up
        -7, // UpRight
        -1, // Left
        1,  // Right
        7,  // DownLeft
        8,  // Down
        9,  // DownRight
    ];

    #[inline(always)]
    pub const fn from_x_signum(signum: i8) -> Option<Dir> {
        #[rustfmt::skip]
        const MAPPING: [Option<Dir>; 3] = [
            Some(Dir::Left),
            None,
            Some(Dir::Right),
        ];
        let index = signum + 1;
        MAPPING[index as usize]
    }

    #[inline(always)]
    pub const fn from_y_signum(signum: i8) -> Option<Dir> {
        #[rustfmt::skip]
        const MAPPING: [Option<Dir>; 3] = [
            Some(Dir::Up),
            None,
            Some(Dir::Down),
        ];
        let index = signum + 1;
        MAPPING[index as usize]
    }

    #[inline(always)]
    pub const fn from_xy_signum((col, row): (i8, i8)) -> Option<Dir> {
        const MAPPING: [Option<Dir>; 9] = [
            Some(Dir::UpLeft),
            Some(Dir::Up),
            Some(Dir::UpRight),
            Some(Dir::Left),
            None,
            Some(Dir::Right),
            Some(Dir::DownLeft),
            Some(Dir::Down),
            Some(Dir::DownRight),
        ];

        let index = 4 + row * 3 + col;
        MAPPING[index as usize]
    }

    #[inline(always)]
    pub const fn inverse(self) -> Dir {
        // SAFETY: All members of `Dir` have a negative equivalent.
        unsafe { std::mem::transmute(7 - self as u8) }
    }

    #[inline]
    pub const fn as_offset(self) -> (i8, i8) {
        match self {
            Dir::UpLeft => (-1, -1),
            Dir::Up => (0, -1),
            Dir::UpRight => (1, -1),
            Dir::Left => (-1, 0),
            Dir::Right => (1, 0),
            Dir::DownLeft => (-1, 1),
            Dir::Down => (0, 1),
            Dir::DownRight => (1, 1),
        }
    }
}

#[cfg(test)]
mod light_tests {
    use super::*;

    const _: Pos = pos!("f7");

    fn should_be_none<T: Debug>(value: T) {
        panic!("Should be None: {value:?}")
    }

    #[test]
    fn add_valid_dir() {
        assert_eq!(pos!("e5").add_dir(Dir::UpLeft), Some(pos!("d6")));
        assert_eq!(pos!("e5").add_dir(Dir::Up), Some(pos!("e6")));
        assert_eq!(pos!("e5").add_dir(Dir::UpRight), Some(pos!("f6")));

        assert_eq!(pos!("e5").add_dir(Dir::Left), Some(pos!("d5")));
        // assert_eq!(pos!("e5").debug_add_dir(Dir::None), Some(pos!("e5")));
        assert_eq!(pos!("e5").add_dir(Dir::Right), Some(pos!("f5")));

        assert_eq!(pos!("e5").add_dir(Dir::DownLeft), Some(pos!("d4")));
        assert_eq!(pos!("e5").add_dir(Dir::Down), Some(pos!("e4")));
        assert_eq!(pos!("e5").add_dir(Dir::DownRight), Some(pos!("f4")));
    }

    #[test]
    fn add_invalid_dir() {
        assert_eq!(pos!("d8").add_dir(Dir::UpLeft), None);
        assert_eq!(pos!("a5").add_dir(Dir::UpLeft), None);
        assert_eq!(pos!("a8").add_dir(Dir::UpLeft), None);

        assert_eq!(pos!("d8").add_dir(Dir::Up), None);

        assert_eq!(pos!("d8").add_dir(Dir::UpRight), None);
        assert_eq!(pos!("h5").add_dir(Dir::UpRight), None);
        assert_eq!(pos!("h8").add_dir(Dir::UpRight), None);

        assert_eq!(pos!("a5").add_dir(Dir::Left), None);

        assert_eq!(pos!("h5").add_dir(Dir::Right), None);

        assert_eq!(pos!("d1").add_dir(Dir::DownLeft), None);
        assert_eq!(pos!("a5").add_dir(Dir::DownLeft), None);
        assert_eq!(pos!("a1").add_dir(Dir::DownLeft), None);

        assert_eq!(pos!("d1").add_dir(Dir::Down), None);

        assert_eq!(pos!("d1").add_dir(Dir::DownRight), None);
        assert_eq!(pos!("h5").add_dir(Dir::DownRight), None);
        assert_eq!(pos!("h1").add_dir(Dir::DownRight), None);
    }

    #[test]
    fn valid_pos() {
        for column in 'a'..='h' {
            for row in 1..=8 {
                let pos_string = format!("{column}{row}");
                Pos::from_str(&pos_string).expect("Should be valid");
            }
        }
    }

    #[test]
    fn specific_valid_pos() {
        let strings = ["a1", "a8", "h1", "h8"];
        for pos_string in strings {
            Pos::from_str(&pos_string).expect("Should be valid");
        }
    }

    #[test]
    fn invalid_pos() {
        let strings = [
            "a0", "a9", "h0", "h9", // Number out of bounds.
            "z1", "z8", "i1", "i8", // Letter out of bounds.
            "z0", "z9", "i0", "i9", // Both number and letter out of bounds.
            "", "c", "3", // Too few characters.
            "cc3", "c33", "cc33", "ccc333", // Too many characters.
            "11", "15", "51", "55", // Not a letter.
            "aa", "ae", "ea", "ee", // Not a number.
        ];
        for pos_string in strings {
            Pos::from_str(&pos_string).map(should_be_none);
        }
    }

    #[test]
    fn pos_from_str() {
        for column in 'a'..='h' {
            for row in 1..=8 {
                let pos_string = format!("{column}{row}");
                Pos::from_str(&pos_string).expect("Should be valid");
            }
        }
    }

    #[test]
    fn pos_from_ascii_bytes() {
        for column_byte in b'a'..=b'h' {
            for row_byte in b'1'..=b'8' {
                Pos::from_str_bytes(column_byte, row_byte).expect("Should be valid");
            }
        }
    }

    #[test]
    fn pos_correct_display_format() {
        for column in 'a'..='h' {
            for row in 1..=8 {
                let pos_string = format!("{column}{row}");
                let pos = Pos::from_str(&pos_string).unwrap();
                assert_eq!(format!("{}", pos), pos_string);
            }
        }
    }

    #[test]
    fn pos_correct_row_index() {
        for column in 'a'..='h' {
            for (row_index, row) in (1..=8).rev().enumerate() {
                let pos_string = format!("{column}{row}");
                let pos = Pos::from_str(&pos_string).unwrap();
                assert_eq!(pos.row_index(), row_index as u8);
            }
        }
    }

    #[test]
    fn pos_correct_column_index() {
        for (column_index, column) in ('a'..='h').enumerate() {
            for row in 1..=8 {
                let pos_string = format!("{column}{row}");
                let pos = Pos::from_str(&pos_string).unwrap();
                assert_eq!(pos.column_index(), column_index as u8);
            }
        }
    }

    #[test]
    fn pos_correct_flat_index() {
        let mut flat_indices = Vec::new();
        for row in (1..=8).rev() {
            for column in 'a'..='h' {
                let pos_string = format!("{column}{row}");
                flat_indices.push(Pos::from_str(&pos_string).unwrap().to_flat_index());
            }
        }

        let expected_flat_indices: Vec<u8> = (0..64).collect();
        assert_eq!(flat_indices, expected_flat_indices);
    }

    #[test]
    fn pos_from_flat_index() {
        for flat_index in 0..64 {
            let pos = Pos::from_flat_index(flat_index).unwrap();
            assert_eq!(pos.to_flat_index(), flat_index);
        }
    }

    #[test]
    fn pos_correct_flat_index_from_row_and_column_index() {
        let mut flat_indices = Vec::new();
        for row_index in 0..8 {
            for column_index in 0..8 {
                flat_indices.push(
                    Pos::from_coord_indices(column_index, row_index)
                        .unwrap()
                        .to_flat_index(),
                );
            }
        }

        let expected_flat_indices: Vec<u8> = (0..64).collect();
        assert_eq!(flat_indices, expected_flat_indices);
    }

    #[test]
    fn correct_size_pos() {
        assert_eq!(std::mem::size_of::<Pos>(), 1);
        assert_eq!(std::mem::size_of::<Option<Pos>>(), 1);
    }
}
