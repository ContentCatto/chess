use std::{
    convert::Infallible,
    error::Error,
    fmt::{Debug, Display},
};

use uci_parser::UciMove;

use crate::{board::InvalidPromotionPieceKind, simple_type_name, Pos, Promotion};

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Movement {
    pub from: Pos,
    pub to: Pos,
}

impl Movement {
    pub const fn from_tuple((from, to): (Pos, Pos)) -> Movement {
        Movement { from, to }
    }
}

impl Display for Movement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} -> {}", self.from, self.to)
    }
}

impl Debug for Movement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct(simple_type_name::<Self>())
            .field_with("from", |f| <Pos as Display>::fmt(&self.from, f))
            .field_with("to", |f| <Pos as Display>::fmt(&self.to, f))
            .finish()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Move {
    movement: Movement,
    promotion: Option<Promotion>,
}

impl Move {
    #[inline]
    pub const fn new(movement: Movement) -> Self {
        Self::new_maybe_promote(movement, None)
    }

    #[inline]
    pub const fn new_promote(movement: Movement, promotion: Promotion) -> Self {
        Self::new_maybe_promote(movement, Some(promotion))
    }

    #[inline]
    pub const fn new_maybe_promote(movement: Movement, promotion: Option<Promotion>) -> Self {
        Self {
            movement,
            promotion,
        }
    }

    #[inline]
    pub const fn movement(self) -> Movement {
        self.movement
    }

    #[inline]
    pub const fn promotion(self) -> Option<Promotion> {
        self.promotion
    }
}

impl Display for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.promotion {
            Some(promotion) => write!(
                f,
                "{} -> {} (promote to {promotion:#?})",
                self.movement.from, self.movement.to
            ),
            None => write!(f, "{} -> {}", self.movement.from, self.movement.to),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum InvalidUciMove {
    InvalidPromotionPieceKind(InvalidPromotionPieceKind),
}

impl Error for InvalidUciMove {}

impl Display for InvalidUciMove {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            InvalidUciMove::InvalidPromotionPieceKind(invalid_promotion_piece_kind) => {
                write!(f, "Invalid move: {invalid_promotion_piece_kind}")
            }
        }
    }
}

impl From<InvalidPromotionPieceKind> for InvalidUciMove {
    fn from(value: InvalidPromotionPieceKind) -> Self {
        Self::InvalidPromotionPieceKind(value)
    }
}

impl TryFrom<UciMove> for Move {
    type Error = InvalidUciMove;

    fn try_from(value: UciMove) -> Result<Self, Self::Error> {
        let from = value.src.into();
        let to = value.dst.into();
        let promotion = value.promote.map(TryInto::try_into).transpose()?;
        Ok(Move::new_maybe_promote(Movement { from, to }, promotion))
    }
}

impl TryFrom<Move> for UciMove {
    type Error = Infallible;

    fn try_from(value: Move) -> Result<Self, Self::Error> {
        Ok(UciMove {
            src: uci_parser::Square::from(value.movement().from),
            dst: uci_parser::Square::from(value.movement().to),
            promote: value.promotion().map(uci_parser::Piece::from),
        })
    }
}

#[derive(Debug, Clone)]
pub enum MaybePromoteMovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    WithoutPromotions(MovesFromPosIter<I>),
    WithPromotions(MovesFromPosIter<I>, PromoteMovesFromPosIter<I>),
}

impl<I> MaybePromoteMovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    pub fn new_without_promotions(from: Pos, to_positions: I) -> Self {
        Self::WithoutPromotions(MovesFromPosIter::new(from, to_positions))
    }

    pub fn new_with_promotions(
        from: Pos,
        non_promotion_positions: I,
        promotion_positions: I,
    ) -> Self {
        Self::WithPromotions(
            MovesFromPosIter::new(from, non_promotion_positions),
            PromoteMovesFromPosIter::new(from, promotion_positions),
        )
    }
}

impl<I> Iterator for MaybePromoteMovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    type Item = Move;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            MaybePromoteMovesFromPosIter::WithoutPromotions(it) => it.next(),
            MaybePromoteMovesFromPosIter::WithPromotions(without, with) => {
                with.next().or_else(|| without.next())
            }
        }
    }
}

impl<I> ExactSizeIterator for MaybePromoteMovesFromPosIter<I>
where
    I: ExactSizeIterator<Item = Pos>,
{
    fn len(&self) -> usize {
        match self {
            MaybePromoteMovesFromPosIter::WithoutPromotions(it) => it.len(),
            MaybePromoteMovesFromPosIter::WithPromotions(without, with) => {
                without.len() + with.len()
            }
        }
    }

    fn is_empty(&self) -> bool {
        match self {
            MaybePromoteMovesFromPosIter::WithoutPromotions(it) => it.is_empty(),
            MaybePromoteMovesFromPosIter::WithPromotions(without, with) => {
                without.is_empty() && with.is_empty()
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct MovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    from: Pos,
    to_positions: I,
}

impl<I> MovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    pub fn new(from: Pos, to_positions: I) -> Self {
        Self { from, to_positions }
    }
}

impl<I> ExactSizeIterator for MovesFromPosIter<I>
where
    I: ExactSizeIterator<Item = Pos>,
{
    fn len(&self) -> usize {
        self.to_positions.len()
    }

    fn is_empty(&self) -> bool {
        self.to_positions.is_empty()
    }
}

impl<I> Iterator for MovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    type Item = Move;

    fn next(&mut self) -> Option<Self::Item> {
        let to = self.to_positions.next()?;
        Some(Move::new(Movement {
            from: self.from,
            to,
        }))
    }
}

#[derive(Debug, Clone)]
pub struct PromoteMovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    move_iter: MovesFromPosIter<I>,
    next_move_without_promotion: Option<Move>,
    next_promotion: Promotion,
}

impl<I> PromoteMovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    pub fn new(from: Pos, to_positions: I) -> Self {
        let move_iter = MovesFromPosIter::new(from, to_positions);
        Self {
            move_iter,
            next_move_without_promotion: None,
            next_promotion: Promotion::Queen,
        }
    }
}

impl<I> Iterator for PromoteMovesFromPosIter<I>
where
    I: Iterator<Item = Pos>,
{
    type Item = Move;

    fn next(&mut self) -> Option<Self::Item> {
        if self.next_move_without_promotion.is_none() {
            self.next_move_without_promotion = self.move_iter.next();
        }
        let move_without_promotion = self.next_move_without_promotion?;

        let mov = Move::new_promote(move_without_promotion.movement(), self.next_promotion);

        self.next_promotion = match self.next_promotion {
            Promotion::Queen => Promotion::Rook,
            Promotion::Rook => Promotion::Knight,
            Promotion::Knight => Promotion::Bishop,
            Promotion::Bishop => Promotion::Queen,
        };
        if self.next_promotion == Promotion::Queen {
            // We have made moves with all promotions and wrapped around.
            self.next_move_without_promotion = None;
        }

        Some(mov)
    }
}

impl<I> ExactSizeIterator for PromoteMovesFromPosIter<I>
where
    I: ExactSizeIterator<Item = Pos>,
{
    fn len(&self) -> usize {
        self.move_iter.len() * 4
            + if self.next_move_without_promotion.is_some() {
                match self.next_promotion {
                    Promotion::Queen => 4,
                    Promotion::Rook => 3,
                    Promotion::Knight => 2,
                    Promotion::Bishop => 1,
                }
            } else {
                0
            }
    }

    fn is_empty(&self) -> bool {
        self.move_iter.is_empty() && self.next_move_without_promotion.is_none()
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum MovePart {
    Movement(Movement),
    Captured(Pos),
    Promoted(Pos, Promotion),
}

impl Debug for MovePart {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MovePart::Movement(movement) => write!(f, "Movement({movement})"),
            MovePart::Captured(pos) => write!(f, "Captured({pos})"),
            MovePart::Promoted(pos, promotion) => write!(f, "Promoted({pos}, {promotion:#?})"),
        }
    }
}

#[cfg(test)]
mod light_tests {
    use crate::{pos, BitBoard};

    use super::*;

    #[test]
    fn moves_from_pos_iter() {
        let it = MovesFromPosIter::new(
            pos!("d2"),
            BitBoard::from_positions([pos!("c1"), pos!("c2"), pos!("d3"), pos!("e3")]).positions(),
        );
        let expected = vec![
            Move::new(Movement {
                from: pos!("d2"),
                to: pos!("d3"),
            }),
            Move::new(Movement {
                from: pos!("d2"),
                to: pos!("e3"),
            }),
            Move::new(Movement {
                from: pos!("d2"),
                to: pos!("c2"),
            }),
            Move::new(Movement {
                from: pos!("d2"),
                to: pos!("c1"),
            }),
        ];
        assert!(expected.is_sorted_by_key(|mov| mov.movement().to.to_flat_index()));

        let collected: Vec<Move> = it.collect();
        assert_eq!(collected, expected);
    }

    #[test]
    fn promote_moves_from_pos_iter() {
        let it = PromoteMovesFromPosIter::new(
            pos!("d2"),
            BitBoard::from_positions([pos!("c1"), pos!("c2"), pos!("d3"), pos!("e3")]).positions(),
        );

        fn for_each_promotion(movement: Movement) -> [Move; 4] {
            [
                Move::new_promote(movement, Promotion::Queen),
                Move::new_promote(movement, Promotion::Rook),
                Move::new_promote(movement, Promotion::Knight),
                Move::new_promote(movement, Promotion::Bishop),
            ]
        }

        let expected = vec![
            for_each_promotion(Movement {
                from: pos!("d2"),
                to: pos!("d3"),
            }),
            for_each_promotion(Movement {
                from: pos!("d2"),
                to: pos!("e3"),
            }),
            for_each_promotion(Movement {
                from: pos!("d2"),
                to: pos!("c2"),
            }),
            for_each_promotion(Movement {
                from: pos!("d2"),
                to: pos!("c1"),
            }),
        ]
        .into_flattened();
        assert!(expected.is_sorted_by_key(|mov| mov.movement().to.to_flat_index()));

        let collected: Vec<Move> = it.collect();
        assert_eq!(collected, expected);
    }

    #[test]
    fn promote_moves_from_pos_iter_length_is_correct() {
        let mut it = PromoteMovesFromPosIter::new(
            pos!("d2"),
            BitBoard::from_positions([pos!("c1"), pos!("c2"), pos!("d3"), pos!("e3")]).positions(),
        );

        assert_eq!(it.len(), 16);

        for expected_length in (0..16).rev() {
            it.next();
            assert_eq!(it.len(), expected_length);
            assert_eq!(it.is_empty(), expected_length == 0);
        }
    }

    #[test]
    fn correct_size_movement() {
        assert_eq!(std::mem::size_of::<Movement>(), 2);
        assert_eq!(std::mem::size_of::<Option<Movement>>(), 2);
    }

    #[test]
    fn correct_size_move() {
        assert_eq!(std::mem::size_of::<Move>(), 3);
        assert_eq!(std::mem::size_of::<Option<Move>>(), 3);
    }
}
