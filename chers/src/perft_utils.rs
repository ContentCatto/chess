use std::time::Instant;

use crate::Board;

#[inline(always)]
pub fn perft_main_impl(fen: &str, plies: u16, expected_num_nodes: usize) {
    let board = Board::from_fen(fen).expect("FEN should be valid");

    let start_time = Instant::now();

    let num_nodes = board.count_legal_moves_recursive(plies);
    assert_eq!(num_nodes, expected_num_nodes);

    let time_taken = start_time.elapsed();
    println!(
        "Took {:.3} ms to find {num_nodes} nodes",
        time_taken.as_micros() as f64 / 1000.0
    );
}

#[macro_export]
macro_rules! perft_main {
    (fen = $fen:literal, plies = $plies:literal, expected_num_nodes = $expected_num_nodes:literal $(,)?) => {
        fn main() {
            $crate::perft_utils::perft_main_impl($fen, $plies, $expected_num_nodes)
        }
    };
}
