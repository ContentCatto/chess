use std::{
    fmt::{Debug, Display},
    ops::{Index, IndexMut},
};

use const_arrayvec::ArrayVec;
use serde::{Deserialize, Serialize};

use crate::{string_representation_by, AssertEq, BitBoard, PieceColor};

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
pub struct ByColor<T> {
    pub white: T,
    pub black: T,
}

impl<T> ByColor<T> {
    #[inline]
    pub const fn index(&self, color: PieceColor) -> &T {
        match color {
            PieceColor::White => &self.white,
            PieceColor::Black => &self.black,
        }
    }

    #[inline]
    pub const fn index_mut(&mut self, color: PieceColor) -> &mut T {
        match color {
            PieceColor::White => &mut self.white,
            PieceColor::Black => &mut self.black,
        }
    }

    pub fn values(&self) -> impl Iterator<Item = &T> {
        [&self.white, &self.black].into_iter()
    }
}

impl<T> Index<PieceColor> for ByColor<T> {
    type Output = T;

    fn index(&self, color: PieceColor) -> &Self::Output {
        self.index(color)
    }
}

impl<T> IndexMut<PieceColor> for ByColor<T> {
    fn index_mut(&mut self, color: PieceColor) -> &mut Self::Output {
        self.index_mut(color)
    }
}

impl<T, const CAP: usize> ByColor<ArrayVec<T, CAP>> {
    #[inline]
    pub const fn new_const() -> Self {
        Self {
            white: ArrayVec::new(),
            black: ArrayVec::new(),
        }
    }

    #[inline]
    pub fn clear(&mut self) {
        self.white.clear();
        self.black.clear();
    }
}

impl ByColor<BitBoard> {
    #[inline]
    pub const fn none() -> Self {
        Self {
            white: BitBoard::none(),
            black: BitBoard::none(),
        }
    }

    #[inline]
    pub const fn all() -> Self {
        Self {
            white: BitBoard::all(),
            black: BitBoard::all(),
        }
    }

    #[inline]
    pub const fn inverse(self) -> Self {
        Self {
            white: self.white.inverse(),
            black: self.black.inverse(),
        }
    }

    #[inline]
    pub const fn numeric(self) -> ByColor<u64> {
        ByColor {
            white: self.white.0,
            black: self.black.0,
        }
    }

    #[inline]
    pub const fn either(self) -> BitBoard {
        self.white.bitor(self.black)
    }

    #[inline]
    pub const fn both(self) -> BitBoard {
        self.white.bitand(self.black)
    }

    #[inline]
    pub const fn self_xor(self) -> BitBoard {
        self.white.bitxor(self.black)
    }
}

impl<T: Debug> Debug for ByColor<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "white:\n{:?}\nblack:\n{:?}", self.white, self.black)
    }
}

impl<T: Display> Display for ByColor<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<white: {}, black: {}>", self.white, self.black)
    }
}

impl<T: AssertEq> AssertEq for ByColor<T> {
    fn string_representation(&self) -> String {
        string_representation_by(
            [PieceColor::White, PieceColor::Black]
                .map(|color| (format!("{color:#?}"), &self[color])),
        )
    }
}
