use std::{
    collections::HashMap,
    error::Error,
    fmt::{Debug, Display},
    marker::PhantomData,
    ops::Index,
    sync::Arc,
};

use crate::BitBoard;

/// Fast array-based mapping from masked bitboard to arbitrary value.
///
/// Like a read-only [`HashMap<BitBoard, V>`] but where hashing is just multiplying by a magic
/// number, and each element is ensured to hash to a single index in the internal buffer.
#[derive(Clone)]
pub struct GenericBitboardLookup<V, S = Arc<[V]>>
where
    S: AsRef<[V]>,
{
    /// Contains the data in a contiguous bit of memory.
    array: S,
    /// Multiplied with the index, then some bits are discarded, and then that's the index into the array.
    magic: u64,
    /// Optimally, this is as close to 64 as possible.
    bits_to_discard: u8,
    /// Which bits to keep when indexing. The other bits are ignored.
    mask: BitBoard,
    /// Shut up, compiler!
    _pd: PhantomData<V>,
}

pub type MagicBitboard<V> = GenericBitboardLookup<V, Arc<[V]>>;

pub type StackMagicBitboard<V, const N: usize> = GenericBitboardLookup<V, [V; N]>;

impl<V: Debug, S: AsRef<[V]>> Debug for GenericBitboardLookup<V, S> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("GenericBitboardLookup")
            .field("magic", &self.magic)
            .field("bits_to_discard", &self.bits_to_discard)
            .field("mask", &self.mask)
            .finish_non_exhaustive()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CreateMagicBitBoardError<V> {
    IndicesNotUnique {
        magic: u64,
        value_a: V,
        value_b: V,
        full_index: u64,
    },
    NeverUniqueWithMagic {
        magic: u64,
    },
    StorageAllocationError,
    TooLargeForExactFinalSize {
        max_final_size: usize,
        actual_size: usize,
        magic: u64,
    },
}

impl<V: Debug> Display for CreateMagicBitBoardError<V> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CreateMagicBitBoardError::IndicesNotUnique {
                magic,
                value_a,
                value_b,
                full_index,
            } => {
                write!(
                    f,
                    "Magic {magic} did not produce unique indexes, {value_a:?} and {value_b:?} \
                    both have the index {full_index}.",
                )
            }
            CreateMagicBitBoardError::NeverUniqueWithMagic { magic } => write!(
                f,
                "No number of discarded bits gives unique indices with magic {magic}"
            ),
            CreateMagicBitBoardError::StorageAllocationError => {
                write!(f, "Could not allocate storage for data")
            }
            CreateMagicBitBoardError::TooLargeForExactFinalSize { max_final_size, actual_size, magic } => write!(
                f,
                "The generated array of length {actual_size} is larger than the given max size {max_final_size} with magic {magic}"
            ),
        }
    }
}

impl<V: Debug> Error for CreateMagicBitBoardError<V> {}

struct MagicBitBoardCreateData<V> {
    key_values_with_full_index: HashMap<u64, V>,
    array_len: usize,
    bits_to_discard: u8,
}

impl<V, S: AsRef<[V]>> GenericBitboardLookup<V, S> {
    pub fn empty() -> Self
    where
        S: TryFrom<Vec<V>>,
    {
        Self {
            array: S::try_from(Vec::new())
                .unwrap_or_else(|_| panic!("Failed to create empty storage")),
            magic: 0,
            bits_to_discard: 0,
            mask: BitBoard::none(),
            _pd: PhantomData,
        }
    }

    #[inline(always)]
    fn try_create_from_magic(
        magic: u64,
        mask: BitBoard,
        mut value_generator: impl FnMut(BitBoard) -> V,
    ) -> Result<MagicBitBoardCreateData<V>, CreateMagicBitBoardError<V>>
    where
        V: Eq + Debug,
    {
        let mut key_values_with_full_index =
            HashMap::with_capacity(mask.num_bit_variations() as usize);

        for key in mask.all_bit_variations() {
            let value = value_generator(key);
            let full_index = Self::magic_math_without_discard(magic, mask, key);
            assert_eq!(
                full_index,
                magic.wrapping_mul(key.0),
                "Masking should be unnecessary during creation"
            );

            let previous_entry = key_values_with_full_index.insert(full_index, value);

            if let Some(colliding_value_a) = previous_entry {
                if colliding_value_a == key_values_with_full_index[&full_index] {
                    // Index collision is fine (and an optimization) if it's the same value;
                    continue;
                }
                let colliding_value_b = key_values_with_full_index
                    .remove(&full_index)
                    .expect("We just inserted the element, so it must be there");

                return Err(CreateMagicBitBoardError::IndicesNotUnique {
                    magic,
                    value_a: colliding_value_a,
                    value_b: colliding_value_b,
                    full_index,
                });
            }
        }

        let mut already_used_key_values = HashMap::with_capacity(key_values_with_full_index.len());
        'new_discard: for bits_to_discard in (0..=64).rev() {
            already_used_key_values.clear();

            for (&full_index, value) in key_values_with_full_index.iter() {
                let index = Self::discard_math(full_index, bits_to_discard);
                let prev_value = already_used_key_values.insert(index, value);
                if let Some(prev_value) = prev_value {
                    if prev_value == already_used_key_values[&index] {
                        // Index collision is fine (and an optimization) if it's the same value;
                        continue;
                    }

                    // Collisions with different values don't work.
                    continue 'new_discard;
                }
            }

            let Some(highest_array_index) = already_used_key_values.into_keys().max() else {
                // Zero-length array.
                return Ok(MagicBitBoardCreateData {
                    key_values_with_full_index,
                    array_len: 0,
                    bits_to_discard: 0,
                });
            };

            // SAFETY: We have checked that the indices are all unique even when discarding the
            //         given number of bits and we have calculated the correct length for the array.
            return Ok(MagicBitBoardCreateData {
                key_values_with_full_index,
                array_len: highest_array_index as usize + 1,
                bits_to_discard,
            });
        }

        Err(CreateMagicBitBoardError::NeverUniqueWithMagic { magic })
    }

    pub fn array_len_for_magic(
        magic: u64,
        mask: BitBoard,
        value_generator: impl FnMut(BitBoard) -> V,
    ) -> Result<usize, CreateMagicBitBoardError<V>>
    where
        V: Eq + Debug,
    {
        Self::try_create_from_magic(magic, mask, value_generator).map(|data| data.array_len)
    }

    pub fn new(
        magic: u64,
        mask: BitBoard,
        mut value_generator: impl FnMut(BitBoard) -> V,
        empty_value_generator: impl FnMut() -> V,
        exact_final_size: Option<usize>,
    ) -> Result<Self, CreateMagicBitBoardError<V>>
    where
        V: Eq + Debug,
        S: TryFrom<Vec<V>, Error: Debug>,
    {
        let MagicBitBoardCreateData {
            key_values_with_full_index,
            array_len,
            bits_to_discard,
        } = Self::try_create_from_magic(magic, mask, &mut value_generator)?;

        let ths = unsafe {
            Self::construct(
                magic,
                mask,
                bits_to_discard,
                array_len,
                key_values_with_full_index,
                empty_value_generator,
                exact_final_size,
            )?
        };

        for key in mask.all_bit_variations() {
            let expected_value = value_generator(key);
            let stored_value = &ths[key];
            assert_eq!(stored_value, &expected_value);
        }

        Ok(ths)
    }

    unsafe fn construct(
        magic: u64,
        mask: BitBoard,
        bits_to_discard: u8,
        array_len: usize,
        key_values: impl IntoIterator<Item = (u64, V)>,
        mut empty_value_generator: impl FnMut() -> V,
        exact_final_size: Option<usize>,
    ) -> Result<Self, CreateMagicBitBoardError<V>>
    where
        V: Debug,
        S: TryFrom<Vec<V>, Error: Debug>,
    {
        assert!(bits_to_discard <= 64);

        let mut buf = Vec::with_capacity(array_len);
        for _ in 0..array_len {
            buf.push(empty_value_generator());
        }

        if let Some(target_size) = exact_final_size {
            if array_len > target_size {
                return Err(CreateMagicBitBoardError::TooLargeForExactFinalSize {
                    max_final_size: target_size,
                    actual_size: array_len,
                    magic,
                });
            }
            for _ in buf.len()..target_size {
                buf.push(empty_value_generator());
            }

            assert_eq!(buf.len(), target_size);
        }

        for (full_index, value) in key_values {
            let index = Self::discard_math(full_index, bits_to_discard);
            buf[index as usize] = value;
        }

        let array = S::try_from(buf)
            // .inspect_err(|error| eprintln!("{error:?}"))
            .map_err(|_| CreateMagicBitBoardError::StorageAllocationError)?;

        Ok(Self {
            array,
            magic,
            bits_to_discard,
            mask,
            _pd: PhantomData,
        })
    }

    #[inline(always)]
    fn magic_math_without_discard(magic: u64, mask: BitBoard, index: BitBoard) -> u64 {
        let masked_index = index & mask;
        masked_index.0.wrapping_mul(magic)
    }

    #[inline(always)]
    fn discard_math(full_index: u64, bits_to_discard: u8) -> u64 {
        debug_assert!(bits_to_discard <= 64);
        if bits_to_discard == 64 {
            return 0;
        }
        // SAFETY: As long as the number of bits is lower than 128, this is safe. We check in
        //         `Self::construct()` that the number of bits is less than or equal to 64, and we
        //         have just checked that it isn't 64, so it must be strictly less than 64.
        unsafe { std::hint::assert_unchecked(bits_to_discard < 64) };
        unsafe {
            (full_index as u128)
                .checked_shr(bits_to_discard as u32)
                .unwrap_unchecked() as u64
        }
    }

    #[inline(always)]
    fn magic_math(magic: u64, mask: BitBoard, bits_to_discard: u8, index: BitBoard) -> u64 {
        let full_index = Self::magic_math_without_discard(magic, mask, index);
        Self::discard_math(full_index, bits_to_discard)
    }

    #[inline(always)]
    pub fn array_len(&self) -> usize {
        self.array.as_ref().len()
    }

    #[inline(always)]
    pub fn bits_to_discard(&self) -> u8 {
        self.bits_to_discard
    }
}

impl<V, S: AsRef<[V]>> Index<BitBoard> for GenericBitboardLookup<V, S> {
    type Output = V;

    #[inline(always)]
    fn index(&self, index: BitBoard) -> &Self::Output {
        let array_index = Self::magic_math(self.magic, self.mask, self.bits_to_discard, index);
        // SAFETY: We have checked that all possible masked keys map to a valid value, which means
        //         all those keys also generate valid indices.
        unsafe { &self.array.as_ref().get_unchecked(array_index as usize) }
    }
}

#[cfg(test)]
mod light_tests {
    use crate::{
        pos,
        position::Dir,
        raw_board::calculate_threatened_squares_for_sliding_piece,
        threat_lookup::{
            generate_sliding_piece_magics_for_square, index_mask_for_sliding_piece,
            move_mask_for_sliding_piece,
        },
    };

    use super::*;

    fn rook_a1_threat_squares_test_impl<
        S: AsRef<[BitBoard]> + TryFrom<Vec<BitBoard>, Error: Debug>,
        const MAX_SIZE: usize,
    >(
        exact_final_size: Option<usize>,
    ) {
        tracing_subscriber::fmt()
            .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
            .pretty()
            .try_init()
            .unwrap_or(());

        let pos = pos!("a1");
        let sliding_directions = &Dir::ORTHOGONAL;

        let (magic, _array_len) = generate_sliding_piece_magics_for_square(pos, sliding_directions)
            .skip_while(|&(_magic, array_len)| array_len > MAX_SIZE)
            .next()
            .expect("The take_while finished, which means a value was produced");

        let index_mask = index_mask_for_sliding_piece(pos, sliding_directions);

        let magic_bitboard = GenericBitboardLookup::<BitBoard, S>::new(
            magic,
            index_mask,
            |occupied_squares| {
                calculate_threatened_squares_for_sliding_piece(
                    pos,
                    occupied_squares,
                    sliding_directions,
                )
            },
            || BitBoard::none(),
            exact_final_size,
        )
        .expect("Already checked to be valid");

        for key in move_mask_for_sliding_piece(pos, sliding_directions).all_bit_variations() {
            let value =
                calculate_threatened_squares_for_sliding_piece(pos, key, sliding_directions);
            assert_eq!(magic_bitboard[key], value);
        }
    }

    #[test]
    fn rook_a1_threat_squares() {
        rook_a1_threat_squares_test_impl::<Arc<[BitBoard]>, 8192>(None);
    }

    #[test]
    fn rook_a1_threat_squares_array() {
        rook_a1_threat_squares_test_impl::<[BitBoard; 8192], 8192>(Some(8192));
    }
}
