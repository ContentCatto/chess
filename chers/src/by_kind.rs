use std::{
    fmt::{Debug, Display},
    ops::{Index, IndexMut},
};

use crate::{string_representation_by, AssertEq, BitBoard, PieceKind};

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct ByKind<T> {
    pub pawn: T,
    pub bishop: T,
    pub knight: T,
    pub rook: T,
    pub queen: T,
    pub king: T,
}

impl<T> ByKind<T> {
    #[inline]
    pub const fn index(&self, kind: PieceKind) -> &T {
        match kind {
            PieceKind::Pawn => &self.pawn,
            PieceKind::Bishop => &self.bishop,
            PieceKind::Knight => &self.knight,
            PieceKind::Rook => &self.rook,
            PieceKind::Queen => &self.queen,
            PieceKind::King => &self.king,
        }
    }

    #[inline]
    pub const fn index_mut(&mut self, kind: PieceKind) -> &mut T {
        match kind {
            PieceKind::Pawn => &mut self.pawn,
            PieceKind::Bishop => &mut self.bishop,
            PieceKind::Knight => &mut self.knight,
            PieceKind::Rook => &mut self.rook,
            PieceKind::Queen => &mut self.queen,
            PieceKind::King => &mut self.king,
        }
    }
}

impl<T> Index<PieceKind> for ByKind<T> {
    type Output = T;

    fn index(&self, kind: PieceKind) -> &Self::Output {
        self.index(kind)
    }
}

impl<T> IndexMut<PieceKind> for ByKind<T> {
    fn index_mut(&mut self, kind: PieceKind) -> &mut Self::Output {
        self.index_mut(kind)
    }
}

impl ByKind<BitBoard> {
    #[inline]
    pub const fn none() -> Self {
        Self {
            pawn: BitBoard::none(),
            bishop: BitBoard::none(),
            knight: BitBoard::none(),
            rook: BitBoard::none(),
            queen: BitBoard::none(),
            king: BitBoard::none(),
        }
    }

    #[inline]
    pub const fn all() -> Self {
        Self {
            pawn: BitBoard::all(),
            bishop: BitBoard::all(),
            knight: BitBoard::all(),
            rook: BitBoard::all(),
            queen: BitBoard::all(),
            king: BitBoard::all(),
        }
    }

    #[inline]
    pub const fn inverse(self) -> Self {
        Self {
            pawn: self.pawn.inverse(),
            bishop: self.bishop.inverse(),
            knight: self.knight.inverse(),
            rook: self.rook.inverse(),
            queen: self.queen.inverse(),
            king: self.king.inverse(),
        }
    }

    #[inline]
    pub const fn numeric(self) -> ByKind<u64> {
        ByKind {
            pawn: self.pawn.0,
            bishop: self.bishop.0,
            knight: self.knight.0,
            rook: self.rook.0,
            queen: self.queen.0,
            king: self.king.0,
        }
    }

    #[inline]
    pub const fn union(self) -> BitBoard {
        self.pawn
            .bitor(self.bishop)
            .bitor(self.knight)
            .bitor(self.rook)
            .bitor(self.queen)
            .bitor(self.king)
    }

    #[inline]
    pub const fn intersection(self) -> BitBoard {
        self.pawn
            .bitand(self.bishop)
            .bitand(self.knight)
            .bitand(self.rook)
            .bitand(self.queen)
            .bitand(self.king)
    }
}

impl<T: Debug> Debug for ByKind<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "pawn:\n{:?}", self.pawn)?;
        writeln!(f, "bishop:\n{:?}", self.bishop)?;
        writeln!(f, "knight:\n{:?}", self.knight)?;
        writeln!(f, "rook:\n{:?}", self.rook)?;
        writeln!(f, "queen:\n{:?}", self.queen)?;
        write!(f, "king:\n{:?}", self.king)?;
        Ok(())
    }
}

impl<T: Display> Display for ByKind<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<pawn: {}, ", self.pawn)?;
        write!(f, "bishop: {}, ", self.bishop)?;
        write!(f, "knight: {}, ", self.knight)?;
        write!(f, "rook: {}, ", self.rook)?;
        write!(f, "queen: {}, ", self.queen)?;
        write!(f, "king: {}>", self.king)?;
        Ok(())
    }
}

impl<T: AssertEq> AssertEq for ByKind<T> {
    fn string_representation(&self) -> String {
        string_representation_by(
            PieceKind::ALL_VALUES.map(|kind| (format!("{kind:#?}"), &self[kind])),
        )
    }
}
