use std::hash::Hash;

use const_arrayvec::ArrayVec;

use crate::{by_pos::ByPos, BitBoard, ColPiece, Pos};

/// Similar to a [`HashSet<Pos>`] but allocated on the stack and with efficient unordered iteration.
///
/// Can also be sorted in-place to iterate in order.
///
/// FIXME: Default Eq and Hash implementations might be bad.
#[derive(Clone)]
pub struct PosBuffer {
    positions: ArrayVec<Pos, 64>,
    inverse: ByPos<u8>,
    bitboard: BitBoard,
}

impl PosBuffer {
    const NO_SQUARE_ARRAY_INDEX: u8 = u8::MAX;

    /// Creates a new empty PosBuffer.
    pub const fn new() -> Self {
        Self {
            positions: ArrayVec::new(),
            inverse: ByPos::new([Self::NO_SQUARE_ARRAY_INDEX; 64]),
            bitboard: BitBoard::none(),
        }
    }

    /// Adds position `added_pos` to the end of this `PosBuffer`.
    ///
    /// Panics if `added_pos` is already contained.
    pub const fn insert(&mut self, added_pos: Pos) {
        if *self.inverse.index(added_pos) != Self::NO_SQUARE_ARRAY_INDEX {
            panic!("The position to be added is already in this PosBuffer");
        }

        self.positions.push(added_pos);
        *self.inverse.index_mut(added_pos) = self.positions.len() as u8 - 1;
        self.bitboard.set_pos(added_pos, true);
    }

    /// Removes position `removed_pos` from this `PosBuffer`.
    ///
    /// Panics if `removed_pos` is not contained in this `PosBuffer`.
    pub fn remove(&mut self, removed_pos: Pos) {
        let removed_pos_array_index = self.inverse[removed_pos];
        if removed_pos_array_index == Self::NO_SQUARE_ARRAY_INDEX {
            panic!("The position to be removed is not in this PosBuffer");
        }

        let swap_element = self.positions.pop().expect(
            "Internal state is corrupt. \
            PosBuffer contains inverse mapping for pos, but self.positions is empty",
        );
        #[cfg(debug_assertions)]
        let swap_element_old_array_index = self.positions.len() as u8;

        if self.positions.len() != 0 && removed_pos_array_index != self.positions.len() as u8 {
            #[cfg(debug_assertions)]
            if removed_pos_array_index == swap_element_old_array_index {
                panic!("Internal state of this PosBuffer is probably corrupt. Attempted to swap pos with itself.");
            }
            // The piece that was removed was not the last piece, so overwrite the previously
            // used index with the position at the end of the array, making this a swap removal.
            self.positions[removed_pos_array_index as usize] = swap_element;
            self.inverse[swap_element] = removed_pos_array_index;
        }

        self.inverse[removed_pos] = Self::NO_SQUARE_ARRAY_INDEX;
        self.bitboard.set_pos(removed_pos, false);
    }

    pub fn clear(&mut self) {
        self.positions.clear();
        self.inverse = ByPos::new([Self::NO_SQUARE_ARRAY_INDEX; 64]);
        self.bitboard = BitBoard::none();
    }

    #[inline]
    pub const fn contains(&self, pos: Pos) -> bool {
        // TODO: Check if `*self.inverse.index(pos) != Self::NO_SQUARE_ARRAY_INDEX` is faster.
        self.bitboard.get_pos(pos)
    }

    #[inline]
    pub const fn len(&self) -> usize {
        self.positions.len()
    }

    #[inline]
    pub fn iter(&self) -> impl ExactSizeIterator<Item = Pos> + '_ {
        self.positions.iter().copied()
    }

    /// SAFETY: Caller must ensure that this `PosBuffer` and the provided `pieces` agree on which
    ///         squares have a piece.
    #[inline]
    pub unsafe fn iter_with_pieces_unchecked<'a>(
        &'a self,
        pieces: &'a [Option<ColPiece>; 64],
    ) -> impl ExactSizeIterator<Item = (Pos, ColPiece)> + 'a {
        self.iter().map(|pos| {
            // SAFETY: Array is statically known to have a length of 64, and
            //         Pos::to_flat_index() returns a number in the range 0..64, so indexing
            //         is always safe.
            let maybe_piece =
                unsafe { pieces.get(pos.to_flat_index() as usize).unwrap_unchecked() };

            // SAFETY: Caller must ensure that self and `pieces` agree on which squares have pieces.
            let piece = unsafe { maybe_piece.unwrap_unchecked() };

            (pos, piece)
        })
    }

    #[inline]
    pub fn iter_with_pieces<'a>(
        &'a self,
        pieces: &'a [Option<ColPiece>; 64],
    ) -> impl ExactSizeIterator<Item = (Pos, ColPiece)> + 'a {
        self.iter().map(|pos| {
            // SAFETY: Array is statically known to have a length of 64, and
            //         Pos::to_flat_index() returns a number in the range 0..64, so indexing
            //         is safe.
            let maybe_piece =
                unsafe { pieces.get(pos.to_flat_index() as usize).unwrap_unchecked() };

            let piece = maybe_piece.expect("Provided pieces array must be in sync with PosBuffer");
            (pos, piece)
        })
    }

    #[inline]
    pub const fn bitboard(&self) -> BitBoard {
        self.bitboard
    }
}

impl PartialEq for PosBuffer {
    fn eq(&self, other: &Self) -> bool {
        self.bitboard.eq(&other.bitboard)
    }
}

impl Eq for PosBuffer {}

impl Hash for PosBuffer {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.bitboard.hash(state)
    }
}
