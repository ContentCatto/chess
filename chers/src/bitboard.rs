use std::{
    fmt::{Debug, Write},
    hint::unreachable_unchecked,
    iter::Map,
    ops::{
        BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Not, Shl, ShlAssign, Shr,
        ShrAssign,
    },
};

use crate::{pos, AssertEq, Movement, Pos};

pub type PositionIter = Map<EnabledBitIter, fn(u8) -> Pos>;
pub type PositionAndValueIter = Map<BitIter, fn((u8, bool)) -> (Pos, bool)>;

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[repr(transparent)]
pub struct BitBoard(pub u64);

impl BitBoard {
    #[inline]
    pub const fn from_position(pos: Pos) -> Self {
        Self(1 << pos.to_flat_index())
    }

    #[inline(always)]
    pub const fn from_optional_position(pos: Option<Pos>) -> Self {
        if let Some(pos) = pos {
            Self::from_position(pos)
        } else {
            Self::none()
        }
    }

    #[inline]
    pub fn from_positions(positions: impl IntoIterator<Item = Pos>) -> Self {
        let mut this = Self(0);
        for pos in positions {
            this.set_pos(pos, true);
        }
        this
    }

    #[inline]
    pub fn positions(self) -> impl ExactSizeIterator<Item = Pos> {
        EnabledBitIter::new(self.0)
            .map(|flat_index| unsafe { Pos::from_flat_index_unchecked(flat_index) })
    }

    #[inline]
    pub fn all_positions(self) -> impl ExactSizeIterator<Item = (Pos, bool)> {
        BitIter::new(self.0).map(|(flat_index, value)| {
            (unsafe { Pos::from_flat_index_unchecked(flat_index) }, value)
        })
    }

    #[inline]
    pub fn moves_from(self, from: Pos) -> impl Iterator<Item = Movement> {
        self.positions().map(move |to| Movement { from, to })
    }

    #[inline]
    pub fn moves_to(self, to: Pos) -> impl Iterator<Item = Movement> {
        self.positions().map(move |from| Movement { from, to })
    }

    #[inline]
    pub const fn get_pos(&self, index: Pos) -> bool {
        self.get_from_u8_index(index.to_flat_index())
    }

    #[inline]
    pub const fn get_from_u8_index(&self, index: u8) -> bool {
        debug_assert!(index < 64);
        (self.0 >> index) & 1 != 0
    }

    #[inline(always)]
    pub const fn set_pos(&mut self, index: Pos, value: bool) {
        self.set_with_u8_index(index.to_flat_index(), value)
    }

    #[inline(always)]
    pub const fn set_with_u8_index(&mut self, index: u8, value: bool) {
        debug_assert!(index < 64);

        if value {
            self.0 |= 1 << index;
        } else {
            self.0 &= !(1 << index);
        }
    }

    #[inline]
    pub const fn get_mut(&mut self, index: Pos) -> MutBit {
        MutBit {
            value: &mut self.0,
            offset: index.to_flat_index(),
        }
    }

    #[inline]
    pub fn then(mut self, f: impl FnOnce(&mut Self)) -> Self {
        f(&mut self);
        self
    }

    #[inline]
    pub const fn above_file(file: u8) -> Self {
        match file {
            1 => BitBoard(!BitBoard::file(1).0),
            2 => BitBoard(BitBoard::above_file(1).0 & !BitBoard::file(2).0),
            3 => BitBoard(BitBoard::above_file(2).0 & !BitBoard::file(3).0),
            4 => BitBoard(BitBoard::above_file(3).0 & !BitBoard::file(4).0),
            5 => BitBoard(BitBoard::above_file(4).0 & !BitBoard::file(5).0),
            6 => BitBoard(BitBoard::above_file(5).0 & !BitBoard::file(6).0),
            7 => BitBoard(BitBoard::above_file(6).0 & !BitBoard::file(7).0),
            _ => BitBoard::none(),
        }
    }

    #[inline]
    pub const fn below_file(file: u8) -> Self {
        match file {
            2 => BitBoard(!BitBoard::above_file(1).0),
            3 => BitBoard(!BitBoard::above_file(2).0),
            4 => BitBoard(!BitBoard::above_file(3).0),
            5 => BitBoard(!BitBoard::above_file(4).0),
            6 => BitBoard(!BitBoard::above_file(5).0),
            7 => BitBoard(!BitBoard::above_file(6).0),
            8 => BitBoard(!BitBoard::above_file(7).0),
            _ => BitBoard::none(),
        }
    }

    #[inline]
    pub const fn shift_rank(self, num_ranks: i8) -> Self {
        if num_ranks.abs() >= 8 {
            return Self::none();
        }

        let shift = num_ranks.abs() as u32 * 8;
        if num_ranks >= 0 {
            // Shift upwards, from white player's perspective.
            Self((self.0 as u128).wrapping_shr(shift) as u64)
        } else {
            // Shift downwards, from white player's perspective.
            Self((self.0 as u128).wrapping_shl(shift) as u64)
        }
    }

    #[inline]
    pub const fn shift_file(self, num_files: i8) -> Self {
        if num_files.abs() >= 8 {
            return Self::none();
        }

        let clipped_value_mask = match num_files.signum() {
            1 => {
                // Shift to the right, from white player's perspective.
                BitBoard::above_file(num_files as u8)
            }
            -1 => {
                // Shift to the left, from white player's perspective.
                BitBoard::below_file((-num_files) as u8)
            }
            0 => {
                // No shift
                BitBoard::none()
            }
            // SAFETY: signum() can't return anything else.
            _ => unsafe { unreachable_unchecked() },
        };

        let masked_input = self.0 & !clipped_value_mask.0;

        let shift = num_files.abs() as u32;
        if num_files >= 0 {
            // Shift to the right, from white player's perspective.
            Self((masked_input as u128).wrapping_shl(shift) as u64)
        } else {
            // Shift to the left, from white player's perspective.
            Self((masked_input as u128).wrapping_shr(shift) as u64)
        }
    }

    #[inline]
    pub const fn rank(rank: u8) -> Self {
        if rank == 0 || rank > 8 {
            return Self::none();
        }

        const RANK_1: BitBoard = BitBoard(
            BitBoard::from_position(pos!("a1")).0
                | BitBoard::from_position(pos!("b1")).0
                | BitBoard::from_position(pos!("c1")).0
                | BitBoard::from_position(pos!("d1")).0
                | BitBoard::from_position(pos!("e1")).0
                | BitBoard::from_position(pos!("f1")).0
                | BitBoard::from_position(pos!("g1")).0
                | BitBoard::from_position(pos!("h1")).0,
        );
        // SAFETY: We have already checked that the rank is within bounds,
        //         so both the unchecked_sub() and the unwrap are safe.
        Self(unsafe {
            RANK_1
                .0
                .checked_shr(rank.unchecked_sub(1) as u32 * 8)
                .unwrap_unchecked()
        })
        // RANK_1.shift_rank(rank.wrapping_sub(1) as i8)
    }

    #[inline]
    pub const fn file(file: u8) -> Self {
        if file == 0 || file > 8 {
            return Self::none();
        }

        const FILE_1: BitBoard = BitBoard(
            BitBoard::from_position(pos!("a1")).0
                | BitBoard::from_position(pos!("a2")).0
                | BitBoard::from_position(pos!("a3")).0
                | BitBoard::from_position(pos!("a4")).0
                | BitBoard::from_position(pos!("a5")).0
                | BitBoard::from_position(pos!("a6")).0
                | BitBoard::from_position(pos!("a7")).0
                | BitBoard::from_position(pos!("a8")).0,
        );
        // SAFETY: We have already checked that the file is within bounds,
        //         so both the unchecked_sub() and the unwrap are safe.
        Self(unsafe {
            FILE_1
                .0
                .checked_shl(file.unchecked_sub(1) as u32)
                .unwrap_unchecked()
        })
        // FILE_1.shift_file(file.wrapping_sub(1) as i8)
    }

    #[inline]
    pub const fn none() -> BitBoard {
        Self(0)
    }

    #[inline]
    pub const fn all() -> BitBoard {
        Self(!0)
    }

    /// Extremely slow. Must be cached to be useful in an algorithm.
    pub fn all_bit_variations(self) -> impl Iterator<Item = BitBoard> + Clone {
        #[inline]
        fn next_bit_variation(
            current: &mut BitBoard,
            mask: BitBoard,
            bit_indices: &[u8],
        ) -> Option<BitBoard> {
            if *current == mask {
                return None;
            }

            for &bit_index in bit_indices {
                let was_zero = !current.get_from_u8_index(bit_index);
                *current ^= BitBoard(1 << bit_index);
                if was_zero {
                    // No more carry.
                    break;
                }
            }

            Some(*current)
        }

        let mut current = BitBoard::none();
        let bit_indices: Vec<u8> = self.positions().map(Pos::to_flat_index).collect();
        std::iter::once(BitBoard::none()).chain(
            std::iter::repeat(self)
                .map_while(move |mask| next_bit_variation(&mut current, mask, &bit_indices)),
        )
    }

    pub const fn num_bit_variations(self) -> u64 {
        2u64.pow(self.0.count_ones())
    }

    #[inline]
    pub const fn inverse(self) -> BitBoard {
        Self(!self.0)
    }

    #[inline]
    pub const fn bitor(self, rhs: BitBoard) -> BitBoard {
        Self(self.0 | rhs.0)
    }

    #[inline]
    pub const fn bitand(self, rhs: BitBoard) -> BitBoard {
        Self(self.0 & rhs.0)
    }

    #[inline]
    pub const fn bitxor(self, rhs: BitBoard) -> BitBoard {
        Self(self.0 ^ rhs.0)
    }

    /// Create a bitboard from a visual string representation showing set and unset squares.
    ///
    /// Mostly a conveniece function. Panics on invalid input.
    ///
    /// Treats "." and " " as an unset square.
    /// Treats "#" and uppercase characters as set squares.
    /// Panics on other values.
    pub fn from_visual(visual: [&str; 8]) -> BitBoard {
        visual
            .into_iter()
            .enumerate()
            .flat_map(|(row_index, row)| {
                assert_eq!(
                    row.chars().count(),
                    8,
                    "Invalid number of characters in row: {row:?}"
                );
                (0..8).zip(row.chars()).map(move |(column_index, ch)| match ch {
                    '.' | ' ' => BitBoard::none(),
                    _ if ch == '#' || ch.is_uppercase() => {
                        BitBoard::from_position(Pos::from_coord_indices(column_index, row_index as u8).expect("Both the column index and row index are checked to always be in the range 0..8"))
                    }
                    other => panic!("Unexpected visual bitboard character {other:?}"),
                })
            })
            .reduce(BitBoard::bitor)
            .expect("Iterator always iterates through 8*8 items, and so cannot have no items")
    }
}

impl BitAnd for BitBoard {
    type Output = BitBoard;

    #[inline]
    fn bitand(self, rhs: Self) -> Self::Output {
        Self(self.0 & rhs.0)
    }
}

impl BitAndAssign for BitBoard {
    #[inline]
    fn bitand_assign(&mut self, rhs: Self) {
        *self = *self & rhs;
    }
}

impl BitOr for BitBoard {
    type Output = BitBoard;

    #[inline]
    fn bitor(self, rhs: Self) -> Self::Output {
        Self(self.0 | rhs.0)
    }
}

impl BitOrAssign for BitBoard {
    #[inline]
    fn bitor_assign(&mut self, rhs: Self) {
        *self = *self | rhs;
    }
}

impl BitXor for BitBoard {
    type Output = BitBoard;

    #[inline]
    fn bitxor(self, rhs: Self) -> Self::Output {
        Self(self.0 ^ rhs.0)
    }
}

impl BitXorAssign for BitBoard {
    #[inline]
    fn bitxor_assign(&mut self, rhs: Self) {
        *self = *self ^ rhs;
    }
}

impl<T> Shl<T> for BitBoard
where
    u64: Shl<T, Output = u64>,
{
    type Output = BitBoard;

    #[inline]
    fn shl(self, rhs: T) -> BitBoard {
        Self(self.0 << rhs)
    }
}

impl<T> Shr<T> for BitBoard
where
    u64: Shr<T, Output = u64>,
{
    type Output = BitBoard;

    #[inline]
    fn shr(self, rhs: T) -> BitBoard {
        Self(self.0 >> rhs)
    }
}

impl<T> ShlAssign<T> for BitBoard
where
    BitBoard: Shl<T, Output = BitBoard>,
{
    #[inline]
    fn shl_assign(&mut self, rhs: T) {
        *self = *self << rhs;
    }
}

impl<T> ShrAssign<T> for BitBoard
where
    BitBoard: Shr<T, Output = BitBoard>,
{
    #[inline]
    fn shr_assign(&mut self, rhs: T) {
        *self = *self >> rhs;
    }
}

impl Not for BitBoard {
    type Output = BitBoard;

    #[inline]
    fn not(self) -> Self::Output {
        Self(!self.0)
    }
}

impl Debug for BitBoard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in (0..8).rev() {
            write!(f, "{} ", row + 1)?;

            for col in 0..8 {
                let pos = Pos::from_coords(col, row).expect("Coordinates should be valid");
                if self.get_pos(pos) {
                    f.write_char('#')?;
                } else {
                    f.write_char('.')?;
                }
            }
            f.write_char('\n')?;
        }

        f.write_str("  ")?;
        for col in 0..8 {
            let pos = Pos::from_coord_indices(col, 0).expect("Coordinates should be valid");
            f.write_char(pos.column_letter_char())?;
        }

        // No trailing newline

        Ok(())
    }
}

impl AssertEq for BitBoard {
    fn string_representation(&self) -> String {
        let mut buf = String::with_capacity(64 + 8);

        for (pos, is_set) in self.all_positions() {
            let ch = if is_set { '#' } else { '.' };
            buf.write_char(ch).unwrap();
            if (pos.to_flat_index() + 1) % 8 == 0 {
                buf.write_char('\n').unwrap();
            }
        }

        buf
    }
}

pub struct MutBit<'a> {
    value: &'a mut u64,
    offset: u8,
}

impl MutBit<'_> {
    pub const fn get(&self) -> bool {
        (*self.value >> self.offset) & 1 != 0
    }

    pub const fn set(&mut self, value: bool) {
        if value {
            *self.value |= 1 << self.offset;
        } else {
            *self.value &= !(1 << self.offset);
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct EnabledBitIter {
    remaining_value: u64,
    index: u8,
}

impl EnabledBitIter {
    pub const fn new(value: u64) -> Self {
        Self {
            remaining_value: value,
            index: 0,
        }
    }
}

impl Iterator for EnabledBitIter {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.remaining_value == 0 {
            return None;
        }
        let steps = self.remaining_value.trailing_zeros();
        self.index += steps as u8;

        self.remaining_value = (self.remaining_value >> steps) & !1;

        Some(self.index)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len(), Some(self.len()))
    }

    fn count(self) -> usize
    where
        Self: Sized,
    {
        self.size_hint().0
    }

    fn last(self) -> Option<Self::Item>
    where
        Self: Sized,
    {
        self.rev().next()
    }

    // TODO: Specialize
    // fn rev(self) -> std::iter::Rev<Self>
    // where
    //     Self: Sized + DoubleEndedIterator,
    // {
    //     std::iter::Rev::new(self)
    // }
}

impl DoubleEndedIterator for EnabledBitIter {
    fn next_back(&mut self) -> Option<Self::Item> {
        // TODO: Actually implement
        todo!()
    }
}

impl ExactSizeIterator for EnabledBitIter {
    fn len(&self) -> usize {
        self.remaining_value.count_ones() as usize
    }
}

#[derive(Debug, Clone, Copy)]
pub struct BitIter {
    remaining_value: u64,
    index: u8,
}

impl BitIter {
    pub const fn new(value: u64) -> Self {
        Self {
            remaining_value: value,
            index: 0,
        }
    }
}

impl Iterator for BitIter {
    type Item = (u8, bool);

    fn next(&mut self) -> Option<Self::Item> {
        while self.index != 64 {
            let is_set = self.remaining_value & 1 != 0;
            let index = self.index;

            self.remaining_value = (self.remaining_value & !1) >> 1;
            self.index += 1;

            return Some((index, is_set));
        }
        None
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len(), Some(self.len()))
    }
}

impl ExactSizeIterator for BitIter {
    fn len(&self) -> usize {
        64 - self.index as usize
    }

    fn is_empty(&self) -> bool {
        self.index == 64
    }
}

#[cfg(test)]
mod light_tests {
    use super::*;

    #[test]
    fn test_from_rank() {
        #![expect(clippy::unreadable_literal)]
        assert_eq!(
            BitBoard::rank(0),
            BitBoard(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000),
        );
        assert_eq!(
            BitBoard::rank(1),
            BitBoard(0b11111111_00000000_00000000_00000000_00000000_00000000_00000000_00000000),
        );
        assert_eq!(
            BitBoard::rank(2),
            BitBoard(0b00000000_11111111_00000000_00000000_00000000_00000000_00000000_00000000),
        );
        assert_eq!(
            BitBoard::rank(7),
            BitBoard(0b00000000_00000000_00000000_00000000_00000000_00000000_11111111_00000000),
        );
        assert_eq!(
            BitBoard::rank(8),
            BitBoard(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_11111111),
        );
        assert_eq!(
            BitBoard::rank(9),
            BitBoard(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000),
        );
    }

    #[test]
    fn test_from_file() {
        #![expect(clippy::unreadable_literal)]
        assert_eq!(
            BitBoard::file(0),
            BitBoard(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000),
        );
        assert_eq!(
            BitBoard::file(1),
            BitBoard(0b00000001_00000001_00000001_00000001_00000001_00000001_00000001_00000001),
        );
        assert_eq!(
            BitBoard::file(2),
            BitBoard(0b00000010_00000010_00000010_00000010_00000010_00000010_00000010_00000010),
        );
        assert_eq!(
            BitBoard::file(7),
            BitBoard(0b01000000_01000000_01000000_01000000_01000000_01000000_01000000_01000000),
        );
        assert_eq!(
            BitBoard::file(8),
            BitBoard(0b10000000_10000000_10000000_10000000_10000000_10000000_10000000_10000000),
        );
        assert_eq!(
            BitBoard::file(9),
            BitBoard(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000),
        );
    }
}
