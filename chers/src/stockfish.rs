use std::{
    collections::HashMap,
    io::{BufRead, BufReader, Write},
    process::{Child, ChildStdin, ChildStdout, Command, Stdio},
};

use crate::{Board, Move, Movement, Pos, Promotion};

pub struct Stockfish {
    _process: Child,
    stdin: ChildStdin,
    stdout: BufReader<ChildStdout>,
}

impl Stockfish {
    pub fn new() -> Self {
        let mut process = Command::new("stockfish")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .expect("Child should spawn");

        let stdin = process.stdin.take().unwrap();

        let stdout = process.stdout.take().unwrap();
        let mut stdout = BufReader::new(stdout);

        // Ignore first line.
        stdout.read_line(&mut String::with_capacity(256)).unwrap();

        let mut slf = Self {
            _process: process,
            stdin,
            stdout,
        };
        slf.reset_board();

        slf
    }

    pub fn set_board_to_fen(&mut self, fen: &str) {
        tracing::trace!("Setting board to fen:  {fen}");
        writeln!(self.stdin, "position fen {fen}").unwrap();
        self.stdin.flush().unwrap();
        // No output will be printed.
        tracing::trace!("Board was set to position.");
    }

    pub fn set_board(&mut self, board: &Board) {
        let fen = board.to_fen();
        self.set_board_to_fen(&fen)
    }

    pub fn reset_board(&mut self) {
        tracing::trace!("Resetting board...");
        writeln!(self.stdin, "ucinewgame").unwrap();
        self.stdin.flush().unwrap();
        // No output will be printed.
        tracing::trace!("Board was reset.");
    }

    pub fn go_perft(&mut self, plies: u16) -> HashMap<Move, usize> {
        writeln!(self.stdin, "go perft {plies}").unwrap();
        self.stdin.flush().unwrap();

        let mut buf = Vec::with_capacity(32 * 1024);
        loop {
            // Read a line.
            self.stdout.read_until(b'\n', &mut buf).unwrap();
            if buf.starts_with(b"info") {
                // Skip this line.
                buf.clear();
                continue;
            }
            // Keep this line and all following lines.
            break;
        }
        self.stdout.read_until(b'N', &mut buf).unwrap();
        buf.pop();

        let output = std::str::from_utf8(&buf).expect("Output should be valid UTF-8");
        tracing::trace!("Stockfish returned:\n{output}");
        let data = Stockfish::parse_pertfs(output.trim());

        self.stdout.read_until(b'\n', &mut buf).unwrap();
        self.stdout.read_until(b'\n', &mut buf).unwrap();

        data
    }

    pub fn run_perft_for_board_and_reset(
        &mut self,
        board: &Board,
        plies: u16,
    ) -> HashMap<Move, usize> {
        self.set_board(board);
        self.run_perft_and_reset(plies)
    }

    pub fn run_perft_for_fen_and_reset(&mut self, fen: &str, plies: u16) -> HashMap<Move, usize> {
        self.set_board_to_fen(fen);
        self.run_perft_and_reset(plies)
    }

    pub fn run_perft_and_reset(&mut self, plies: u16) -> HashMap<Move, usize> {
        let data = self.go_perft(plies);
        self.reset_board();
        data
    }

    fn parse_pertfs(output: &str) -> HashMap<Move, usize> {
        let mut map = HashMap::with_capacity(218);
        for line in output.lines() {
            tracing::trace!(line);
            if line.starts_with("info ") {
                continue;
            }
            let (move_str, count_str) = line.split_once(": ").expect("Missing the colon and space");
            tracing::trace!(move_str, count_str);
            let from_pos_str = &move_str[0..2];
            let to_pos_str = &move_str[2..4];
            let movement = Movement {
                from: Pos::from_str(from_pos_str).unwrap_or_else(|| {
                    panic!("Invalid from pos: {from_pos_str:?} in move string {move_str:?}")
                }),
                to: Pos::from_str(to_pos_str).unwrap_or_else(|| {
                    panic!("Invalid to pos: {to_pos_str:?} in move string {move_str:?}")
                }),
            };
            let promotion_str = &move_str[4..];
            let promotion = match promotion_str {
                "b" => Some(Promotion::Bishop),
                "n" => Some(Promotion::Knight),
                "r" => Some(Promotion::Rook),
                "q" => Some(Promotion::Queen),
                "" => None,
                _ => panic!("Invalid promotion {promotion_str:?}"),
            };
            let mov = Move::new_maybe_promote(movement, promotion);

            let count = count_str.trim().parse().expect("Invalid count");
            map.insert(mov, count);
        }
        map
    }
}
