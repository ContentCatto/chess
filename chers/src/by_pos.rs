use std::{
    fmt::{Debug, Display},
    marker::PhantomData,
    mem::MaybeUninit,
    ops::{Index, IndexMut},
};

use const_arrayvec::ArrayVec;
use serde::{de::Visitor, ser::SerializeMap, Deserialize, Serialize};

use crate::{magic_bitboard::MagicBitboard, string_representation_by, AssertEq, BitBoard, Pos};

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct ByPos<T> {
    pub data: [T; 64],
}

impl<T> ByPos<T> {
    #[inline]
    pub const fn new(data: [T; 64]) -> Self {
        Self { data }
    }

    pub fn from_generator(mut value_gen: impl FnMut(Pos) -> T) -> Self {
        let mut data: [MaybeUninit<T>; 64] = unsafe { MaybeUninit::uninit().assume_init() };

        for pos in Pos::positions() {
            data[pos.to_flat_index() as usize].write(value_gen(pos));
        }

        // Manual `std::mem::transmute()` to allow it to work for generic arguments.
        assert_eq!(
            std::mem::size_of::<[MaybeUninit<T>; 64]>(),
            std::mem::size_of::<[T; 64]>()
        );
        let ptr = &data as *const [MaybeUninit<T>; 64] as *const [T; 64];
        // SAFETY: I got this from https://users.rust-lang.org/t/transmuting-a-generic-array/45645/5
        //         and I trust the forums implicitly.
        let final_data = unsafe { ptr.read() };
        std::mem::forget(data);

        Self::new(final_data)
    }

    #[inline]
    pub fn from_single_value_generator(mut value_gen: impl FnMut() -> T) -> Self {
        Self::from_generator(|_pos| value_gen())
    }

    #[inline]
    pub fn from_value(value: T) -> Self
    where
        T: Clone,
    {
        Self::from_single_value_generator(|| value.clone())
    }

    #[inline]
    pub const fn index(&self, pos: Pos) -> &T {
        &self.data[pos.to_flat_index() as usize]
    }

    #[inline]
    pub const fn index_mut(&mut self, pos: Pos) -> &mut T {
        &mut self.data[pos.to_flat_index() as usize]
    }
}

impl<T: Default> Default for ByPos<T> {
    #[inline]
    fn default() -> Self {
        Self::from_single_value_generator(T::default)
    }
}

impl<V> ByPos<MagicBitboard<V>> {
    #[inline]
    pub fn new_empty_magic_bitboards() -> Self {
        Self::from_generator(|_pos| MagicBitboard::empty())
    }
}

impl<T> Index<Pos> for ByPos<T> {
    type Output = T;

    #[inline]
    fn index(&self, pos: Pos) -> &Self::Output {
        self.index(pos)
    }
}

impl<T> IndexMut<Pos> for ByPos<T> {
    #[inline]
    fn index_mut(&mut self, pos: Pos) -> &mut Self::Output {
        self.index_mut(pos)
    }
}

impl ByPos<BitBoard> {
    #[inline]
    pub const fn none() -> Self {
        Self {
            data: [BitBoard::none(); 64],
        }
    }

    #[inline]
    pub const fn all() -> Self {
        Self {
            data: [BitBoard::all(); 64],
        }
    }

    #[inline]
    pub const fn inverse(mut self) -> Self {
        let mut index = 0;
        while index < self.data.len() {
            self.data[index] = self.data[index].inverse();
            index += 1;
        }
        self
    }

    #[inline]
    pub const fn numeric(&self) -> ByPos<u64> {
        let mut numeric = ByPos::new([0; 64]);

        let mut index = 0;
        while index < self.data.len() {
            numeric.data[index] = self.data[index].0;
            index += 1;
        }

        numeric
    }
}

impl<T> IntoIterator for ByPos<T> {
    type Item = (Pos, T);

    type IntoIter = std::iter::Map<
        std::iter::Enumerate<std::array::IntoIter<T, 64>>,
        fn((usize, T)) -> (Pos, T),
    >;

    fn into_iter(self) -> Self::IntoIter {
        let f: fn((usize, T)) -> (Pos, T) = |(flat_index, value)| {
            (
                unsafe { Pos::from_flat_index_unchecked(flat_index as u8) },
                value,
            )
        };
        self.data.into_iter().enumerate().map(f)
    }
}

impl<'t, T> IntoIterator for &'t ByPos<T> {
    type Item = (Pos, &'t T);

    type IntoIter =
        std::iter::Map<std::iter::Enumerate<std::slice::Iter<'t, T>>, fn((usize, &T)) -> (Pos, &T)>;

    fn into_iter(self) -> Self::IntoIter {
        let f: fn((usize, &T)) -> (Pos, &T) = |(flat_index, value)| {
            (
                unsafe { Pos::from_flat_index_unchecked(flat_index as u8) },
                value,
            )
        };
        self.data.iter().enumerate().map(f)
    }
}

impl<T: Debug> Debug for ByPos<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for rank in (1..=8).rev() {
            for file in 1..=8 {
                if file != 1 {
                    write!(f, " ")?;
                }

                let pos = Pos::from_rank_and_file(rank, file)
                    .expect("1..=8 is valid for both rank and file");
                let value = self.index(pos);
                write!(f, "{value:?}")?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl<T: Display> Display for ByPos<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for rank in (1..=8).rev() {
            for file in 1..=8 {
                if file != 1 {
                    write!(f, " ")?;
                }

                let pos = Pos::from_rank_and_file(rank, file)
                    .expect("1..=8 is valid for both rank and file");
                let value = self.index(pos);
                write!(f, "{value}")?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl<T: Serialize> Serialize for ByPos<T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(self.data.len()))?;
        for pos in Pos::positions() {
            let value = &self[pos];
            map.serialize_key(&pos)?;
            map.serialize_value(value)?;
        }
        map.end()
    }
}

struct DeserHelper<T>(PhantomData<T>);

impl<'de, T: Deserialize<'de> + Clone + Debug> Visitor<'de> for DeserHelper<T> {
    type Value = ByPos<T>;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a mapping from all chessboard positions to values")
    }

    fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
    where
        A: serde::de::MapAccess<'de>,
    {
        use serde::de::Error;

        let mut buf: ArrayVec<(Pos, T), 64> = ArrayVec::new();
        while let Some(key) = map.next_key()? {
            let value = map.next_value()?;
            buf.push((key, value));
        }

        if buf.len() != 64 {
            return Err(A::Error::invalid_length(buf.len(), &"64"));
        }

        buf.sort_unstable_by_key(|(pos, _value)| pos.to_flat_index());
        buf.reverse();

        for [a, b] in buf
            .iter()
            .map(|(pos, _value)| *pos)
            .map_windows(|[a, b]| [*a, *b])
        {
            if a == b {
                return Err(A::Error::custom(format!("Duplicate key {a:?}")));
            }
        }

        let mut packed_buf: ArrayVec<T, 64> = ArrayVec::new();
        for _ in 0..64 {
            let (_pos, value) = buf
                .pop()
                .expect("The length has been checked to be correct");
            packed_buf.push(value);
        }

        let array: &[T; 64] = packed_buf[..]
            .try_into()
            .expect("The length has been checked to be correct");
        let array = array.clone();

        Ok(ByPos::new(array))
    }
}

impl<'de, T: Deserialize<'de> + Clone + Debug> Deserialize<'de> for ByPos<T> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_map(DeserHelper(PhantomData))
    }
}

impl<T: AssertEq> AssertEq for ByPos<T> {
    fn string_representation(&self) -> String {
        string_representation_by(Pos::positions().map(|pos| (format!("{pos}"), &self[pos])))
    }
}
