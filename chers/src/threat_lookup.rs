use std::{
    collections::HashMap,
    path::Path,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, LazyLock,
    },
    time::{Duration, Instant},
};

use rayon::iter::{IntoParallelIterator, ParallelIterator};
use serde::{Deserialize, Serialize};

use crate::{
    by_pos::ByPos,
    magic_bitboard::{CreateMagicBitBoardError, MagicBitboard},
    position::Dir,
    raw_board::{
        calculate_threatened_squares_for_king, calculate_threatened_squares_for_knight,
        calculate_threatened_squares_for_pawn, calculate_threatened_squares_for_sliding_piece,
        relevant_block_positions_for_sliding_piece,
    },
    BitBoard, ByColor, PieceColor, PieceKind, Pos,
};

// Not public, don't use directly.
static THREAT_LOOKUP: LazyLock<ThreatLookup> = LazyLock::new(|| {
    MagicsData::load(concat!(env!("CARGO_MANIFEST_DIR"), "/../data/magics.json"))
        .expect("Magics should load")
        .try_into()
        .expect("Magics should be valid")
});

pub fn get_threat_lookup() -> &'static ThreatLookup {
    &*THREAT_LOOKUP
}

#[derive(Debug, Clone)]
pub struct ThreatLookup {
    bishop: ByPos<MagicBitboard<BitBoard>>,
    rook: ByPos<MagicBitboard<BitBoard>>,
    knight: ByPos<BitBoard>,
    king: ByPos<BitBoard>,
    pawn_threats: ByColor<ByPos<BitBoard>>,
    pawn_double_moves: ByColor<ByPos<MagicBitboard<BitBoard>>>,
}

impl ThreatLookup {
    #[inline]
    pub fn lookup_bishop_threats(&self, pos: Pos, occupied_positions: BitBoard) -> BitBoard {
        self.bishop[pos][occupied_positions]
    }

    #[inline]
    pub fn lookup_rook_threats(&self, pos: Pos, occupied_positions: BitBoard) -> BitBoard {
        self.rook[pos][occupied_positions]
    }

    #[inline(always)]
    pub fn lookup_queen_threats(&self, pos: Pos, occupied_positions: BitBoard) -> BitBoard {
        self.lookup_bishop_threats(pos, occupied_positions)
            | self.lookup_rook_threats(pos, occupied_positions)
    }

    #[inline]
    pub fn lookup_knight_threats(&self, pos: Pos) -> BitBoard {
        self.knight[pos]
    }

    #[inline]
    pub fn lookup_king_threats(&self, pos: Pos) -> BitBoard {
        self.king[pos]
    }

    #[inline]
    pub fn lookup_pawn_threats(&self, pos: Pos, color: PieceColor) -> BitBoard {
        self.pawn_threats[color][pos]
    }

    #[inline]
    pub fn lookup_pawn_double_moves(
        &self,
        pos: Pos,
        color: PieceColor,
        occupied_positions: BitBoard,
    ) -> BitBoard {
        self.pawn_double_moves[color][pos][occupied_positions]
    }
}

impl TryFrom<MagicsData> for ThreatLookup {
    type Error = CreateMagicBitBoardError<BitBoard>;

    fn try_from(value: MagicsData) -> Result<Self, Self::Error> {
        fn create_from_magics(
            magics: ByPos<(u64, u8)>,
            mut index_mask_generator: impl FnMut(Pos) -> BitBoard,
            mut value_generator: impl FnMut(Pos, BitBoard) -> BitBoard,
        ) -> Result<ByPos<MagicBitboard<BitBoard>>, CreateMagicBitBoardError<BitBoard>> {
            let magic_bitboards = magics.into_iter().map(|(pos, (magic, _shift))| {
                let mask = index_mask_generator(pos);
                let magic_bitboard = MagicBitboard::new(
                    magic,
                    mask,
                    |occupied_squares| value_generator(pos, occupied_squares),
                    || BitBoard::none(),
                    None,
                )?;
                Ok((pos, magic_bitboard))
            });

            let mut result = ByPos::new_empty_magic_bitboards();

            for data in magic_bitboards {
                let (pos, magic_bitboard) = data?;
                result[pos] = magic_bitboard;
            }

            Ok(result)
        }

        fn create_for_sliding_piece(
            magics: ByPos<(u64, u8)>,
            sliding_directions: &[Dir],
        ) -> Result<ByPos<MagicBitboard<BitBoard>>, CreateMagicBitBoardError<BitBoard>> {
            create_from_magics(
                magics,
                |pos| index_mask_for_sliding_piece(pos, sliding_directions),
                |pos, occupied_squares| {
                    calculate_threatened_squares_for_sliding_piece(
                        pos,
                        occupied_squares,
                        sliding_directions,
                    )
                },
            )
        }

        fn create_for_pawn_double_moves(
            magics: ByColor<ByPos<(u64, u8)>>,
        ) -> Result<ByColor<ByPos<MagicBitboard<BitBoard>>>, CreateMagicBitBoardError<BitBoard>>
        {
            fn for_single_color(
                color: PieceColor,
                magics: ByPos<(u64, u8)>,
            ) -> Result<ByPos<MagicBitboard<BitBoard>>, CreateMagicBitBoardError<BitBoard>>
            {
                create_from_magics(
                    magics,
                    |pos| {
                        if color.pawn_double_move_rank() == pos.rank() {
                            BitBoard::from_position(
                                pos.add_offset(color.colorless_offset((0, 1))).expect(
                                    "All double move positions have a square that can block them",
                                ),
                            )
                        } else {
                            BitBoard::none()
                        }
                    },
                    |pos, occupied_squares| {
                        if occupied_squares != BitBoard::none() {
                            BitBoard::from_position(
                                pos.add_offset(color.colorless_offset((0, 2)))
                                    .expect("All double move positions have a square to move to"),
                            )
                        } else {
                            BitBoard::none()
                        }
                    },
                )
            }

            let ByColor {
                white: white_magics,
                black: black_magics,
            } = magics;

            Ok(ByColor {
                white: for_single_color(PieceColor::White, white_magics)?,
                black: for_single_color(PieceColor::Black, black_magics)?,
            })
        }

        fn create_for_knight() -> ByPos<BitBoard> {
            ByPos::from_generator(calculate_threatened_squares_for_knight)
        }

        fn create_for_king() -> ByPos<BitBoard> {
            ByPos::from_generator(calculate_threatened_squares_for_king)
        }

        fn create_for_pawn_threats() -> ByColor<ByPos<BitBoard>> {
            ByColor {
                white: ByPos::from_generator(|pos| {
                    calculate_threatened_squares_for_pawn(pos, PieceColor::White)
                }),
                black: ByPos::from_generator(|pos| {
                    calculate_threatened_squares_for_pawn(pos, PieceColor::Black)
                }),
            }
        }

        Ok(Self {
            bishop: create_for_sliding_piece(value.bishop, &Dir::DIAGONAL)?,
            rook: create_for_sliding_piece(value.rook, &Dir::ORTHOGONAL)?,
            knight: create_for_knight(),
            king: create_for_king(),
            pawn_threats: create_for_pawn_threats(),
            pawn_double_moves: create_for_pawn_double_moves(value.pawn_double_moves)?,
        })
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MagicsData {
    // FIXME: Should just be `ByPos<u64>`.
    pub bishop: ByPos<(u64, u8)>,
    pub rook: ByPos<(u64, u8)>,
    // TODO: Should really be `ByColor<ByRank<(u64, u8)>>`
    pub pawn_double_moves: ByColor<ByPos<(u64, u8)>>,
}

impl MagicsData {
    pub fn new() -> Self {
        let nil = ByPos::new([(0, 0); 64]);
        Self {
            bishop: nil,
            rook: nil,
            pawn_double_moves: ByColor {
                white: nil,
                black: nil,
            },
        }
    }

    pub fn search_all(
        &mut self,
        save_file: &Path,
        time_limit_each: Duration,
    ) -> std::io::Result<()> {
        let should_cancel = Arc::new(AtomicBool::new(false));
        let should_cancel_clone = Arc::clone(&should_cancel);
        ctrlc::set_handler(move || {
            should_cancel_clone.store(true, Ordering::Release);
            eprint!("\r");
        })
        .unwrap_or_else(|error| panic!("Setting the SIGINT handler failed: {error}"));

        while !should_cancel.load(Ordering::Acquire) {
            let results: Vec<(PieceKind, Pos, u64, u8)> = IntoParallelIterator::into_par_iter([
                (PieceKind::Bishop, &self.bishop, &Dir::DIAGONAL),
                (PieceKind::Rook, &self.rook, &Dir::ORTHOGONAL),
            ]).flat_map(|(piece_kind, map, sliding_directions)| {
                let should_cancel = Arc::clone(&should_cancel);
                Pos::POSITIONS.into_par_iter().flat_map(move |pos| {
                    if should_cancel.load(Ordering::Relaxed) {
                        return None;
                    }
                    let (mut best_magic, mut best_shift) = map[pos];

                    let mask = index_mask_for_sliding_piece(pos, sliding_directions);
                    let num_key_value_pairs = mask.num_bit_variations() as usize;

                    let mut best_array_len =
                        MagicBitboard::array_len_for_magic(best_magic, mask, |occupied_squares| {
                            calculate_threatened_squares_for_sliding_piece(
                                pos,
                                occupied_squares,
                                sliding_directions,
                            )
                        })
                        .unwrap_or(usize::MAX);

                    let start_time = Instant::now();

                    generate_sliding_piece_magics_for_square(pos, sliding_directions)
                        // It must at least be reasonably small, so we don't try to allocate petabytes of ram.
                        .filter(|&(_new_magic, new_array_len)| new_array_len < 50 * num_key_value_pairs)
                        // Stop the search after the time has elapsed.
                        .take_while(|_| start_time.elapsed() < time_limit_each)
                        .for_each(|(new_magic, new_array_len)| {
                            if new_array_len >= best_array_len {
                                return;
                            }
                            let new =
                                MagicBitboard::new(new_magic, mask, |occupied_squares| {
                                    calculate_threatened_squares_for_sliding_piece(
                                        pos,
                                        occupied_squares,
                                        sliding_directions,
                                    )
                                },
                                || { BitBoard::none() },
                                None,
                             )
                                .expect("Already checked to be valid");

                            const BOLD_RED: &str = "\x1b[1;32m";
                            const NO_COLOR: &str = "\x1b[0m";

                            let diff = new_array_len as isize - best_array_len as isize;
                            let diff_text = format!("{BOLD_RED}{diff}{NO_COLOR}");

                            eprintln!(
                                "{pos} - {piece_kind:#?}: New best magic {new_magic:#18x} produces length {new_array_len} ({diff_text})  (data length {num_key_value_pairs})",
                            );
                            best_array_len = new_array_len;
                            best_magic = new_magic;
                            best_shift = new.bits_to_discard();
                        });

                        Some((piece_kind, pos, best_magic, best_shift))
                    })
                }).collect();

            for (piece_kind, pos, new_magic, new_shift) in results {
                let lookup = match piece_kind {
                    PieceKind::Bishop => &mut self.bishop,
                    PieceKind::Rook => &mut self.rook,
                    _ => unreachable!(),
                };
                lookup[pos] = (new_magic, new_shift);
            }

            self.save(save_file)?;
        }

        Ok(())
    }

    pub fn save(&self, save_file: &Path) -> Result<(), std::io::Error> {
        tracing::info!("Saving magics to {save_file:?}");
        let writer = std::io::BufWriter::new(
            std::fs::OpenOptions::new()
                .write(true)
                .truncate(true)
                .create(false)
                .open(save_file)?,
        );
        serde_json::to_writer_pretty(writer, self).expect("serialization should succeed");
        Ok(())
    }

    pub fn load(save_file: impl AsRef<Path>) -> Result<Self, std::io::Error> {
        let save_file = save_file.as_ref();
        tracing::info!("Loading magics from {save_file:?}");
        let reader = std::io::BufReader::new(std::fs::File::open(save_file)?);
        Ok(serde_json::from_reader(reader).expect("deserialization should succeed"))
    }
}

pub fn move_mask_for_sliding_piece(pos: Pos, sliding_directions: &[Dir]) -> BitBoard {
    calculate_threatened_squares_for_sliding_piece(pos, BitBoard::none(), sliding_directions)
}

pub fn index_mask_for_sliding_piece(pos: Pos, sliding_directions: &[Dir]) -> BitBoard {
    let all_possible_threat_squares = move_mask_for_sliding_piece(pos, sliding_directions);
    all_possible_threat_squares & relevant_block_positions_for_sliding_piece(pos)
}

pub fn generate_sliding_piece_magics_for_square<'d>(
    pos: Pos,
    sliding_directions: &'d [Dir],
) -> impl Iterator<Item = (u64, usize)> + 'd {
    // TODO: Could this `HashMap` perhaps be replaced with a `MagicBitBoard`?
    let key_values: HashMap<BitBoard, BitBoard> =
        index_mask_for_sliding_piece(pos, sliding_directions)
            .all_bit_variations()
            .map(|occupied_squares| {
                (
                    occupied_squares,
                    calculate_threatened_squares_for_sliding_piece(
                        pos,
                        occupied_squares,
                        sliding_directions,
                    ),
                )
            })
            .collect();
    let mask = index_mask_for_sliding_piece(pos, sliding_directions);

    let mut rng = fastrand::Rng::new();

    std::iter::repeat(()).filter_map(move |()| {
        let magic = rng.u64(..);
        MagicBitboard::array_len_for_magic(magic, mask, |occupied_squares| {
            key_values[&occupied_squares]
        })
        .inspect_err(|error| {
            match error {
                CreateMagicBitBoardError::IndicesNotUnique { .. }
                | CreateMagicBitBoardError::NeverUniqueWithMagic { .. } => {
                    // Ignored. These are normal.
                }
                CreateMagicBitBoardError::TooLargeForExactFinalSize {
                    max_final_size,
                    actual_size,
                    magic,
                } => {
                    tracing::warn!(
                        %pos,
                        ?sliding_directions,
                        max_final_size,
                        actual_size,
                        magic,
                        "Generated array too large when creating GenericBitboardLookup",
                    )
                }
                CreateMagicBitBoardError::StorageAllocationError => {
                    tracing::warn!(
                        %pos,
                        ?sliding_directions,
                        "Allocation error when creating GenericBitboardLookup",
                    )
                }
            }
        })
        .ok()
        .map(|array_len| (magic, array_len))
    })
}

#[cfg(test)]
mod heavy_tests {
    use crate::{
        raw_board::calculate_threatened_squares_for_sliding_piece, BitBoard, PieceKind, Pos,
    };

    use super::{ThreatLookup, THREAT_LOOKUP};

    #[test]
    fn test_stored_magics_store_threats_correctly() {
        for piece_kind in [PieceKind::Bishop, PieceKind::Rook, PieceKind::Queen] {
            let sliding_directions = piece_kind.sliding_directions().unwrap();
            for pos in Pos::positions() {
                let all_possible_threatened_squares =
                    calculate_threatened_squares_for_sliding_piece(
                        pos,
                        BitBoard::none(),
                        sliding_directions,
                    );
                for occupied_squares in all_possible_threatened_squares.all_bit_variations() {
                    let expected_threats = calculate_threatened_squares_for_sliding_piece(
                        pos,
                        occupied_squares,
                        sliding_directions,
                    );
                    let lookup_method = match piece_kind {
                        PieceKind::Bishop => ThreatLookup::lookup_bishop_threats,
                        PieceKind::Rook => ThreatLookup::lookup_rook_threats,
                        PieceKind::Queen => ThreatLookup::lookup_queen_threats,
                        PieceKind::Knight | PieceKind::Pawn | PieceKind::King => unreachable!(),
                    };
                    let looked_up_threats = lookup_method(&*THREAT_LOOKUP, pos, occupied_squares);
                    assert_eq!(looked_up_threats, expected_threats, "Lookup doesn't match for {piece_kind:#?} on {pos} with occupied squares\n{occupied_squares:?}");
                }
            }
        }
    }
}
