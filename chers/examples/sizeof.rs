use chers::{Board, RawBoard};
use std::mem::size_of;

fn main() {
    println!("RawBoard: {} Bytes", size_of::<RawBoard>());
    println!("Board: {} Bytes", size_of::<Board>());
}
