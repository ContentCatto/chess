use std::{path::Path, str::FromStr, time::Duration};

use chers::threat_lookup::MagicsData;
use tracing_subscriber::filter::Directive;

fn main() {
    tracing_subscriber::fmt()
        .with_env_filter(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(Directive::from_str("info").unwrap())
                .from_env()
                .unwrap_or_else(|error| panic!("Failed to parse log directive: {error}")),
        )
        .compact()
        .init();

    let save_file = Path::new("data/magics.json");
    let mut current = MagicsData::load(save_file).expect("Should open successfully");
    current
        .search_all(save_file, Duration::from_secs(10))
        .expect("Saving should succeed");
}
