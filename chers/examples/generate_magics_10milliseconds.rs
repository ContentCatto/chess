use std::{path::Path, time::Duration};

use chers::threat_lookup::MagicsData;

fn main() {
    let save_file = Path::new("data/magics.json");
    let mut current = MagicsData::load(save_file).expect("Should load successfully");
    current
        .search_all(save_file, Duration::from_secs(1))
        .expect("Saving should succeed");
}
